# Use ffxblue Node.js Docker image
FROM 175914186171.dkr.ecr.ap-southeast-2.amazonaws.com/infrastructure/cli-node-builder:0.37.0 AS build

# Define NPM token required to access NPM private registry
ARG NPM_TOKEN
# Define PACT related environment variables
ARG PACT_BROKER_HOST
ARG PACT_PUBLISH_CONTRACTS

RUN if [ -z "${NPM_TOKEN}" ]; then \
        echo "ERROR: Required build argument 'NPM_TOKEN' is not set"; \
        exit 1; \
    fi

# Set working directory
WORKDIR /srv

COPY . .
# ---- Setting files for release image ----

# Install production modules only, and copy them to a temporary location
RUN yarn install --no-progress --production=true
RUN cp -R node_modules /tmp/prod_node_modules

# ---- Build ----

# Install ALL node_modules which adds the 'devDependencies' to what we've already installed
RUN yarn install --no-progress
RUN make set-build-version
RUN yarn prestart
RUN yarn test:contract

# ---- Release ----
# Build image for release using only necessary files
# (No devDependencies)
FROM node:14.15.0-alpine AS release

WORKDIR /srv

COPY --from=build /srv/package.json /srv
COPY --from=build /tmp/prod_node_modules /srv/node_modules
COPY --from=build /srv/build /srv/build
COPY --from=build /srv/.version-git-hash /srv
COPY --from=build /srv/.version-git-tag /srv

# Start application
CMD ["node", "./build/public/server"]
