require 'sprkl'

SparkleFormation.new(:stack, provider: :aws).load(:base)
                .overrides(&::Sprkl.process(ENV['environment'], ENV['version']))
