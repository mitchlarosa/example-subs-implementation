# CI Pipeline

CI/CD process runs in [Concourse CI](https://concourse-ci.org/), extended by our own custom tooling.

## Service Pipeline

The pipeline snippets used for services created from this skeleton are defined by
 [pipeline.service.list](./pipeline.service.list), which will be renamed to `pipeline.list` when the skeleton has been
 applied to create an application.

## Skeleton Pipeline

The skeleton itself currently does not have it's own pipeline, but is planned.
