# CI Pipeline

CI/CD process runs in [Concourse CI](https://concourse-ci.org/), extended by our own custom tooling.

## Introduction

The final `pipeline.yaml` will be produced by merging multiple pipeline snippets together. This aids in understanding as
 snippets can be more focused on particular tasks, and allows a 'shared library' of snippets to be used as appropriate.

`pipeline.list` defines which snippets will be used. These snippets can be sourced from the application's repository,
 or from other common sources.

## Snippets

### Shared

#### [pipeline.yaml](./pipeline.yaml)

Defines resources and resource types which are common to multiple job snippets.

### Build

#### [build.yaml](./jobs/build.yaml)

Build process from the main branch of the `git` repository (typically `master`).

* SemVer, `git` tags
* Static Checks
* Unit tests
* Docker Image

#### [check-pull-request.yaml](./jobs/check-pull-request.yaml)

Build process for pull requests made against the repository.

* Merge with the main branch (result does not get pushed back)
* Static Checks
* Docker Image
* Update PR Status (passed/failed)
* Add comments to the PR from build results

#### [deploy-build.yaml](./jobs/deploy-build.yaml)

Prepare infrastructure resources required to run builds.

### Release

Snippet filenames for `release` jobs should be prefixed with `release-`

#### Automatically Triggered Releases

* [release-autodeploy-development.yaml](./jobs/release-autodeploy-development.yaml)

Automatically deploys to the `development` environment after a successful build of `master`

#### Manually Triggered Releases

* `test`: [release-deploy-test.yaml](./jobs/release-deploy-test.yaml)
* `staging`: [release-deploy-staging.yaml](./jobs/release-deploy-staging.yaml)
* `production`: [release-deploy-production.yaml](./jobs/release-deploy-production.yaml)

Releases to the deployment environment when requested using [Releaser](https://bitbucket.org/ffxblue/infrastructure-releaser)

Releases can be requested via Slack in [#tech-releases-ink](https://necpublishing.slack.com/archives/C6W7AUN1E)

### Other

#### [set-catalog.yaml](./jobs/set-catalog.yaml)

Configures the [Service Catalog](https://airtable.com/shrWVYxrHYITQ13RI) entry for this service.

This will be run whenever the main branch has been modified.

See [Documentation](https://nine.atlassian.net/wiki/spaces/TECH/pages/1175617882/Service+Catalog)

#### [set-pipeline.yaml](./jobs/set-pipeline.yaml)

Updates the CI pipeline in Concourse, keeping the applied pipeline in sync with any changes.

This will be run whenever the main branch has been modified.
