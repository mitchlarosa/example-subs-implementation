var path = require('path')

var config = {
  collectCoverage: true,
  coverageThreshold: {
    global: {
      branches: 65,
      functions: 60,
      lines: 75,
      statements: 75,
    },
  },
  moduleDirectories: ['node_modules', path.resolve(__dirname, 'app/src')],
  restoreMocks: true,
  roots: ['<rootDir>/app/src'],
  setupFiles: ['./jest.setup.js'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  transform: {
    '^.+\\.j?t?sx?$': 'babel-jest',
  },
}

module.exports = config
