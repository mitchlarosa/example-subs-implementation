/* eslint-disable import/no-extraneous-dependencies */
const jest = require('jest')
const path = require('path')
const enzyme = require('enzyme')
const Adapter = require('enzyme-adapter-react-16')
const dotenv = require('dotenv')

const dotenvPath = path.resolve('./.env.test')
require('isomorphic-fetch')

dotenv.config({ path: dotenvPath })

enzyme.configure({ adapter: new Adapter() })
