# Maintaining this skeleton

This skeleton is cloned for each new Node/React app. As such, it is important to keep it in good health. So the first thing to note is that if you or a colleague fix something on a repo cloned from this skeleton, please raise the equivalent PR on the skeleton at the same time and shepherd it through to `master`.

The entire skeleton repository is one big template. This was done to remove manual work and the errors that result from essentially interpolating the template oneself by following a readme file (the way we used to do it). Please read the current `README.md` to get a feel for how this template is consumed by an end-user developer.

Once you've read the `README.md` you'll be wondering how the templating automation works:

- `make init-repo` calls a shell script: `./bin/interpolate-templates.sh`
- This script gathers input from a file called `./template-parameters.yaml` or command-line if no file is present (automation can first init-repo the file and then call the script)
- These inputs are things like "what are your helm release ID's" and can be extended over time as needed
- Next, files not needed for the setup defined in the parameters are removed
- Then the core logic happens: a Docker container is spun up, the entire repository is passed into it as a Docker volume mount, Gomplate (a general-purpose Go templating engine) is executed over this repository, and the output is stored in a temporary directory inside this repository (available locally). This temporary folder is then in-place moved onto the existing repository structure (replacement)
- Temporary files and other irrelevant state are now cleaned up

## Developing this skeleton

1. Understand Go templating - specifically Gomplate. This is the same syntax we use for our Kubernetes Helm charts and Go generate. Each tool wraps Go templating slightly differently and may choose to expose or not expose different capabilities (often from the Sprig standard library for Go templating), but the differences are minor or domain-specific.

  Gomplate: https://gomplate.hairyhenderson.ca/

  Sprig: http://masterminds.github.io/sprig/

2. Get a feel for how the existing templating has been done. It's pretty basic: largely variable substitution for things like repository name, and some if/else conditionals for things like whether a database is to be used. Gomplate provides much richer capabilities than this, but our use-case is straight-forward.

3. When you are making a change, `git add` the changed files, and then run `make init-repo`. To revert to the original state so you can test something you've changed, run `make init-repo-undo` - this will blow away any changes you have not done a `git add` for.

4. So the development cycle goes like so:

  - Make a change.
  - `make init-repo` to test the change (if touching an optional component, remember to test with and without the component)
  - If a problem arises, then once you've analysed it and are ready to repeat the steps above, first run:
  - `make init-repo-undo` to revert to the original git state of the skeleton (except any changes that you `git add`)

5. You may find it useful to write a `./template-parameters.yaml` file so that you don't need to go through interactive mode every time you test (unless intertactive mode is what is being tested).