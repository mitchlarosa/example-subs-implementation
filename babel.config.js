module.exports = function(api) {
  const isDev = process.env.NODE_ENV === 'development'

  const plugins = [
    [
      'babel-plugin-styled-components',
      {
        pure: true,
      },
    ],
    'graphql-tag',
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-transform-runtime',
  ]

  if (!isDev) {
    plugins.push('@babel/plugin-transform-react-constant-elements', '@babel/plugin-transform-react-inline-elements')
  }

  const presets = [
    [
      '@babel/preset-react',
      {
        development: isDev,
        runtime: 'automatic',
      },
    ],
    '@babel/preset-typescript',
  ]

  const env = {
    client: {
      presets: [
        [
          '@babel/preset-env',
          {
            targets: {
              esmodules: true,
            },
          },
        ],
      ],
    },
    server: {
      presets: [
        [
          '@babel/preset-env',
          {
            targets: {
              node: '14.15.0',
            },
          },
        ],
      ],
    },
    test: {
      presets: ['@babel/preset-env'],
    },
  }

  api.cache.using(() => isDev)

  return {
    plugins,
    presets,
    env,
  }
}
