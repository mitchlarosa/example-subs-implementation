module.exports = {
  client: {
    includes: ['./app/src/**/*.ts', './app/src/**/*.tsx'],
    service: {
      name: '@ffxblue/{<{ (datasource "values").repoName }>}',
      localSchemaFile: './app/src/config/graphql/schema.json',
    },
  },
}
