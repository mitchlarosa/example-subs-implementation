
# NodeJS Application Skeleton

This repository is a skeleton to be used as a starting point for render applications using Node and React.

## Using this skeleton

### Requirements

* [cli-local-environment](https://bitbucket.org/ffxblue/cli-local-environment) - `latest`

Node versions are managed with [nodenv](https://github.com/nodenv/nodenv) which is installed by `cli-local-environment`.

### Setup application repo in Bitbucket

**Do not add any application code until after the below steps are completed**

1. In Bitbucket click the `+` (Create) button -> `Repository`
    * For `Access Level` check `This is a private repository`
    * For `Include a README?` select `Yes, with a template`
    * In `Advanced Settings` for `Language` select `Node.js`
2. In `Settings` under `Branch Permissions` -
    * For `Write Access` leave blank - `Nobody has write access`
    * For `Merge via pull request` select `Everybody`
    * For `Merge checks` -
        * `Check for at least 2 approvals`
        * `Check for unresolved pull request tasks`
        * `Check the last commit forsuccessful build and no failed builds`
        * `Prevent a merge with unresolved merge checks`
3. In `Settings` under `Links` toggle on `Require issue keys in commit messages`
4. In `Settings` under `Default Reviewers` add users
5. In your local `~/workspace` directory clone the repository for the new application -
   ```
   git clone git@bitbucket.org:ffxblue/skeleton-nodejs-application.git <repository-name>
   ```
6. Replace the git remote with the new repo -
   ```
   git remote set-url origin git@bitbucket.org:ffxblue/<repository-name>.git
   ```
7. Push to the new repository with the skeleton
   ```
   git push origin HEAD
   ```
8. Branch. **DO NOT MAKE ANY MANUAL CHANGES YET**. Before you progress to the next step, you will need to be prepared with a couple of things:
    * You will need four randomly generated alphanumeric strings (you will be prompted to provide these for Helm release IDs) with the following requirements:
        - Be a lower-case 5-character ([a-z0-9]) value
        - MUST start with a letter (is used in DNS names, which require a letter as the first character)
        - MUST be unique within helm (test this by running `helm ls | grep your-random-str` ) Note that one of these must be tested on the production cluster (for helm-release-production) and the other three on the development cluster.
        - Don't generate by mashing keys! https://www.google.com/search?q=random+string+generator
    * You should have some idea about which on-call-group this app will be assigned to (you will be prompted to choose an option).

9. Run `make init-repo` and commit these automated changes.
   (If required you can revert to the skeleton's initial state by running `make init-repo-undo`.)
    * You may be required to run `brew update && brew upgrade node-build` if the `nodenv install` step fails

10. Test your application locally by running all available build options -

    #### Using source

    ##### Development

    ```
    yarn dev
    ```

    ##### Production

    ```
    yarn build && yarn start:local
    ```
    #### Using Docker

    ##### Development

    ```
    docker-compose up --build -d && docker-compose logs -f
    ```

    ##### Production

    ```
    docker build --build-arg NPM_TOKEN=${NPM_TOKEN} PACT_BROKER_HOST=${PACT_BROKER_HOST} PACT_PUBLISH_CONTRACTS=${PACT_PUBLISH_CONTRACTS} -t <repository-name> .
    ```
    ```
    docker run -d -P <repository-name>
    ```

11. Commit, push, then raise a pull request for review

12. Once your pull request has (minimum) two approvals it can be merged to `master`

13. Make any manual changes for cleanup left over after the automated make-init task.
  - Remove unused skeleton tasks from `Makefile` -
    * check-shell
    * init-repo
    * init-repo-undo

### Setup infrastructure

#### Setup credentials management in Confidant

If you do not have login access to Confidant it can be requested via a [ServiceNow](https://ffxblue.atlassian.net/wiki/spaces/TECH/pages/30854847/Credentials+Management#CredentialsManagement-GettingaccesstoConfidant) form.

1. Login to [Confidant](https://ffxblue.atlassian.net/wiki/spaces/TECH/pages/30854847/Credentials+Management)
2. In Confidant click the `+` button and select `Create credential`
    * Under `Credential Name` enter your application name using the `<repository-name>-[environment]` naming convention
    * Under `Credential Pairs` enter each secret your application requires using the Helm naming convention (all lowercase and full stop to separate words)
        * eg. `Key` field `myservice.api.key` and `Value` field `ABC123` will be made available in your application as environment variable `MYSERVICE_API_KEY=ABC123`
    * Under `Credential Metadata` in `Key` field enter `secret-name` and in `Value` field enter the Helm chart name `blue-app`
    * Repeat the above steps for each environment your application requires
3. In Confidant click the `+` button and select `Create service`
    * Under `Service ID` enter your application name using the `k8s-<repository-name>-<environment>-v1` naming convention
    * Under `Credentials` add the `<repository-name>-<environment>` credential you created in step 2 and `k8s-cluster-development` (for `development`, `test` and `staging`) or `k8s-cluster-production` (for `production`) environments
    * Repeat the above steps for each environment your application requires

#### Setup CI pipeline in Concourse

Please ensure you have completed steps **Create application repo in Bitbucket** and **Setup credentials management in Confidant** prior to commencing this task.

1. In the `#tech-ci-ink` Slack channel run -


    ```
    bootstrap main <repository-name>
    ```


    Visit the application's Concourse pipeline at [https://ci.ffxblue.com.au/teams/main/pipelines/repository-name to check it is created, unpaused and visible.

    It might still be required to unpuase the pipeline, if that is the case, in the `#tech-ci-ink` run  -

    ```
    unpause-pipeline main <repository-name>
    ```

2. In the `#tech-ci-ink` Slack channel subsequent pipeline jobs can be run using -


    ```
    trigger-job main <repository-name> <job-name>
    ```

#### Deployments

Upon merge of a feature branch to `master` the Concourse pipeline will automatically trigger a new `build` job which (if successful) then triggers a deploy to the `development` environment.

##### To retry a failed `development` environment deploy

In the `#tech-ci-ink` Slack channel run -

```
trigger-job main <repository-name> deploy-development
```

##### To deploy to other environments

In the `#tech-releases-ink` Slack channel run -

```
@INK Releaser request <repository-name> <build-number> <environment>
```


### First code change

1. Start implementing the interesting details of your application

__________________________________
Remove README above this line!

Keep README below this line!
__________________________________

# %REPLACE_ME%
Replace the above heading with the name of the application, e.g. 'Render Web Metro'

This application is responsible for:

%REPLACE_ME% add application responsibilities in the list below
* ...
* ...

## Table of Contents

1. [Application Structure](#application-structure)
1. [Requirements](#requirements)
1. [Setup](#setup)
1. [Development](#development)
1. [Dependencies](#dependencies)

## Application Structure

```
│── .dockerignore                            # [Docker](https://www.docker.com/) ignore config
│── .editorconfig                            # IDE config (for local development only)
│── .env.example                             # Example environment variables (for local development only)
│── .env.pact                                # Environment variables for [Pact] (https://pact.io/) contract testing
│── .env.test                                # Environment variables for unit testing
│── .eslintignore                            # [ESLint](https://eslint.org) ignore config
│── .eslintrc                                # [ESLint](https://eslint.org) config
│── .gitignore                               # Git ignore config
│── .node-version                            # [nodenv](https://github.com/nodenv/nodenv) config (for local development only)
│── .npmrc                                   # [NPM](https://docs.npmjs.com/files/npmrc) config
│── .prettierignore                          # [Prettier](https://prettier.io/) ignore config
│── .prettierrc                              # [Prettier](https://prettier.io/) config file
│── apollo.config.js                         # [Apollo](https://www.apollographql.com/docs/devtools/apollo-config) CLI config
│── babel.config.js                          # [Babel](http://babeljs.io) config
│── codegen.yml                              # [GraphQL Code Generator](https://graphql-code-generator.com/) config
│── docker-compose.yaml                      # [Docker Compose](https://docs.docker.com/compose/) config (for local development only)
│── Dockerfile                               # [Docker](https://www.docker.com/) config
│── jest.config.js                           # [Jest](jestjs.io/) config
│── jest.pact.config.js                      # [Pact] (https://pact.io/) config (using Jest as test runner)
│── jest.setup.js                            # [Jest](jestjs.io/) unit test setup
│── Makefile                                 # [Make](https://en.wikipedia.org/wiki/Makefile) config (for Git semver files)
│── package.json                             # [NPM/Yarn](https://docs.npmjs.com/files/package.json) config
│── pact.hooks.js                            # [Pact] (https://pact.io/) hooks
│── pact.setup.js                            # [Pact] (https://pact.io/) setup
│── tsconfig.json                            # [TypeScript](https://www.typescriptlang.org/) config
│── yarn.lock                                # [Yarn](https://classic.yarnpkg.com/en/docs/yarn-lock) dependency lockfile
│── .yarnclean                               # [Yarn](https://classic.yarnpkg.com/en/docs/cli/autoclean/) file and folder removal config
│── .yarnrc                                  # [Yarn](https://classic.yarnpkg.com/en/docs/yarnrc/) config
│── app
│   │── src
│   │   │── assets                           # Static assets (eg. images, fonts)
│   │   │── client
│   │   │   └── client.tsx                   # Example client side application entry
│   │   │── components                       # Example React components
│   │   │── config
│   │   │   |── environment       
│   │   │   │   │── environment.client.ts    # Get client side environment variables
│   │   │   │   │── environment.ts           # Get server side environment variables
│   │   │   │   │── types.ts                 # TypeScript types
│   │   │   │   │── variables.client.ts      # Set client side environment variables
│   │   │   │   │── variables.common.ts      # Set common environment variables (client and server side)
│   │   │   │   └── variables.server.ts      # Set server side environment variables
│   │   │   │── featureflags       
│   │   │   │   │── featureflags.client.tsx  # Client side feature flags config
│   │   │   │   │── featureflags.default.ts  # Common feature flags config
│   │   │   │   └── featureflags.server.ts   # Server side feature flags config
│   │   │   │── graphql
│   │   │   │   │── graphql.ts               # Apollo Client
│   │   │   │   │── introspectionQueryResultData.ts  # Types for Apollo Client cache IntrospectionFragmentMatcher
│   │   │   │   │── schema.json              # GraphQL schema (api-front)
│   │   │   │   └── types.ts                 # GraphQL schema types
│   │   │   └── logger
│   │   │       │── logger.client.ts         # Client side logging
│   │   │       └── logger.ts                # Server side logging
│   │   │── routes
│   │   │   └── index.tsx                    # Route config
│   │   │── server
│   │   │   |── metrics       
│   │   │   │   │── constants.ts             # Prometheus metrics routes
│   │   │   │── routes       
│   │   │   │   │── healthCheck.ts           # Health check route handler
│   │   │   │   │── ping.ts                  # Ping route handler
│   │   │   │   └── serviceInfo.ts           # Service info route handler
│   │   │   └── server.tsx                   # Example server side application entry
│   │   │── templates
│   │   │   |── Html
│   │   │   │   └── Html.tsx                 # Example base template
│   │   │   │── App.tsx                      # Example application JS template
│   │   │   └── App.spec.tsx                 # Example application Jest tests
│   │   └── types                            # TypeScript global declarations
│   └── tools
│       │── storybook
│       │   │── addons.js                    # StoryBook addons
│       │   │── config.js                    # StoryBook config
│       │   └── webpack.config.js            # StoryBook Webpack config
│       └── webpack
│           │── webpack.config.client.js     # Webpack client side config
│           │── webpack.config.default.js    # Webpack common config (client and server side)
│           └── webpack.config.server.js     # Webpack server side config
│── ci
│   │── config.yaml                          # Concourse CI config
│   │── helm_config.yaml                     # Concourse CI Helm config
│   │── pipeline.yaml                        # Concourse CI pipeline
│   └── tasks
│       |── node
│       │   └── prestart.yaml                # Concourse CI task - runs in `check-pull-request` job to build application
│       └── pr-preview
│           └── preview-build-values.yaml    # Concourse CI task - runs in `check-pull-request` job to start PR server
│── githooks
│   │── common
│   │   └── validate-branch.sh               # Hook to validate the branch
│   │── post-merge
│   │   └── update-trigger.sh                # Hook to update post merge
│   │── pre-commit
│   │   │── validate-branch.sh               # Hook to validate the branch
│   │   └── validate-git-settings.sh         # Hook to validate git settings
│   │── pre-push
│   │   └── validate-branch.sh               # Hook to validate the branch
│   │── prepare-commit-msg
│   │   └── verify-commit-message.sh         # Hook to verify the commit message
│── infrastructure
│   │──helm
│   │   │── values-common.yaml               # Helm common config
│   │   │── values-development.yaml          # Helm development config
│   │   │── values-production.yaml           # Helm production config
│   │   │── values-staging.yaml              # Helm staging config
│   │   └── values-test.yaml                 # Helm test config
│   │── sparkleformation
│   │   │── components
│   │   │   └── base.rb
│   │   │── config
│   │   │   │── shared
│   │   │   │   └── ecr-app.yaml             # Amazon EC2 Container Registry config
│   │   │── .rubocop.yml                     # Rubocop config
│   │   └── stack.rb
│   │── .sfn                                 # SparkleFormation config
│   │── Gemfile                              # Gem config for SparkleFormation
│   └── Gemfile.lock                         # Gem dependency lockfile
└── tests
    └──contract
       └── lib
           └── matchers.ts                   # Pact matchers
```

## Requirements

* [cli-local-environment](https://bitbucket.org/ffxblue/cli-local-environment) - `latest`

Node versions are managed with [nodenv](https://github.com/nodenv/nodenv) which is installed by `cli-local-environment`.

## Setup

### Using Yarn

##### Create / install dependencies (all environments)

```
brew update && brew upgrade node-build
make env
yarn
```

##### Build / start server

Development

Starts server at http://localhost:3000

```
yarn dev
```

Production

Starts server at  http://localhost:3333

```
yarn build
yarn start:local
```

### Using Docker

##### Install dependencies

Complete [cli-local-environment](https://bitbucket.org/ffxblue/cli-local-environment) installation details

Run `lenv start` and choose **Shared** option

##### Build / start server

Starts server using the `traefik` address configured in the `docker-compose.yaml`. i.e. `skeleton-nodejs-application-local-v1.ffxblue.com.au`

```
docker-compose up --build -d && docker-compose logs -f
```

## Development

### Types

To run type checking,

```bash
yarn types
```

To run type checking while watching for changes,

```bash
yarn types:watch
```

### Linting

To run linting with auto-formatting,

```bash
yarn format
```

To run linting without auto-formatting,

```bash
yarn lint
```

## Testing

To run unit tests,

```bash
yarn test
```

To run unit tests while watching for changes,

```bash
yarn test:watch
```

To run unit test coverage,

```bash
yarn test:coverage
```

To run contract tests,

```bash
yarn test:contract
```

## Docs

To run docs server,

```bash
yarn docs:build && yarn docs:start
```

## Dependencies

This skeleton uses the following libraries and frameworks -

Server

* [Node](https://nodejs.org/en/)
* [Express](https://expressjs.com/)

Lint

* [@ffxblue/eslint-config](https://bitbucket.org/ffxblue/eslint-config) (using [ESLint](https://eslint.org/))
* [Prettier](https://prettier.io/)

Test

* [Jest](https://facebook.github.io/jest/)
* [testing-library] (https://testing-library.com/)
* [Pact] (https://pact.io/)

Types

* [TypeScript](https://www.typescriptlang.org/)

Bundle

* [Webpack](https://webpack.js.org/)

Compile

* [Babel](https://babeljs.io/)

CSS

* [styled components](https://www.styled-components.com)

Logging and Metrics

* [@ffxblue/library-render-logging](https://bitbucket.org/ffxblue/library-render-logging) (using [express-winston](https://github.com/bithavoc/express-winston))
* [@ffxblue/library-nodejs-middlewares](https://bitbucket.org/ffxblue/library-nodejs-middlewares) (using [prometheus](https://prometheus.io/))

CI / CD

* [Concourse CI](http://concourse.ci/)
* [Helm](https://github.com/kubernetes/helm)
* [SparkleFormation](http://www.sparkleformation.io/)
