import {apiFrontProvider} from "./pact.setup";

jest.setTimeout(30000)

beforeAll(async () => {
  await apiFrontProvider.setup()
})

afterAll(async () => {
  await apiFrontProvider.verify()
  await apiFrontProvider.finalize()
})
