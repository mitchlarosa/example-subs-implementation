#!/usr/bin/env bash

set -euo pipefail

script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Import helper functions
. "${script_dir}"/functions/io-helpers.sh

for file in $(git diff --name-only | grep ".*\.sh")
do
  shellcheck "$file"
done

formatted_echo "green" "Shellcheck successful"