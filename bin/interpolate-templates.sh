#!/usr/bin/env bash
set -euo pipefail

script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Import helper functions
. ${script_dir}/functions/git-helpers.sh
. ${script_dir}/functions/io-helpers.sh

repo_root=$(git_repo_root)
repo_path=${repo_root}

if [ ${repo_root} != $(pwd) ]; then
    cd "${repo_root}"
fi

declare -a impacts=(
  "low"
  "medium"
  "high"
)

declare -a on_call_groups=(
  "amp-9web"
  "amp-afrweb"
  "amp-dataengineering"
  "amp-distribution"
  "amp-ed-authoring"
  "amp-ed-tools"
  "amp-enterprisesystems"
  "amp-metroweb"
  "amp-mobileapps"
  "amp-platformengineering"
  "amp-subscriptions"
  "amp-techsquads"
)

declare -a release_groups=(
  "9web"
  "afrweb"
  "corepublishing"
  "dataengineering"
  "distribution"
  "ed-authoring"
  "ed-tools"
  "financialmarket"
  "metroweb"
  "platformengineering"
  "productivity"
  "shared"
  "subscriptions"
  "video"
)

function interactive_mode() {
  formatted_echo "blue" "\nPre-pulling required container images...\n"
  docker pull "${docker_infrastructure_builder_image}" || {
    formatted_echo "red" "\n  Failed to pull container image from AWS ECR - attempting 'bs auth':\n"
    bs auth || {
      formatted_echo "red" "\n  Aborting due to failure to 'docker pull ${docker_infrastructure_builder_image}'.\n\n  Is 'bs auth' setup correctly? Are you online?\n\n";
      exit 1
    }
    docker pull "${docker_infrastructure_builder_image}"
  }

  formatted_echo "blue" "\nPlease follow the prompts below to initialise this new repository:"

  formatted_echo "green" "\n\nTeam that supports the service:"
  list_prompt "${on_call_groups[@]}"
  local on_call_group="${choice}"

  formatted_echo "green" "\n\nTeam that manages the releases:"
  list_prompt "${release_groups[@]}"
  local release_group="${choice}"

  formatted_echo "green" "\n\nWhat severity is the impact of failure of this service?"
  list_prompt "${impacts[@]}"
  local impact="${choice}"

  formatted_echo "green" "\nProvide a short desciption of the repo for the package.json"
  local repo_description
  read -r -p "Description: " repo_description

  formatted_echo "green" '\nHelm release IDs below must not be in use, 5 characters, lowercase alphanumeric, first letter is a-z only - i.e. ^[a-z][a-z0-9]{4}$'

  request_helm_release_id "development"
  local helm_release_id_development=$helm_release_id

  request_helm_release_id "test"
  local helm_release_id_test=$helm_release_id

  request_helm_release_id "staging"
  local helm_release_id_staging=$helm_release_id

  request_helm_release_id "production"
  local helm_release_id_production=$helm_release_id

  # Write a simple YAML file containing parameters for later
  cat <<EOF > template-parameters.yaml
repoName: $repo_name
impact: $impact
onCallGroup: $on_call_group
releaseGroup: $release_group
repoDescription: "$repo_description"
helmReleaseDevelopment: $helm_release_id_development
helmReleaseTest: $helm_release_id_test
helmReleaseStaging: $helm_release_id_staging
helmReleaseProduction: $helm_release_id_production
EOF

  formatted_echo "green" "\nValues to use:"
  cat template-parameters.yaml

  formatted_echo "green" "\n\nPlease verify the above values. If incorrect, this script will abort - rerun 'make init-repo'."
  while true; do
      local continue_choice
      read -p "Are the above values correct? (y/n): " continue_choice
      case $continue_choice in
          [Yy]* ) break;;
          [Nn]* ) rm template-parameters.yaml; formatted_echo "red" "\n  Aborting due to user choice.\n"; exit 1;;
          * ) formatted_echo "red" "\n  Please answer yes or no. \n";;
      esac
  done
  echo
}

function request_helm_release_id() {
  local deploy_environment=$1
  while true; do
    read -p "Helm release ID for '$deploy_environment': " helm_release_id
    [[ $helm_release_id =~ ^[a-z][a-z0-9]{4}$ ]] || { formatted_echo "red" "\n  Input does not follow required pattern - please try again.\n  Helm release IDs must not be in use, 5 characters, lowercase alphanumeric, first letter is a-z only\n  Regex: ^[a-z][a-z0-9]{4}$\n"; continue; }
    break
  done
}

function prepare_pipeline() {
  git mv infrastructure/ci/pipeline.service.list infrastructure/ci/pipeline.list
  git mv infrastructure/ci/pipeline.service.yaml infrastructure/ci/pipeline.yaml
  git rm infrastructure/ci/README.md
  git mv infrastructure/ci/README.service.md infrastructure/ci/README.md
}

function strip_interpolation_comments() {
  docker run -t --rm \
             -v "${repo_root}:/go/${repo_path}:delegated" \
             -w /go/${repo_path} \
             -e repo_name="${repo_name}" \
             "${docker_infrastructure_builder_image}" \
      sh -c 'find . -type f ! -path "./.git/*" ! -path "./.idea/*" ! -path "./.vscode/*" ! -path "./node_modules/*" | xargs -I {} sed -i -E "s/^(.*)(\/\/|#) gomplate:(.*)$/\1\3/g" "{}"'
}

function interpolate_repository() {
  # Git ignored files aren't useful to interpolate as they aren't part of the committed repo
  git_ignored=""
  # 'git check-ignore --stdin' returns:
  # * 0 when there are ignored files in the input
  # * 1 when there are _no_ ignored files in the input
  # * 128 when any other error occurs
  # 0 or 1 are expected, valid cases, so allow either
  if find . -type f ! -path "./.git/*" ! -path "./.idea/*" ! -path "./.vscode/*" | git check-ignore --stdin; then
    git_ignored=$(find . -type f ! -path "./.git/*" ! -path "./.idea/*" ! -path "./.vscode/*" | git check-ignore --stdin)
  elif [[ $? -ne 1 ]]; then
    formatted_echo "red" "Failed to check gitignored files for exclusion from template interpolation"
    exit 1
  fi
  local args=(
    --left-delim "{<{" \
    --right-delim "}>}" \
    --datasource values=template-parameters.yaml \
    --exclude .git \
    --exclude .idea \
    --exclude .vscode \
    --exclude "bin/interpolate-templates.sh" \
    --exclude node_modules \
    --input-dir . \
    --output-dir ./template-results
  )
  for i in ${git_ignored}; do
    args+=(--exclude "${i}")
  done
  docker run -t --rm -v "${repo_root}:/go/${repo_path}:delegated" -w /go/${repo_path} "${docker_infrastructure_builder_image}" \
    gomplate "${args[@]}"
}

function rename_skeleton_strings() {
  docker run -t --rm \
             -v "${repo_root}:/go/${repo_path}:delegated" \
             -w /go/${repo_path} \
             -e repo_name="${repo_name}" \
             "${docker_infrastructure_builder_image}" \
      sh -c 'find . -type f ! -path "./.git/*" ! -path "./.idea/*" ! -path "./.vscode/*" ! -path "./bin/interpolate-templates.sh" ! -path "./node_modules/*" ! -path "./README.md" | xargs -I {} sed -i "s/skeleton-nodejs-application/${repo_name}/g; s/skeleton_nodejs_application/${repo_name//-/_}/g" "{}"'
}

function initialise() {
  repo_name=$(basename "$PWD")
  docker_infrastructure_builder_image="175914186171.dkr.ecr.ap-southeast-2.amazonaws.com/infrastructure/cli-infrastructure-builder"

  if [ -f "template-parameters.yaml" ]; then
     formatted_echo "blue" "\nTemplate parameters file (template-parameters.yaml) exists. Skipping interactive mode."
  else
     formatted_echo "blue" "\nTemplate parameters file (template-parameters.yaml) does not exist. Running in interactive mode."
     interactive_mode
  fi
}

function transform_repository() {
  prepare_pipeline

  strip_interpolation_comments

  rename_skeleton_strings

  interpolate_repository
}

function finalise() {
  # Replace existing files with interpolated versions
  cp -R ./template-results/ .

  # Clean up after ourselves
  rm -rf ./template-results/

  formatted_echo "blue" "\nRunning 'make' to confirm that the app is healthy\n\n"

  # Confirm that the dependencies are healthy and that the repo passes basic checks
  make env
  yarn install
  yarn types
  yarn lint
  make check-shell

  git status
  formatted_echo "blue" "\n\nRepository initialisation off skeleton complete.\nPlease examine the 'git status' output above and then proceed to the next step of 'README.md' in 'skeleton-nodejs-application'\n"
  rm ./template-parameters.yaml
}

###

initialise

transform_repository

finalise
