#!/usr/bin/env bash
set -euo pipefail

function formatted_echo() {
  local format=$1
  local text=$2
  local formatted_text=""
  case $format in
      "blue" ) formatted_text="\033[1;34m$text\033[0m";;
      "green" ) formatted_text="\033[1;32m$text\033[0m";;
      "red" ) formatted_text="\033[1;31m$text\033[0m";;
      * ) ;;
  esac
  echo -e "${formatted_text}"
}

function list_prompt() {
  local options_array=("$@")
  for i in "${!options_array[@]}"; do
    formatted_echo "green" "  $i) ${options_array[$i]}"
  done
  while true; do
    local number_chosen
    read -p 'Please enter a NUMBER from above: ' number_chosen
    [[ $number_chosen =~ ^[0-9]+$ ]] || { formatted_echo "red" "\n  Please enter a valid number.\n"; continue; }
    if ((number_chosen >= 0 && number_chosen < ${#options_array[@]})); then
      break
    else
      formatted_echo "red" "\n  Selection out of range - please try again.\n"
    fi
  done
  choice="${options_array[$number_chosen]}"
}
