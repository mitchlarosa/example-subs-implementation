#!/usr/bin/env bash

function git_repo_root() {
    git rev-parse --show-toplevel
}
