#!/usr/bin/env bash
# This task will upload the pact contracts written in the given path
# To run this task the host machine/container should have following
# pre-requisites : jq, curl, git

function pact_post_url() {
  # Get the consumer and provider name from generated contracts
  CONSUMER=$( jq -M -r '.consumer.name // ""' < "$file" )
  PROVIDER=$( jq -M -r '.provider.name // ""' < "$file")
  if [[ "$CONSUMER" == "" ]] || [[ "$PROVIDER" == "" ]] ; then
    echo "ERROR: Valid Consumer/Provider are not available in the contract file"
    return 1
  fi
  # Construct PUBLISH URL
  URL="$PACT_BROKER_HOST/pacts/provider/${PROVIDER?}/consumer/${CONSUMER?}/version/${PACT_CONSUMER_VERSION?}"
}

function publish_contract() {
  echo "URL to post : $URL"
  echo "File to post : $file"
  curl --fail -XPUT -H "Content-Type: application/json" -d @"$file" "$URL" >/dev/null
}

PACT_CONTRACTS_PATH="$1"
if [[ "$PACT_CONTRACTS_PATH" == "" ]] ; then
  echo "Error : Need to pass the path for the json contracts"
  exit 1
fi

: "${PACT_PUBLISH_CONTRACTS:=false}"
if [[ "$PACT_PUBLISH_CONTRACTS" != "true" ]] ; then
  echo "INFO: Publishing contracts to broker is disabled.."
  exit 0
fi

PACT_CONSUMER_VERSION=$(git describe --tags)
: "${PACT_CONSUMER_VERSION:=0.0_local_default}"
: "${PACT_BROKER_HOST:=http://pact-broker-shared-v1.ffxblue.com.au}"

for file in ${PACT_CONTRACTS_PATH}*.json; do
  pact_post_url
  if [[ "$?" != 1  ]] ; then
    publish_contract
  fi
done
