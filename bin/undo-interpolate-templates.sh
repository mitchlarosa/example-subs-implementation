#!/usr/bin/env bash
set -euo pipefail

script_dir=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Import helper functions
. ${script_dir}/functions/io-helpers.sh

formatted_echo "red" "\n  Removing installed node_modules and other generated files\n"
rm -rf node_modules

repo_name=$(basename "$PWD")

formatted_echo "red" "\n\n  Please note that this will DESTROY all changes in your working tree'\n\n  So please 'git add' or otherwise save anything you don't want reverted, then press enter - otherwise Ctrl+C aborts.\n"
read

git reset --hard HEAD
git clean -f -d
git checkout -- .
