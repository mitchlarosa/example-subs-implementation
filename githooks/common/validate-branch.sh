#!/usr/bin/env bash

# While rebasing, the branch name will be temporarily 'HEAD', so ignore the branch name while rebasing
if [ -d ".git/rebase-merge" -o -d ".git/rebase-apply" ]; then
  echo "Currently rebasing, ignoring branch name"
  exit 0
fi

shopt -s extglob

current_branch=`git rev-parse --abbrev-ref HEAD`

# Disallow 'master' and 'release/<anything>' branches being committed to directly
echo "Validating that the current branch is a feature branch"
case "$current_branch" in
master) true;;
*) false;;
esac && echo -e "\n#########################################################################" \
                "\n# REJECTED: '$current_branch' is not a feature or bug branch.           #" \
                "\n#                                                                       #" \
                "\n# Do not develop code directly on 'master' branch                       #" \
                "\n#########################################################################" && exit 1

# Disallow branch names that do not start with a JIRA id ('feature/' or 'bug/' folder optional)
case "$current_branch" in
@(feature/|bug/)+([A-Z])-+([0-9])-*) false;;
*) true;;
esac && echo -e "\n########################################################################" \
                "\n# REJECTED: '$current_branch' is not a valid feature branch name.      #" \
                "\n#                                                                      #" \
                "\n# Feature branch names must include:                                   #" \
                "\n# * The JIRA ID being worked on                                        #" \
                "\n# * A short description of the changeset; and                          #" \
                "\n# * Be located at the root, or under one of:                           #" \
                "\n#     a. 'feature' parent folder                                       #" \
                "\n#     b. 'bug' parent folder                                           #" \
                "\n#                                                                      #" \
                "\n# e.g. 'feature/AUD-1234-social-sharing' or                            #" \
                "\n#      'bug/AUD-1234-social-sharing'                                   #" \
                "\n########################################################################" && exit 1

echo "Branch name is valid"
exit 0