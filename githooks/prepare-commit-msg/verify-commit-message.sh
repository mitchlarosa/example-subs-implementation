#!/usr/bin/env bash

# If this is a merge commit, then different rules apply. For now, skip commit message checks
if [ -f ".git/MERGE_MSG" ]; then
  echo "Currently merging, skipping commit message checks"
  exit 0
fi

# Add JIRA ticket number to commit message
echo "Verifying commit message format"

COMMIT_FILE=.git/COMMIT_EDITMSG
COMMIT_MSG=$(cat $COMMIT_FILE)
CURRENT_BRANCH=$(git symbolic-ref --short HEAD)
JIRA_ID=$(echo "$CURRENT_BRANCH" | grep -Eo "[A-Z0-9]{1,10}-?[A-Z0-9]+-\d+")

if [[ "$COMMIT_MSG" == *"$JIRA_ID"* ]]; then
  echo "Commit message already contains $JIRA_ID, no modification required."
elif [ ! -z "$JIRA_ID" ]; then
  echo "$JIRA_ID: $COMMIT_MSG" > $COMMIT_FILE
  echo "JIRA ID '$JIRA_ID', matched in current branch name, prepended to commit message. (Use --no-verify to skip)"  
fi