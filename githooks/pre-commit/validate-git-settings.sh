#!/usr/bin/env bash

# Ensure that the user name and email have been set
function check_user_config()
{
    local git_username=`git config user.name`
    local git_email=`git config user.email`

    if [ "$git_username" == "" ]; then
            printf "Git username has not been set\n"
            printf "Please configure your git username:\n"
            printf " * Globally (all repos) with 'git config --global user.name \"John Doe\"; or\n"
            printf " * Locally (this repo) with 'git config user.name \"John Doe\"'\n"
            exit 1
    fi
    if [ "$git_email" == "" ]; then
            printf "Git email address has not been set\n"
            printf "Please configure your git email address:\n"
            printf " * Globally (all repos) with 'git config --global user.email johndoe@example.com; or\n"
            printf " * Locally (this repo) with 'git config user.email johndoe@example.com'\n"
            exit 1
    fi
}

# Run each of the Git setting checks
check_user_config