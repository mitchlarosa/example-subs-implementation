/* eslint-disable import/no-extraneous-dependencies */
import 'isomorphic-fetch'

import * as path from 'path';
import * as dotenv from "dotenv";
import { Pact } from '@pact-foundation/pact'

const dotenvPath = path.resolve('./.env.pact')

dotenv.config({ path: dotenvPath })

export const apiFrontProvider = new Pact({
  port: 3333,
  dir: path.resolve(process.cwd(), 'tests/generated/contracts'),
  log: path.resolve(process.cwd(), 'tests/generated/logs', 'consumer.log'),
  consumer: 'skeleton-nodejs-application',
  provider: 'api-front',
  pactfileWriteMode: 'merge',
})


