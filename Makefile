SHELL:=/bin/bash
GIT_TAG := $$(git describe --tags 2>/dev/null || echo "nil")
GIT_HASH := $$(git rev-parse --short HEAD 2>/dev/null || echo "nil")

check-shell:
	./bin/shellcheck.sh

env:
	nodenv install -s
	cp .env.example .env
	make set-build-version

init-repo:
	./bin/interpolate-templates.sh

init-repo-undo:
	./bin/undo-interpolate-templates.sh

set-build-version:
	echo $(GIT_TAG) > .version-git-tag
	echo $(GIT_HASH) > .version-git-hash

.PHONY: check-shell env init-repo init-repo-undo set-build-version
