const path = require('path') // eslint-disable-line import/no-extraneous-dependencies

const rootPath = path.resolve(__dirname, '../../../')
const srcPath = path.resolve(__dirname, '../../../app/src')
const nodeModulesPath = path.resolve(__dirname, '../../../node_modules')
const isDev = process.env.NODE_ENV !== 'production'

module.exports = {
  isDev,
  rootPath,
  srcPath,
  defaultConfig: {
    devtool: isDev ? 'eval-source-map' : false,

    mode: isDev ? 'development' : 'production',

    module: {
      rules: [
        {
          test: /\.css$/,
          include: [nodeModulesPath],
          use: [
            {
              loader: 'css-loader',
              options: {
                localIdentName: isDev ? '[name]-[hash:base64:5]' : '[hash:base64:5]',
              },
            },
          ],
        },
      ],
    },

    optimization: {
      splitChunks: {
        cacheGroups: {
          vendor: {
            test: /[\\/]node_modules[\\/](@babel\/runtime|abortcontroller-polyfill|apollo-client|apollo-link|apollo-link-dedup|apollo-utilities|axios|babel-runtime|core-js|create-react-context|fast-json-stable-stringify|fbjs|graphql|gud|history|hoist-non-react-statics|immutable-tuple|lodash.isequal|object-assign|optimism|process|prop-types|react|react-apollo|react-dom|react-is|react-router|react-router-dom|resolve-pathname|scheduler|symbol-observable|tiny-invariant|ts-invariant|tslib|value-equal|zen-observable)[\\/]/, // eslint-disable-line max-len
            name: 'vendor',
            chunks: 'all',
          },
        },
      },
    },

    plugins: [],

    resolve: {
      extensions: ['.graphql', '.js', '.json', '.ts', '.tsx'],
      modules: [srcPath, 'node_modules'],
    },
  },
}
