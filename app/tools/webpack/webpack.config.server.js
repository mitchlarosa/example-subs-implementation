const path = require('path') // eslint-disable-line import/no-extraneous-dependencies
const webpack = require('webpack')
const webpackNodeExternals = require('webpack-node-externals')
const CopyPlugin = require('copy-webpack-plugin')
const { defaultConfig, isDev, rootPath, srcPath } = require('./webpack.config.default')

const buildPath = path.resolve(__dirname, '../../../build/public')

module.exports = {
  ...defaultConfig,

  context: srcPath,

  entry: {
    server: path.resolve(rootPath, 'app/src/server/server.tsx'),
  },

  output: {
    path: buildPath,
    filename: 'server.js',
  },

  module: {
    ...defaultConfig.module,
    rules: [
      ...defaultConfig.module.rules,
      {
        test: /\.j?t?sx?$/,
        loader: 'babel-loader',
        include: [srcPath],
        options: {
          envName: 'server',
        },
      },
      {
        test: /\.(eot|gif|ico|jpg|jpeg|png|svg|ttf|woff(2)?)(\?[a-z0-9]+)?$/,
        use: {
          loader: 'file-loader',
          options: {
            emitFile: false,
          },
        },
      },
    ],
  },

  target: 'node',

  externals: [/^\.\/assets\.json$/, webpackNodeExternals()],

  node: {
    console: false,
    global: false,
    process: false,
    Buffer: false,
    __filename: false,
    __dirname: false,
  },

  plugins: [
    ...defaultConfig.plugins,

    new webpack.DefinePlugin({
      'process.env.NODE_ENV': isDev ? '"development"' : '"production"',
      'process.env.BROWSER': false,
    }),

    new webpack.optimize.LimitChunkCountPlugin({ maxChunks: 1 }),

    new CopyPlugin({
      patterns: [
        {
          from: require.resolve('jaeger-client/dist/src/jaeger-idl/thrift/jaeger.thrift'),
          to: 'jaeger-idl/thrift/jaeger.thrift',
        },
        {
          from: require.resolve('jaeger-client/dist/src/jaeger-idl/thrift/jaeger.thrift'),
          to: '../jaeger-idl/thrift/jaeger.thrift',
        },
        {
          from: require.resolve('jaeger-client/dist/src/thriftrw-idl/agent.thrift'),
          to: '../thriftrw-idl/agent.thrift',
        },
      ],
    }),
  ],
}
