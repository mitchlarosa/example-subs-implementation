const path = require('path') // eslint-disable-line import/no-extraneous-dependencies
const AssetsPlugin = require('assets-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const { defaultConfig, isDev, rootPath, srcPath } = require('./webpack.config.default')

const buildPath = path.resolve(__dirname, '../../../build/public/assets')
const isBundleAnalyzer = process.argv.includes('--bundle-analyzer')

module.exports = {
  ...defaultConfig,

  context: srcPath,

  entry: {
    client: path.resolve(rootPath, 'app/src/client/client.tsx'),
  },

  output: {
    path: buildPath,
    publicPath: '/assets/',
    filename: isDev ? '[name].js' : '[name].[chunkhash].js',
    chunkFilename: isDev ? '[name].chunk.js' : '[name].[chunkhash].chunk.js',
  },

  externals: [/^\.\/assets\.json$/],

  module: {
    ...defaultConfig.module,
    rules: [
      ...defaultConfig.module.rules,
      {
        test: /\.j?t?sx?$/,
        loader: 'babel-loader',
        include: [srcPath],
        options: {
          envName: 'client',
        },
      },
      {
        test: /\.(eot|gif|ico|jpg|jpeg|png|svg|ttf|woff(2)?)(\?[a-z0-9]+)?$/,
        use: {
          loader: 'file-loader',
          options: {
            name: isDev ? '[path][name].[ext]?[hash:8]' : '[name].[hash:8].[ext]',
          },
        },
      },
    ],
  },

  plugins: [
    ...defaultConfig.plugins,

    new AssetsPlugin({
      path: buildPath,
      filename: 'assets.json',
    }),

    ...(isBundleAnalyzer
      ? [
          new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            // Path to bundle report file that will be generated in `static` mode.
            // Relative to bundles output directory.
            reportFilename: path.resolve(__dirname, '../report.html'),
            // If `true`, automatically open report in default browser
            openAnalyzer: false,
            // If `true`, Webpack Stats JSON file will be generated in bundles output directory
            generateStatsFile: true,
            // Name of Webpack Stats JSON file that will be generated if `generateStatsFile` is `true`.
            // Relative to bundles output directory.
            statsFilename: path.resolve(__dirname, '../stats.json'),
            // Log level. Can be 'info', 'warn', 'error' or 'silent'.
            logLevel: 'info',
          }),
        ]
      : []),
  ],

  stats: {
    colors: true,
    timings: true,
  },
}
