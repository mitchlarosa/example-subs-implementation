/* eslint-disable import/no-extraneous-dependencies */
require('dotenv').config()

const browserSync = require('browser-sync')
const webpackMiddleware = require('webpack-dev-middleware')
const webpack = require('webpack')
const WriteFilePlugin = require('write-file-webpack-plugin')
const clientWebpackConfig = require('../webpack/webpack.config.client')
const serverWebpackConfig = require('../webpack/webpack.config.server')
const runServer = require('./develop')

async function start() {
  await new Promise((resolve) => {
    serverWebpackConfig.plugins.push(new WriteFilePlugin({ log: false }))

    const clientJS = clientWebpackConfig.entry.client
    clientWebpackConfig.entry.client = [clientJS]
    clientWebpackConfig.plugins.push(new webpack.NoEmitOnErrorsPlugin())

    const bundler = webpack([clientWebpackConfig, serverWebpackConfig])
    const wpMiddleware = webpackMiddleware(bundler, {
      // webpack middleware can't access config,
      // so we should provide publicPath by ourselves
      publicPath: clientWebpackConfig.output.publicPath,

      // Pretty colored output
      stats: clientWebpackConfig.stats,
    })

    let handleBundleComplete = async () => {
      handleBundleComplete = (stats) => !stats.stats[1].compilation.errors.length && runServer()

      const server = await runServer()
      const bs = browserSync.create()

      bs.init(
        {
          proxy: {
            target: server.host,
            middleware: [wpMiddleware],
            proxyOptions: {
              xfwd: true,
            },
          },
        },
        resolve
      )
    }

    bundler.plugin('done', (stats) => handleBundleComplete(stats))
  })
}

start()
