const webpack = require('webpack') // eslint-disable-line import/no-extraneous-dependencies
const webpackClientConfig = require('../webpack/webpack.config.client')
const webpackServerConfig = require('../webpack/webpack.config.server')

const webpackConfigs = [webpackClientConfig, webpackServerConfig]

function build(configs) {
  return new Promise((resolve, reject) =>
    webpack(configs).run((err, stats) => {
      if (err) {
        return reject(err)
      }

      console.log(stats.toString(configs[0].stats)) // eslint-disable-line no-console
      return resolve()
    })
  )
}

build(webpackConfigs)
