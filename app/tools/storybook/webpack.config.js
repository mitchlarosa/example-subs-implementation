const path = require('path') // eslint-disable-line import/no-extraneous-dependencies
const clientConfig = require('../webpack/webpack.config.client')
const { defaultConfig } = require('../webpack/webpack.config.default')

module.exports = async ({ config }) => ({
  ...config,
  devtool: clientConfig.devtool,
  mode: clientConfig.mode,
  module: {
    ...config.module,
    rules: clientConfig.module.rules,
  },
  optimization: { ...config.optimization, ...clientConfig.optimization },
  plugins: [...config.plugins, ...defaultConfig.plugins],
  resolve: {
    ...config.resolve,
    ...clientConfig.resolve,
    alias: {
      ...config.resolve.alias,
      ...clientConfig.resolve.alias,
      'config/environment/environment': path.resolve(__dirname, '../../src/config/environment/environment.default.js'),
    },
  },
})
