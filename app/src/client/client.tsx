import React from 'react'
import { render } from 'react-dom'
import App from '../templates/App'
import { RENDER_NODE } from '../templates/Html/Html'

const renderNode = document.getElementById(RENDER_NODE)

if (renderNode) {
  render(<App />, renderNode)
}
