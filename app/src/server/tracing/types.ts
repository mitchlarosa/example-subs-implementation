import { Request } from 'express'
import { Span } from 'opentracing'

export interface TracedRequest extends Request {
  span: Span
}
