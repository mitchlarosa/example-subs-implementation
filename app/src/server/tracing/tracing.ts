import { createTracingMiddleware } from '@ffxblue/library-nodejs-middlewares/build/middlewares/tracing'
import environment from 'config/environment/environment'

const {
  TRACING: { HOST, PORT, SAMPLING_RATE, SERVICE_NAME },
} = environment

const tracingMiddleware = createTracingMiddleware({
  host: HOST,
  port: PORT,
  samplingRate: SAMPLING_RATE,
  serviceName: SERVICE_NAME,
})

export { tracingMiddleware }
