import http from 'http'
import path from 'path'
import express, { NextFunction, Request, Response } from 'express'
import { globalTracer } from 'opentracing'
import React from 'react'
import { renderToString } from 'react-dom/server'
import { destroy } from 'unleash-client'
import cookieParser from 'cookie-parser'
import { log } from '@ffxblue/library-js-logging/build/shared'
import middlewares from '@ffxblue/library-nodejs-middlewares/build'
import environmentVariablesClient from 'config/environment/variables.client'
import environment from 'config/environment/environment'
import initFeatureFlags, { getFeatureFlags, isFeatureFlagsRegistered } from 'config/featureflags/featureflags.server'
import { FEATURE_FLAGS, FeatureFlags } from 'config/featureflags/featureflags.default'
import { Logger, loggerMiddleware } from 'config/logger/logger'
import Html from 'templates/Html/Html'
import appInternal from './internal/server'
import { metricsMiddleware } from './metrics/metrics'
import { tracingMiddleware } from './tracing/tracing'
import { TracedRequest } from './tracing/types'

interface InitialState {
  featureFlags?: FeatureFlags
}

// @ts-ignore
const assets = __non_webpack_require__('./assets/assets.json') // eslint-disable-line
const { APP_NAME, HTTP_PORT, HTTP_PORT_INTERNAL, HTTP_SERVER_CLOSE_TIMEOUT, NODE_ENV } = environment
const isDev = NODE_ENV !== 'production'
const app = express()

const AUTH_API_URL = 'https://api-dev.ffx.io'
const AUTH_SECRET = './.local-secrets/auth.public.key'

//
// Static files
// -----------------------------------------------------------------------------

app.use(
  express.static(path.join(__dirname, '/'), {
    setHeaders: (res) => {
      // 31557600 = 365.25 days
      res.set('Cache-Control', 'public, max-age=31557600')
    },
    redirect: false,
  })
)

//
// Logging
// -----------------------------------------------------------------------------

app.use(loggerMiddleware)

//
// Metrics
// -----------------------------------------------------------------------------
app.use(metricsMiddleware)

//
// Tracing
// -----------------------------------------------------------------------------
app.use(tracingMiddleware)

//
// Feature flags
// -----------------------------------------------------------------------------

const featureFlags = initFeatureFlags()
const isFeatureFlagsEnabled = featureFlags.isEnabled.bind(featureFlags)

const { authenticate, editorial } = middlewares

app.use(cookieParser())

app.use(
  editorial({
    isEditorial: true,
  })
)

app.use(
  authenticate({
    authSecret: AUTH_SECRET,
    authURL: AUTH_API_URL,
    logHandler: ({ message }: { message: string }) => {
      Logger.error('Failed request', log.EventType('middleware', 'error'), log.Error(message))
    },
  })
)

//
// Routes
// -----------------------------------------------------------------------------
app.get(
  '*',
  async (req: TracedRequest, res: express.Response, next: express.NextFunction): Promise<void> => {
    const tracer = globalTracer()
    const { span } = req
    try {
      const initialState: InitialState = {}

      // Get feature flags
      if (isFeatureFlagsEnabled) {
        const flags = isFeatureFlagsRegistered
          ? getFeatureFlags(FEATURE_FLAGS, isFeatureFlagsEnabled, req.query.featureFlagsOverrides)
          : FEATURE_FLAGS

        initialState.featureFlags = { ...flags }
      }

      const renderToStringSpan = tracer.startSpan('renderToString', { childOf: span })
      const html = renderToString(
        <Html assets={assets} environment={environmentVariablesClient} initialState={initialState} />
      )
      renderToStringSpan.finish()

      res.status(200).send(html)
    } catch (err) {
      next(err)
    }
  }
)

app.use((err: Error, req: Request, res: Response, next: NextFunction): void => {
  Logger.error('Fail request', log.EventType('middleware', 'error'), log.Error(err))
  res.status(500).send(`<!doctype html>${err.message}`)
  next(err)
})

//
// Start servers
// -----------------------------------------------------------------------------
let server: http.Server
let internalServer: http.Server

if (HTTP_PORT) {
  server = app
    .listen(HTTP_PORT, (): void => {
      Logger.info(`${APP_NAME} started`, log.EventType('app', 'start'), log.Int('port', HTTP_PORT))

      if (isDev) {
        console.log(`The server is running at http://localhost:${HTTP_PORT}/`) // eslint-disable-line no-console
      }
    })
    .on('error', (error) => {
      Logger.error(`${APP_NAME} failed to start`, log.EventType('app', 'start'), log.Error(error))
    })
}

if (HTTP_PORT_INTERNAL) {
  internalServer = appInternal
    .listen(HTTP_PORT_INTERNAL, (): void => {
      Logger.info(`${APP_NAME} internal started`, log.EventType('app', 'start'), log.Int('port', HTTP_PORT_INTERNAL))

      if (isDev) {
        console.log(`Internal endpoints are available at http://localhost:${HTTP_PORT_INTERNAL}/`) // eslint-disable-line no-console
      }
    })
    .on('error', (error) => {
      Logger.error(`${APP_NAME} internal failed to start`, log.EventType('app', 'start'), log.Error(error))
    })
}

//
// Shutdown servers
// -----------------------------------------------------------------------------
const sigs: NodeJS.Signals[] = ['SIGINT', 'SIGTERM']

sigs.forEach((sig): void => {
  process.on(sig, async () => {
    let serverOrError: http.Server | Error
    let internalServerOrError: http.Server | Error

    Logger.info('Graceful shutting triggered', log.EventType('app', 'exit'))

    setTimeout(() => {
      const err = new Error('Servers failed to close')
      Logger.error(err.message, log.EventType('app', 'exit'), log.Error(err))
      process.exit(1)
    }, HTTP_SERVER_CLOSE_TIMEOUT).unref()

    if (internalServer) {
      internalServerOrError = await internalServer.close()

      if (internalServerOrError instanceof Error) {
        Logger.error('Internal server failed to close', log.EventType('app', 'exit'), log.Error(internalServerOrError))
      }
    }

    if (server) {
      serverOrError = await server.close()

      if (serverOrError instanceof Error) {
        Logger.error('Server failed to close', log.EventType('app', 'exit'), log.Error(serverOrError))
      }
    }

    destroy()

    if (internalServerOrError instanceof Error || serverOrError instanceof Error) {
      process.exit(1)
    }

    Logger.info('Servers closed', log.EventType('app', 'exit'))
    process.exit(0)
  })
})
