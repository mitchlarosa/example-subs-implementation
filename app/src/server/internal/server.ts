import express from 'express'
import { createMetricsHandler } from '@ffxblue/library-nodejs-middlewares/build/middlewares/metrics'
import { metricsMiddleware } from 'server/metrics/metrics'
import {
  INTERNAL_HEALTH_PATH,
  INTERNAL_METRICS_PATH,
  INTERNAL_PING_PATH,
  INTERNAL_READINESS_PATH,
  INTERNAL_SERVICE_INFO_PATH,
} from 'server/metrics/constants'
import healthCheck from './routes/healthCheck'
import ping from './routes/ping'
import readiness from './routes/readiness'
import serviceInfo from './routes/serviceInfo'

//
// Create internal server
// -----------------------------------------------------------------------------
// Create a second app to separate internal endpoints from public endpoints.
// This app should only provide endpoints required for infrastructure.
const appInternal = express()

appInternal.use(metricsMiddleware)
appInternal.get(INTERNAL_HEALTH_PATH, healthCheck())
appInternal.get(INTERNAL_METRICS_PATH, createMetricsHandler())
appInternal.get(INTERNAL_PING_PATH, ping())
appInternal.get(INTERNAL_READINESS_PATH, readiness())
appInternal.get(INTERNAL_SERVICE_INFO_PATH, serviceInfo())

export default appInternal
