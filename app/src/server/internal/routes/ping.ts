import { Request, Response } from 'express'

// Ping returns a simple 'PONG' response indicating that the service is available
// This should be used by other services to test connectivity rather than invoking the application's health
// endpoint (which should be reserved for infrastructure use).
function ping() {
  return (req: Request, res: Response): void => {
    res.status(200).send('PONG')
  }
}
export default ping
