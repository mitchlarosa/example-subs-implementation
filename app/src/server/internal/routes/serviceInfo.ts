import fs from 'fs'
import { Request, Response } from 'express'
import { log } from '@ffxblue/library-js-logging/build/shared'
import { Logger } from 'config/logger/logger'

let version = ''

try {
  version = fs.readFileSync('.version-git-tag', 'utf8').trim()
} catch (err) {
  Logger.error('Fail to read .version-git-tag', log.EventType('app', 'start'), log.Error(err))
}

let gitCommit = ''

try {
  gitCommit = fs.readFileSync('.version-git-hash', 'utf8').trim()
} catch (err) {
  Logger.error('Fail to read .version-git-hash', log.EventType('app', 'start'), log.Error(err))
}

function serviceInfo() {
  return (req: Request, res: Response): void => {
    res.status(200).json({ build: { gitCommit, version } })
  }
}

export default serviceInfo
