import { Request, Response } from 'express'

function readiness() {
  return (req: Request, res: Response): void => {
    res.status(200).json({})
  }
}

export default readiness
