import { Request, Response } from 'express'

function healthCheck() {
  return (req: Request, res: Response): void => {
    res.status(200).json({})
  }
}

export default healthCheck
