import express from 'express'
import { createDefaultMetricsMiddleware } from '@ffxblue/library-nodejs-middlewares/build/middlewares/metrics'
import {
  HEALTH,
  HOMEPAGE,
  HOMEPAGE_PATH,
  INTERNAL_HEALTH_PATH,
  INTERNAL_METRICS_PATH,
  INTERNAL_PING_PATH,
  INTERNAL_READINESS_PATH,
  INTERNAL_SERVICE_INFO_PATH,
  METRICS,
  PING,
  READINESS,
  SERVICE_INFO,
} from './constants'

const isPathMatch = (req: express.Request, pathname: string): boolean =>
  req.path.substr(0, pathname.length) === pathname

const getHandlerValue = (req: express.Request): string | undefined => {
  let handler

  if (isPathMatch(req, INTERNAL_HEALTH_PATH)) {
    handler = HEALTH
  } else if (isPathMatch(req, INTERNAL_METRICS_PATH)) {
    handler = METRICS
  } else if (isPathMatch(req, INTERNAL_PING_PATH)) {
    handler = PING
  } else if (isPathMatch(req, INTERNAL_READINESS_PATH)) {
    handler = READINESS
  } else if (isPathMatch(req, INTERNAL_SERVICE_INFO_PATH)) {
    handler = SERVICE_INFO
  } else if (isPathMatch(req, HOMEPAGE_PATH)) {
    handler = HOMEPAGE
  }

  return handler
}

// pass options for each specific metric middleware
const metricsMiddleware = createDefaultMetricsMiddleware({
  requestCount: { getHandlerValue },
  requestInFlight: { getHandlerValue },
  requestSize: { getHandlerValue },
  responseSize: { getHandlerValue },
  responseTime: { getHandlerValue },
})

export { metricsMiddleware }
