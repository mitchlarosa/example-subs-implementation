import React, { ReactElement } from 'react'
import { storiesOf } from '@storybook/react'
import { MemoryRouter } from 'react-router-dom'
import { StoryApi } from '@storybook/addons'

export const routerStoriesOf = (name: string, module: NodeModule): StoryApi =>
  storiesOf(name, module).addDecorator((story): ReactElement => <MemoryRouter>{story()}</MemoryRouter>)
