import { gql } from '@apollo/client'

const variables = {
  input: {
    assignee: 'ALL',
    brandIds: [
      'QnJhbmQ6ImJyaXNiYW5ldGltZXMi',
      'QnJhbmQ6ImNhbmJlcnJhdGltZXMi',
      'QnJhbmQ6InNtaCI=',
      'QnJhbmQ6InRoZWFnZSI=',
      'QnJhbmQ6IndhdG9kYXki',
    ],
    datePreset: 'RANGE',
    newsroomIds: ['TmV3c3Jvb206Im1ldHJvIg=='],
    first: 20,
    includePublic: true,
    query: 'Test headline',
  },
}

const EditorialAssetPreviewFragment = gql`
  fragment Asset on EditorialAssetPreview {
    brands {
      name
    }
    byline
    category {
      name
    }
    dates {
      created
      firstPublished
      modified
      published
      saved
      timeToTakeDown
    }
    headlines {
      headline
    }
    images {
      landscape3x2 {
        altText
        animated
        crop {
          offsetX
          offsetY
          width
          zoom
        }
        mediaId
        mimeType
        type
      }
      landscape16x9 {
        altText
        animated
        crop {
          offsetX
          offsetY
          width
          zoom
        }
        mediaId
        mimeType
        type
      }
      portrait2x3 {
        altText
        animated
        crop {
          offsetX
          offsetY
          width
          zoom
        }
        mediaId
        mimeType
        type
      }
      square1x1 {
        altText
        animated
        crop {
          offsetX
          offsetY
          width
          zoom
        }
        mediaId
        mimeType
        type
      }
    }
    listing {
      assignee
      brief
      dates {
        deadline
      }
      legalStatus
      lockedBy
      printReady
    }
    overview {
      about
      wordCount
    }
    participants {
      authors {
        name
        # profiles {
        #   avatar
        # }
      }
    }
    shortId
    sourceCms
    state {
      editing
      public
    }
    tags {
      primary {
        displayName
      }
    }
    type
    url {
      canonical {
        brand {
          name
        }
        path
      }
    }
  }
`

const editorialAssetSearch = gql`
  query EditorialAssetSearch($input: EditorialAssetSearchInput!) {
    editorialAssetSearch(input: $input) {
      edges {
        node {
          asset {
            ...Asset
          }
        }
      }
      error {
        message
        type {
          class
          ... on ErrorTypeForbidden {
            class
          }
          ... on ErrorTypeInvalidAuth {
            code
          }
          ... on ErrorTypeInvalidRequest {
            fields {
              field
              message
            }
          }
          ... on ErrorTypeNotFound {
            class
          }
          ... on ErrorTypeUnavailable {
            retryable
          }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
      totalCount
    }
  }

  ${EditorialAssetPreviewFragment}
`

export { EditorialAssetPreviewFragment, variables }
export default editorialAssetSearch
