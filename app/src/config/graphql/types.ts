export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export interface Scalars {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  Time: any;
}

export interface Account {
   __typename?: 'Account';
  autoplay: Scalars['Boolean'];
  error?: Maybe<AccountError>;
  id: Scalars['ID'];
  location?: Maybe<AccountLocation>;
  onboarding?: Maybe<AccountOnboarding>;
}

export interface AccountCorporateCreateError  extends Error {
   __typename?: 'AccountCorporateCreateError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface AccountCorporateCreateInput {
  accountName: Scalars['String'];
  domains: Array<Scalars['String']>;
  packageID: Scalars['String'];
}

export interface AccountCorporateCreateResponse {
   __typename?: 'AccountCorporateCreateResponse';
  error?: Maybe<AccountCorporateCreateError>;
  id: Scalars['ID'];
}

export interface AccountCorporateError  extends Error {
   __typename?: 'AccountCorporateError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface AccountCorporateResponse {
   __typename?: 'AccountCorporateResponse';
  account?: Maybe<CorporateAccount>;
  error?: Maybe<AccountCorporateError>;
}

export enum AccountCorporateRoleType {
  AccountAdmin = 'ACCOUNT_ADMIN',
  Member = 'MEMBER',
  SuperAdmin = 'SUPER_ADMIN'
}

export interface AccountCorporateSearchError  extends Error {
   __typename?: 'AccountCorporateSearchError';
  message: Scalars['String'];
  type: ErrorType;
}

export enum AccountCorporateSearchOrder {
  CreatedAsc = 'CREATED_ASC',
  CreatedDesc = 'CREATED_DESC',
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  UpdatedAsc = 'UPDATED_ASC',
  UpdatedDesc = 'UPDATED_DESC'
}

export interface AccountCorporateSearchResponse {
   __typename?: 'AccountCorporateSearchResponse';
  accounts: Array<CorporateAccount>;
  error?: Maybe<AccountCorporateSearchError>;
  offset: Scalars['Int'];
  total: Scalars['Int'];
}

export interface AccountCorporateSearchTermInput {
  name: Scalars['String'];
}

export interface AccountCorporateUpdateError  extends Error {
   __typename?: 'AccountCorporateUpdateError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface AccountCorporateUpdateInput {
  accountId: Scalars['ID'];
  domains: Array<Scalars['String']>;
}

export interface AccountCorporateUpdateResponse {
   __typename?: 'AccountCorporateUpdateResponse';
  error?: Maybe<AccountCorporateUpdateError>;
  id: Scalars['ID'];
}

export interface AccountError {
   __typename?: 'AccountError';
  message: Scalars['String'];
  type: AccountErrorType;
}

export enum AccountErrorType {
  AccountNotFoundInDatabase = 'ACCOUNT_NOT_FOUND_IN_DATABASE',
  AuthenticationError = 'AUTHENTICATION_ERROR',
  GenericError = 'GENERIC_ERROR',
  InvalidRequestError = 'INVALID_REQUEST_ERROR'
}

export interface AccountLocation {
   __typename?: 'AccountLocation';
  postCode?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  suburb?: Maybe<Scalars['String']>;
}

export interface AccountMember {
   __typename?: 'AccountMember';
  accountId: Scalars['ID'];
  costCentre: Scalars['String'];
  role: AccountCorporateRoleType;
}

export interface AccountOnboarding {
   __typename?: 'AccountOnboarding';
  newsfeed: Scalars['Boolean'];
  tags: Scalars['Boolean'];
}

export interface AmpLinkMemberReply {
   __typename?: 'AMPLinkMemberReply';
  error?: Maybe<MeteringError>;
  success: Scalars['Boolean'];
}

export interface AppleStoreBundle {
   __typename?: 'AppleStoreBundle';
  id: Scalars['ID'];
}

export enum AppleStoreEnvironment {
  Production = 'PRODUCTION',
  Sandbox = 'SANDBOX'
}

export interface AppleStoreLinkingError  extends Error {
   __typename?: 'AppleStoreLinkingError';
  message: Scalars['String'];
  type: ErrorType;
}

export enum AppleStoreLinkingStatus {
  Linked = 'LINKED',
  Pending = 'PENDING'
}

export interface AppleStoreProduct {
   __typename?: 'AppleStoreProduct';
  id: Scalars['ID'];
  key: Scalars['String'];
}

export interface AppleStoreReceipt {
   __typename?: 'AppleStoreReceipt';
  bundle: AppleStoreBundle;
  subscriptions: Array<AppleStoreSubscription>;
}

export interface AppleStoreSubscription {
   __typename?: 'AppleStoreSubscription';
  discounts: AppleStoreSubscriptionDiscounts;
  expiry: AppleStoreSubscriptionExpiry;
  purchase: AppleStoreSubscriptionPurchase;
  renewal?: Maybe<AppleStoreSubscriptionRenewal>;
  status: AppleStoreSubscriptionStatus;
}

export interface AppleStoreSubscriptionDiscounts {
   __typename?: 'AppleStoreSubscriptionDiscounts';
  introOffer?: Maybe<AppleStoreSubscriptionIntroOffer>;
  trial?: Maybe<AppleStoreSubscriptionTrial>;
}

export interface AppleStoreSubscriptionExpiry {
   __typename?: 'AppleStoreSubscriptionExpiry';
  date: Scalars['Time'];
}

export interface AppleStoreSubscriptionIntroOffer {
   __typename?: 'AppleStoreSubscriptionIntroOffer';
  status: AppleStoreSubscriptionIntroOfferStatus;
}

export enum AppleStoreSubscriptionIntroOfferStatus {
  Active = 'ACTIVE',
  Expired = 'EXPIRED'
}

export interface AppleStoreSubscriptionPurchase {
   __typename?: 'AppleStoreSubscriptionPurchase';
  date: Scalars['Time'];
  originalTransaction: AppleStoreTransaction;
  product: AppleStoreProduct;
  transaction: AppleStoreTransaction;
}

export interface AppleStoreSubscriptionRenewal {
   __typename?: 'AppleStoreSubscriptionRenewal';
  auto: Scalars['Boolean'];
  currentProduct: AppleStoreProduct;
  newProduct: AppleStoreProduct;
}

export enum AppleStoreSubscriptionStatus {
  Active = 'ACTIVE',
  Expired = 'EXPIRED',
  Grace = 'GRACE'
}

export interface AppleStoreSubscriptionTrial {
   __typename?: 'AppleStoreSubscriptionTrial';
  status: AppleStoreSubscriptionTrialStatus;
}

export enum AppleStoreSubscriptionTrialStatus {
  Active = 'ACTIVE',
  Expired = 'EXPIRED'
}

export interface AppleStoreTransaction {
   __typename?: 'AppleStoreTransaction';
  id: Scalars['ID'];
}

export enum ArchiveStatusExcludeType {
  Active = 'ACTIVE',
  Archived = 'ARCHIVED'
}

export interface Asset {
   __typename?: 'Asset';
  about: Scalars['String'];
  assetType: Scalars['String'];
  asset: AssetData;
  brands?: Maybe<Array<Maybe<Scalars['String']>>>;
  categories?: Maybe<Array<Maybe<Scalars['String']>>>;
  category?: Maybe<AssetCategory>;
  commentsConnection: CommentsConnection;
  dates: AssetDates;
  editingState: Scalars['String'];
  intro?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
  legalStatus: Scalars['String'];
  publicState: Scalars['String'];
  featuredImages?: Maybe<AssetImages>;
  headlines: AssetHeadlines;
  id: Scalars['ID'];
  participants?: Maybe<AssetParticipants>;
  promotedBrand?: Maybe<AssetPromotedBrand>;
  sourceCms?: Maybe<AssetSourceCms>;
  sponsor?: Maybe<AssetSponsor>;
  tags?: Maybe<AssetTag>;
  /** @deprecated Please, use the *comments* query under *asset* to fetch this total comment count. This one is retained for backwards compatibility reasons. */
  totalCommentCount: Scalars['Int'];
  urls?: Maybe<AssetUrLs>;
}


export interface AssetCommentsConnectionArgs {
  limit: Scalars['Int'];
}

export interface AssetCategory {
   __typename?: 'AssetCategory';
  id?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  parentID?: Maybe<Scalars['Int']>;
  urls?: Maybe<AssetCategoryUrLs>;
}

export interface AssetCategoryUrLs {
   __typename?: 'AssetCategoryURLs';
  published?: Maybe<AssetPublishedUrLs>;
}

export interface AssetComment {
   __typename?: 'AssetComment';
  body: Scalars['String'];
  created_at: Scalars['String'];
  id: Scalars['ID'];
  user: Scalars['String'];
}

export interface AssetData {
   __typename?: 'AssetData';
  /** @deprecated Please, use the *about* field at the root level. This one is added for backwards compatibility reasons. */
  about: Scalars['String'];
  byline?: Maybe<Scalars['String']>;
  duration?: Maybe<Scalars['Int']>;
  /** @deprecated Please, use the *headlines* field at the root level. This one is added for backwards compatibility reasons. */
  headlines: AssetHeadlines;
  live?: Maybe<Scalars['Boolean']>;
  totalImages?: Maybe<Scalars['Int']>;
}

export interface AssetDates {
   __typename?: 'AssetDates';
  created: Scalars['String'];
  firstPublished?: Maybe<Scalars['String']>;
  modified: Scalars['String'];
  published: Scalars['String'];
  saved?: Maybe<Scalars['String']>;
  timeToTakeDown?: Maybe<Scalars['String']>;
}

export interface AssetEdge {
   __typename?: 'AssetEdge';
  cursor: Scalars['ID'];
  node?: Maybe<Asset>;
}

export interface AssetFilterByInput {
  categories?: Maybe<Array<Scalars['String']>>;
}

export interface AssetHeadlines {
   __typename?: 'AssetHeadlines';
  headline: Scalars['String'];
}

export interface AssetImages {
   __typename?: 'AssetImages';
  landscape16x9?: Maybe<Image>;
  landscape3x2?: Maybe<Image>;
  portrait2x3?: Maybe<Image>;
  square1x1?: Maybe<Image>;
}

export interface AssetList {
   __typename?: 'AssetList';
  id: Scalars['Int'];
  description?: Maybe<Scalars['String']>;
  itemListElement: Array<Maybe<AssetListEntry>>;
  name: Scalars['String'];
}

export interface AssetListEntry {
   __typename?: 'AssetListEntry';
  id: Scalars['ID'];
  position: Scalars['Int'];
  variations?: Maybe<Variations>;
}

export interface AssetLock {
   __typename?: 'AssetLock';
  isLocked: Scalars['Boolean'];
  userID?: Maybe<Scalars['String']>;
}

export interface AssetLockInput {
  assetID: Scalars['String'];
}

export interface AssetLockReply {
   __typename?: 'AssetLockReply';
  assetID: Scalars['String'];
}

export interface AssetParticipants {
   __typename?: 'AssetParticipants';
  authors?: Maybe<Array<Maybe<Author>>>;
}

export enum AssetPromotedBrand {
  Allhomes = 'ALLHOMES',
  Domain = 'DOMAIN'
}

export interface AssetPublishedUrl {
   __typename?: 'AssetPublishedURL';
  brand: Scalars['String'];
  path: Scalars['String'];
}

export interface AssetPublishedUrLs {
   __typename?: 'AssetPublishedURLs';
  afr?: Maybe<AssetPublishedUrl>;
  brisbanetimes?: Maybe<AssetPublishedUrl>;
  canberratimes?: Maybe<AssetPublishedUrl>;
  smh?: Maybe<AssetPublishedUrl>;
  theage?: Maybe<AssetPublishedUrl>;
  watoday?: Maybe<AssetPublishedUrl>;
}

export interface AssetReadError {
   __typename?: 'AssetReadError';
  message: Scalars['String'];
  type: AssetReadErrorType;
}

export enum AssetReadErrorType {
  AuthenticationError = 'AUTHENTICATION_ERROR',
  GenericError = 'GENERIC_ERROR',
  InvalidRequestError = 'INVALID_REQUEST_ERROR'
}

export interface AssetReadInput {
  assetId: Scalars['String'];
  deviceId?: Maybe<Scalars['String']>;
  newsroom?: Maybe<Newsroom>;
}

export interface AssetReadReference {
   __typename?: 'AssetReadReference';
  assetEdge?: Maybe<AssetEdge>;
  readingHistoryId?: Maybe<Scalars['ID']>;
  shortlistId: Scalars['ID'];
  shortlistItem: ShortlistItem;
}

export interface AssetReadReply {
   __typename?: 'AssetReadReply';
  error?: Maybe<AssetReadError>;
  reference?: Maybe<AssetReadReference>;
}

export interface AssetsConnection {
   __typename?: 'AssetsConnection';
  assets?: Maybe<Array<Maybe<Asset>>>;
  edges?: Maybe<Array<Maybe<AssetEdge>>>;
  pageInfo: PageInfo;
  totalCount?: Maybe<Scalars['Int']>;
}

export interface AssetsConnectionError  extends Error {
   __typename?: 'AssetsConnectionError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface AssetsConnectionResponse {
   __typename?: 'AssetsConnectionResponse';
  edges?: Maybe<Array<AssetEdge>>;
  error?: Maybe<AssetsConnectionError>;
  pageInfo: PageInfo;
}

export enum AssetSortBy {
  LeastRecent = 'LEAST_RECENT',
  MostRecent = 'MOST_RECENT',
  Relevance = 'RELEVANCE'
}

export interface AssetSourceCms {
   __typename?: 'AssetSourceCms';
  cmsType?: Maybe<Scalars['String']>;
}

export interface AssetSponsor {
   __typename?: 'AssetSponsor';
  name?: Maybe<Scalars['String']>;
}

export interface AssetTag {
   __typename?: 'AssetTag';
  /** @deprecated Please use the *primaryTag.displayName* field. */
  primary: Scalars['String'];
  primaryTag?: Maybe<AssetTagDetails>;
  secondary?: Maybe<Array<Maybe<AssetTagDetails>>>;
}

export interface AssetTagDetails {
   __typename?: 'AssetTagDetails';
  context: Scalars['String'];
  displayName: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
  shortID: Scalars['String'];
  slug?: Maybe<Scalars['String']>;
  urls?: Maybe<TagUrLs>;
}

export enum AssetType {
  Article = 'article',
  Bespoke = 'bespoke',
  FeatureArticle = 'featureArticle',
  Gallery = 'gallery',
  LiveArticle = 'liveArticle',
  Url = 'url',
  Video = 'video'
}

export interface AssetUnlockInput {
  assetID: Scalars['String'];
}

export interface AssetUnlockReply {
   __typename?: 'AssetUnlockReply';
  assetID: Scalars['String'];
}

export interface AssetUrl {
   __typename?: 'AssetURL';
  brand: Scalars['String'];
  path: Scalars['String'];
}

export interface AssetUrLs {
   __typename?: 'AssetURLs';
  canonical?: Maybe<AssetUrl>;
  external?: Maybe<Scalars['String']>;
  published?: Maybe<AssetPublishedUrLs>;
}

export enum Assignee {
  DeskEditors = 'DESK_EDITORS',
  Pagemasters = 'PAGEMASTERS'
}

export interface AudienceSegments {
   __typename?: 'AudienceSegments';
  engagementSegment: EngagementSegment;
  type: SegmentType;
}

export interface AudienceSegmentsError  extends Error {
   __typename?: 'AudienceSegmentsError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface AudienceSegmentsResponse {
   __typename?: 'AudienceSegmentsResponse';
  error?: Maybe<AudienceSegmentsError>;
  segments: Array<AudienceSegments>;
}

export interface Author {
   __typename?: 'Author';
  bio: Scalars['String'];
  dates: AuthorDates;
  email?: Maybe<Scalars['String']>;
  featuredImages?: Maybe<AuthorImages>;
  id: Scalars['ID'];
  name: Scalars['String'];
  social?: Maybe<AuthorSocial>;
  title?: Maybe<Scalars['String']>;
  userEmail?: Maybe<Scalars['String']>;
  assetsConnection?: Maybe<AssetsConnection>;
}


export interface AuthorAssetsConnectionArgs {
  brand: Scalars['String'];
  count?: Maybe<Scalars['Int']>;
  since?: Maybe<Scalars['ID']>;
}

export interface AuthorDates {
   __typename?: 'AuthorDates';
  created: Scalars['String'];
  updated?: Maybe<Scalars['String']>;
}

export interface AuthorImages {
   __typename?: 'AuthorImages';
  headshot?: Maybe<Image>;
  woodcut?: Maybe<Image>;
}

export interface AuthorsByIdsError {
   __typename?: 'AuthorsByIdsError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface AuthorsByIdsResponse {
   __typename?: 'AuthorsByIdsResponse';
  authors: Array<Author>;
  error?: Maybe<AuthorsByIdsError>;
}

export interface AuthorSocial {
   __typename?: 'AuthorSocial';
  facebook?: Maybe<AuthorSocialId>;
  googlePlus?: Maybe<AuthorSocialId>;
  twitter?: Maybe<AuthorSocialId>;
}

export interface AuthorSocialId {
   __typename?: 'AuthorSocialID';
  id?: Maybe<Scalars['String']>;
}

export interface BillingAddressInput {
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  streetAddress?: Maybe<Scalars['String']>;
  suburb?: Maybe<Scalars['String']>;
  postcode: Scalars['String'];
  state?: Maybe<Scalars['String']>;
  country: Scalars['String'];
}

export enum Brand {
  Afr = 'AFR',
  BrisbaneTimes = 'BRISBANE_TIMES',
  CanberraTimes = 'CANBERRA_TIMES',
  Smh = 'SMH',
  TheAge = 'THE_AGE',
  WaToday = 'WA_TODAY',
  AfrLockup = 'AFR_LOCKUP',
  BrisbaneTimesLockup = 'BRISBANE_TIMES_LOCKUP',
  SmhLockup = 'SMH_LOCKUP',
  TheAgeLockup = 'THE_AGE_LOCKUP',
  WaTodayLockup = 'WA_TODAY_LOCKUP'
}

export interface BrandDetails {
   __typename?: 'BrandDetails';
  id: Scalars['String'];
  name: Brand;
  shortName: Scalars['String'];
}

export interface Canonical {
   __typename?: 'Canonical';
  brand?: Maybe<Scalars['String']>;
  path?: Maybe<Scalars['String']>;
}

export interface Category {
   __typename?: 'Category';
  id: Scalars['ID'];
  name: Scalars['String'];
  selectable: Scalars['Boolean'];
  shortID: Scalars['Int'];
  subCategories: Array<Category>;
}

export interface CategoryContentQuery  extends MobileConfigMenuItemContentQuery {
   __typename?: 'CategoryContentQuery';
  assetsCount: Scalars['Int'];
  name: Scalars['String'];
}

export interface ChannelSubscriptionPackage {
   __typename?: 'ChannelSubscriptionPackage';
  features: SubscriptionPackageFeatures;
  header: SubscriptionPackagePageHeader;
  headline: SubscriptionPackageHeadline;
  id: Scalars['ID'];
  name: SubscriptionPackageName;
  pricing: SubscriptionPackagePricing;
  termsAndConditions: SubscriptionPackageTermsAndConditions;
}

export interface Clipping {
   __typename?: 'Clipping';
  asset: Asset;
  assetVariations?: Maybe<Variations>;
  lockedBy: Scalars['String'];
}

export interface ClippingAddInput {
  assetVariations?: Maybe<Scalars['String']>;
  assetID: Scalars['String'];
}

export interface ClippingAddReply {
   __typename?: 'ClippingAddReply';
  assetID: Scalars['String'];
}

export interface ClippingLastUpdatedError  extends Error {
   __typename?: 'ClippingLastUpdatedError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface ClippingList {
   __typename?: 'ClippingList';
  assetVariations?: Maybe<Variations>;
  listing: Listing;
}

export interface ClippingListError  extends Error {
   __typename?: 'ClippingListError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface ClippingListResponse {
   __typename?: 'ClippingListResponse';
  clippings: Array<ClippingList>;
  error?: Maybe<ClippingListError>;
}

export interface ClippingsLastUpdatedResponse {
   __typename?: 'ClippingsLastUpdatedResponse';
  changedHash: Scalars['String'];
  error?: Maybe<ClippingLastUpdatedError>;
}

export interface CommentsConnection {
   __typename?: 'CommentsConnection';
  closed: Scalars['Boolean'];
  nodes?: Maybe<Array<AssetComment>>;
  totalCommentCount: Scalars['Int'];
}

export interface Commodity {
   __typename?: 'Commodity';
  code: Scalars['String'];
  price: CommodityPrice;
  lastUpdated: Scalars['Time'];
  name: Scalars['String'];
}

export interface CommodityPrice {
   __typename?: 'CommodityPrice';
  high: Scalars['Float'];
  latest: Scalars['Float'];
  low: Scalars['Float'];
  movement: Scalars['Float'];
  movementPercent: Scalars['Float'];
}

export interface CommodityPricesError {
   __typename?: 'CommodityPricesError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface CommodityPricesResponse {
   __typename?: 'CommodityPricesResponse';
  commodities: Array<Commodity>;
  error?: Maybe<CommodityPricesError>;
}

export enum CommodityType {
  Agriculture = 'AGRICULTURE',
  Energy = 'ENERGY',
  Livestock = 'LIVESTOCK',
  Metal = 'METAL'
}

export interface Company {
   __typename?: 'Company';
  asxCode: Scalars['String'];
  displayName: Scalars['String'];
  name: Scalars['String'];
}

export interface CompanySearchInput {
  query: Scalars['String'];
}

export interface CompanySearchReply {
   __typename?: 'CompanySearchReply';
  companies: Array<Maybe<Company>>;
}

export interface ContentUnit {
   __typename?: 'ContentUnit';
  assetList?: Maybe<AssetList>;
  assets: Array<Asset>;
  config?: Maybe<ContentUnitConfig>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['Int'];
  isDraft?: Maybe<Scalars['Boolean']>;
  maxCount: Scalars['Int'];
  name: Scalars['String'];
  typeName: Scalars['String'];
}

export interface ContentUnitConfig {
   __typename?: 'ContentUnitConfig';
  displayConfig?: Maybe<DisplayConfiguration>;
  heading?: Maybe<Scalars['String']>;
  headingLink?: Maybe<Scalars['String']>;
  links?: Maybe<Array<Maybe<ContentUnitConfigLink>>>;
  mode?: Maybe<Scalars['String']>;
  sponsor?: Maybe<Scalars['String']>;
  widget?: Maybe<WidgetConfiguration>;
}

export interface ContentUnitConfigLink {
   __typename?: 'ContentUnitConfigLink';
  link?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
}

export interface ContentUnitLayout {
   __typename?: 'ContentUnitLayout';
  maxCount: Scalars['Int'];
  name: Scalars['String'];
}

export interface ContentUnitSaveInput {
  assetList: Scalars['String'];
  contentUnitID: Scalars['Int'];
}

export interface ContentUnitSaveReply {
   __typename?: 'ContentUnitSaveReply';
  contentUnitID: Scalars['Int'];
}

export interface CorporateAccount {
   __typename?: 'CorporateAccount';
  createdTime: Scalars['Time'];
  domains: Array<Scalars['String']>;
  id: Scalars['ID'];
  name: Scalars['String'];
  updatedTime: Scalars['Time'];
}

export interface CreateEntitlementTokenInput {
  brand: Brand;
}

export interface CreateEntitlementTokenResponse {
   __typename?: 'CreateEntitlementTokenResponse';
  error?: Maybe<EntitlementTokenError>;
  token: Scalars['String'];
}

export interface CreateMemberFromSocialError  extends Error {
   __typename?: 'CreateMemberFromSocialError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface CreateMemberFromSocialInput {
  brand?: Maybe<Brand>;
  idToken: Scalars['String'];
  provider?: Maybe<SocialLoginProvider>;
  redirectURL: Scalars['String'];
  waitForEntitlements?: Maybe<Scalars['Boolean']>;
}

export interface CreateMemberFromSocialResponse {
   __typename?: 'CreateMemberFromSocialResponse';
  error?: Maybe<CreateMemberFromSocialError>;
  requestID: Scalars['String'];
}

export interface CreateMemberInput {
  email: Scalars['String'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  siteKey: Scalars['String'];
  password: Scalars['String'];
}

export interface CreateMemberResponse {
   __typename?: 'CreateMemberResponse';
  requestID: Scalars['String'];
}

export interface CreateRunsheetError  extends Error {
   __typename?: 'CreateRunsheetError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface CreateRunsheetInput {
  brands: Array<Brand>;
  category: Scalars['Int'];
  divisions: Array<RunsheetDivisionInput>;
  runsheetID?: Maybe<Scalars['ID']>;
  name: Scalars['String'];
  newsroom: NewsroomType;
  newsrooms?: Maybe<Array<Newsroom>>;
  productionDate?: Maybe<Scalars['Time']>;
  watchers?: Maybe<Array<Scalars['String']>>;
}

export interface CreateRunsheetLockReply {
   __typename?: 'CreateRunsheetLockReply';
  runsheetID: Scalars['ID'];
  success: Scalars['Boolean'];
  userID: Scalars['String'];
}

export interface CreateRunsheetResponse {
   __typename?: 'CreateRunsheetResponse';
  error?: Maybe<CreateRunsheetError>;
  id?: Maybe<Scalars['ID']>;
  validLock: Scalars['Boolean'];
}

export interface CreditCardInput {
  number: Scalars['String'];
  cardholderName: Scalars['String'];
  expiryMonth: Scalars['String'];
  expiryYear: Scalars['String'];
  refId: Scalars['String'];
  securityCode: Scalars['String'];
}

export interface CricketScorecardData {
   __typename?: 'CricketScorecardData';
  classNames?: Maybe<Scalars['String']>;
  lazyLoad?: Maybe<Scalars['String']>;
}

export interface CrossWord {
   __typename?: 'CrossWord';
  author: Scalars['String'];
  clues: CrossWordClues;
  date: Scalars['String'];
  grid: Array<Array<Scalars['String']>>;
  id: Scalars['ID'];
  name: Scalars['String'];
  specialInstructions?: Maybe<Scalars['String']>;
  type: CrosswordType;
}

export interface CrossWordClueDetails {
   __typename?: 'CrossWordClueDetails';
  position: Scalars['Int'];
  question: Scalars['String'];
}

export interface CrossWordClues {
   __typename?: 'CrossWordClues';
  across: Array<CrossWordClueDetails>;
  down: Array<CrossWordClueDetails>;
}

export interface CrossWordsRecentError  extends Error {
   __typename?: 'CrossWordsRecentError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface CrossWordsRecentResponse {
   __typename?: 'CrossWordsRecentResponse';
  crosswords: Array<CrossWord>;
  error?: Maybe<CrossWordsRecentError>;
}

export enum CrosswordType {
  Cryptic = 'CRYPTIC',
  Quick = 'QUICK'
}

export interface Cta {
   __typename?: 'CTA';
  active: Scalars['Boolean'];
  description?: Maybe<Scalars['String']>;
  name: Scalars['String'];
  id: Scalars['String'];
  slot: CtaSlot;
  type: Scalars['String'];
  config: CtaPodcastConfig;
}

export interface CtaBySlotResponse {
   __typename?: 'CTABySlotResponse';
  cta?: Maybe<Cta>;
  error?: Maybe<CtaError>;
}

export interface CtaConfigInput {
  podcastConfig: CtaPodcastConfigInput;
}

export interface CtaError  extends Error {
   __typename?: 'CTAError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface CtaPodcastConfig {
   __typename?: 'CTAPodcastConfig';
  episode: Episode;
  image: Scalars['String'];
  link: Scalars['String'];
  subscribeLinks: CtaPodcastSubscribeLinks;
  title: Scalars['String'];
}

export interface CtaPodcastConfigInput {
  episode: EpisodeInput;
  image: Scalars['String'];
  link: Scalars['String'];
  subscribeLinks: CtaPodcastSubscribeLinksInput;
  title: Scalars['String'];
}

export interface CtaPodcastSubscribeLinks {
   __typename?: 'CTAPodcastSubscribeLinks';
  google: Scalars['String'];
  itunes: Scalars['String'];
}

export interface CtaPodcastSubscribeLinksInput {
  google: Scalars['String'];
  itunes: Scalars['String'];
}

export interface CtasConnection {
   __typename?: 'CtasConnection';
  ctas: Array<Cta>;
  error?: Maybe<CtaError>;
  pageInfo: PageInfo;
}

export interface CtaSlot {
   __typename?: 'CTASlot';
  description?: Maybe<Scalars['String']>;
  name: Scalars['String'];
}

export interface CtaUpdateConfigReply {
   __typename?: 'CTAUpdateConfigReply';
  error?: Maybe<CtaError>;
  success: Scalars['Boolean'];
}

export interface CtaUpdatePodcastConfigInput {
  config: CtaConfigInput;
  id: Scalars['String'];
}

export enum CurrencyCode {
  Aud = 'AUD',
  Cad = 'CAD',
  Chf = 'CHF',
  Cny = 'CNY',
  Eur = 'EUR',
  Gbp = 'GBP',
  Hkd = 'HKD',
  Idr = 'IDR',
  Inr = 'INR',
  Jpy = 'JPY',
  Myr = 'MYR',
  Nzd = 'NZD',
  Sgd = 'SGD',
  Thb = 'THB',
  Usd = 'USD',
  Zar = 'ZAR'
}

export interface CurrencyCompare {
   __typename?: 'CurrencyCompare';
  compareName: Scalars['String'];
  from: CurrencyCode;
  fromDisplayName: Scalars['String'];
  to: CurrencyCode;
  toDisplayName: Scalars['String'];
  price: Scalars['Float'];
  lastUpdated: Scalars['String'];
  priceMvt: Scalars['Float'];
  priceMvtPct: Scalars['Float'];
  buyPrice: Scalars['Float'];
  sellPrice: Scalars['Float'];
}

export interface DeleteRunsheetError  extends Error {
   __typename?: 'DeleteRunsheetError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface DeleteRunsheetInput {
  runsheetID: Scalars['ID'];
}

export interface DeleteRunsheetResponse {
   __typename?: 'DeleteRunsheetResponse';
  error?: Maybe<DeleteRunsheetError>;
  success: Scalars['Boolean'];
}

export interface DeliveryAddressInput {
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  unitNumber: Scalars['String'];
  streetNumber: Scalars['String'];
  streetName: Scalars['String'];
  streetType: StreetType;
  suburb: Scalars['String'];
  postcode: Scalars['String'];
  state: Scalars['String'];
  country: Scalars['String'];
}

export interface DisplayConfiguration {
   __typename?: 'DisplayConfiguration';
  layout?: Maybe<ContentUnitLayout>;
  layouts?: Maybe<Array<ContentUnitLayout>>;
  showPrimaryTag?: Maybe<Scalars['Boolean']>;
  showRelated: Scalars['Boolean'];
  variation?: Maybe<Scalars['String']>;
  variations?: Maybe<Array<Scalars['String']>>;
}

export interface DividendHistory {
   __typename?: 'DividendHistory';
  amount: Scalars['Float'];
  booksCloseDate: Scalars['String'];
  exDate: Scalars['String'];
  frankedPercentage: Scalars['Float'];
  payableDate: Scalars['String'];
}

export interface DividendSummary {
   __typename?: 'DividendSummary';
  amount: Scalars['Float'];
  earningsPerShare: Scalars['Float'];
  exDate: Scalars['String'];
  frankedPercentage: Scalars['Float'];
  payableDate: Scalars['String'];
  ratePerShare: Scalars['Float'];
  type: DividendType;
  yield: Scalars['Float'];
}

export interface DividendType {
   __typename?: 'DividendType';
  code: Scalars['String'];
  description: Scalars['String'];
}

export interface EngagementSegment {
   __typename?: 'EngagementSegment';
  batchTime: Scalars['Time'];
  userSegment: Scalars['String'];
}

export interface EntitlementsByPlansError  extends Error {
   __typename?: 'EntitlementsByPlansError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface EntitlementsByPlansResponse {
   __typename?: 'EntitlementsByPlansResponse';
  error?: Maybe<EntitlementsByPlansError>;
  planEntitlements: Array<SubscriptionPlanEntitlement>;
}

export interface EntitlementTokenError  extends Error {
   __typename?: 'EntitlementTokenError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface Episode {
   __typename?: 'Episode';
  link: Scalars['String'];
  title: Scalars['String'];
}

export interface EpisodeInput {
  link: Scalars['String'];
  title: Scalars['String'];
}

export interface Error {
  message: Scalars['String'];
  type: ErrorType;
}

export enum ErrorClass {
  Forbidden = 'FORBIDDEN',
  InvalidAuth = 'INVALID_AUTH',
  InvalidRequest = 'INVALID_REQUEST',
  NotFound = 'NOT_FOUND',
  Unavailable = 'UNAVAILABLE'
}

export interface ErrorType {
  class: ErrorClass;
}

export interface ErrorTypeForbidden  extends ErrorType {
   __typename?: 'ErrorTypeForbidden';
  class: ErrorClass;
}

export interface ErrorTypeInvalidAuth  extends ErrorType {
   __typename?: 'ErrorTypeInvalidAuth';
  class: ErrorClass;
  code: InvalidAuthErrorCode;
}

export interface ErrorTypeInvalidRequest  extends ErrorType {
   __typename?: 'ErrorTypeInvalidRequest';
  class: ErrorClass;
  fields: Array<RequestFieldError>;
}

export interface ErrorTypeNotFound  extends ErrorType {
   __typename?: 'ErrorTypeNotFound';
  class: ErrorClass;
}

export interface ErrorTypeUnavailable  extends ErrorType {
   __typename?: 'ErrorTypeUnavailable';
  class: ErrorClass;
  retryable: Scalars['Boolean'];
}

export interface ExternalContentQuery  extends MobileConfigMenuItemContentQuery {
   __typename?: 'ExternalContentQuery';
  link: Scalars['String'];
  name: Scalars['String'];
}

export interface FileUploadUrlError  extends Error {
   __typename?: 'FileUploadURLError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface FileUploadUrlResponse {
   __typename?: 'FileUploadURLResponse';
  error?: Maybe<FileUploadUrlError>;
  preSignedURL?: Maybe<Scalars['String']>;
}

export interface FinancialChange {
   __typename?: 'FinancialChange';
  delta: Scalars['String'];
  percent: Scalars['Float'];
}

export interface FinancialCommoditiesBySymbolsError {
   __typename?: 'FinancialCommoditiesBySymbolsError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface FinancialCommoditiesBySymbolsResponse {
   __typename?: 'FinancialCommoditiesBySymbolsResponse';
  error?: Maybe<FinancialCommoditiesBySymbolsError>;
  instruments: Array<FinancialInstrument>;
}

export interface FinancialCommodity {
  commodityType: CommodityType;
}

export interface FinancialCommodityFuture  extends FinancialInstrument, FinancialCommodity, FinancialFuture {
   __typename?: 'FinancialCommodityFuture';
  commodityType: CommodityType;
  currencySymbol: Scalars['String'];
  expiry: Scalars['Time'];
  id: Scalars['ID'];
  name: Scalars['String'];
  quoteSummary: FinancialQuoteSummary;
  symbol: Scalars['String'];
  unit: Scalars['String'];
}

export interface FinancialCommoditySpot  extends FinancialInstrument, FinancialCommodity {
   __typename?: 'FinancialCommoditySpot';
  commodityType: CommodityType;
  currencySymbol: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
  quoteSummary: FinancialQuoteSummary;
  symbol: Scalars['String'];
  unit: Scalars['String'];
}

export interface FinancialForexRate {
   __typename?: 'FinancialForexRate';
  providerUpdateTime: Scalars['Time'];
  rate: Scalars['String'];
}

export interface FinancialForexRateError {
   __typename?: 'FinancialForexRateError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface FinancialForexRateResponse {
   __typename?: 'FinancialForexRateResponse';
  error?: Maybe<FinancialForexRateError>;
  rate?: Maybe<FinancialForexRate>;
}

export interface FinancialFuture {
  expiry: Scalars['Time'];
}

export interface FinancialInstrument {
  currencySymbol: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
  quoteSummary?: Maybe<FinancialQuoteSummary>;
  symbol: Scalars['String'];
  unit: Scalars['String'];
}

export interface FinancialQuote {
   __typename?: 'FinancialQuote';
  change: FinancialChange;
  close: FinancialQuoteValue;
  range?: Maybe<FinancialQuoteRange>;
}

export interface FinancialQuoteRange {
   __typename?: 'FinancialQuoteRange';
  high: Scalars['String'];
  low: Scalars['String'];
}

export interface FinancialQuoteSummary {
   __typename?: 'FinancialQuoteSummary';
  fiftyTwoWeeks?: Maybe<FinancialQuoteRange>;
  quote?: Maybe<FinancialQuote>;
}

export interface FinancialQuoteValue {
   __typename?: 'FinancialQuoteValue';
  time: Scalars['Time'];
  value: Scalars['String'];
}

export interface FindTagsExcludeInput {
  archiveStatus?: Maybe<ArchiveStatusExcludeType>;
  contextIDs?: Maybe<Array<Scalars['ID']>>;
  tagIDs?: Maybe<Array<Scalars['String']>>;
  visibility?: Maybe<VisibilityExcludeType>;
}

export enum FindTagsField {
  DisplayName = 'DISPLAY_NAME',
  Name = 'NAME',
  PartialText = 'PARTIAL_TEXT',
  StartDisplayName = 'START_DISPLAY_NAME',
  PartialTextDisplayName = 'PARTIAL_TEXT_DISPLAY_NAME'
}

export enum FindTagsFilterType {
  Context = 'CONTEXT',
  None = 'NONE'
}

export interface FindTagsReply {
   __typename?: 'FindTagsReply';
  tagDetails: Array<Maybe<TagDetail>>;
  totalCount: Scalars['Int'];
}

export interface FootballScoreboard {
   __typename?: 'FootballScoreboard';
  id: Scalars['Int'];
  matches: Array<Maybe<Match>>;
}

export interface Forecast {
   __typename?: 'Forecast';
  current?: Maybe<Scalars['Float']>;
  maximum?: Maybe<Scalars['Float']>;
  minimum?: Maybe<Scalars['Float']>;
  weatherSummary: Scalars['String'];
}

export interface GooglePlayLinkingError  extends Error {
   __typename?: 'GooglePlayLinkingError';
  message: Scalars['String'];
  type: ErrorType;
}

export enum GooglePlayLinkingStatus {
  Linked = 'LINKED',
  Pending = 'PENDING'
}

export interface GooglePlayOrder {
   __typename?: 'GooglePlayOrder';
  id: Scalars['ID'];
}

export interface GooglePlayReceipt {
   __typename?: 'GooglePlayReceipt';
  subscription: GooglePlaySubscription;
}

export interface GooglePlaySubscription {
   __typename?: 'GooglePlaySubscription';
  discount: GooglePlaySubscriptionDiscounts;
  expiry: GooglePlaySubscriptionExpiry;
  purchase: GooglePlaySubscriptionPurchase;
  renewal: GooglePlaySubscriptionRenewal;
  status: GooglePlaySubscriptionStatus;
}

export interface GooglePlaySubscriptionDiscounts {
   __typename?: 'GooglePlaySubscriptionDiscounts';
  trial: GooglePlaySubscriptionTrial;
}

export interface GooglePlaySubscriptionExpiry {
   __typename?: 'GooglePlaySubscriptionExpiry';
  date: Scalars['Time'];
}

export interface GooglePlaySubscriptionPurchase {
   __typename?: 'GooglePlaySubscriptionPurchase';
  date: Scalars['Time'];
  order: GooglePlayOrder;
}

export interface GooglePlaySubscriptionRenewal {
   __typename?: 'GooglePlaySubscriptionRenewal';
  auto: Scalars['Boolean'];
}

export enum GooglePlaySubscriptionStatus {
  Active = 'ACTIVE',
  Grace = 'GRACE',
  Expired = 'EXPIRED'
}

export interface GooglePlaySubscriptionTrial {
   __typename?: 'GooglePlaySubscriptionTrial';
  status: GooglePlaySubscriptionTrialStatus;
}

export enum GooglePlaySubscriptionTrialStatus {
  Active = 'ACTIVE',
  Inactive = 'INACTIVE'
}

export interface Image {
   __typename?: 'Image';
  data: ImageData;
  url?: Maybe<Scalars['String']>;
}

export interface ImageData {
   __typename?: 'ImageData';
  altText?: Maybe<Scalars['String']>;
  animated?: Maybe<Scalars['Boolean']>;
  aspect?: Maybe<Scalars['Float']>;
  autoCrop?: Maybe<Scalars['Boolean']>;
  /** @deprecated Please use the *ImageData.autoCrop* field. */
  autocrop?: Maybe<Scalars['Boolean']>;
  caption?: Maybe<Scalars['String']>;
  credit?: Maybe<Scalars['String']>;
  cropWidth?: Maybe<Scalars['Int']>;
  id: Scalars['ID'];
  mimeType?: Maybe<Scalars['String']>;
  offsetX?: Maybe<Scalars['Int']>;
  offsetY?: Maybe<Scalars['Int']>;
  source?: Maybe<Scalars['String']>;
  zoom?: Maybe<Scalars['Float']>;
}

export interface ImportStatusValidationError {
   __typename?: 'ImportStatusValidationError';
  email: Scalars['String'];
  errors: Scalars['String'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  rowId: Scalars['Int'];
}

export interface IndexPrice {
   __typename?: 'IndexPrice';
  code: Scalars['String'];
  name: Scalars['String'];
  price: Scalars['Float'];
  priceClose: Scalars['Float'];
  priceMovement: Scalars['Float'];
  priceMovementPercent: Scalars['Float'];
  series: Array<IndexPriceItem>;
  lastUpdated: Scalars['String'];
}

export interface IndexPriceItem {
   __typename?: 'IndexPriceItem';
  price: Scalars['Float'];
  time: Scalars['String'];
}

export interface IngestVideoError  extends Error {
   __typename?: 'IngestVideoError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface IngestVideoInput {
  name: Scalars['String'];
  source: Scalars['String'];
}

export interface IngestVideoResponse {
   __typename?: 'IngestVideoResponse';
  error?: Maybe<IngestVideoError>;
  id?: Maybe<Scalars['ID']>;
}

export enum InvalidAuthErrorCode {
  Ia100 = 'IA100',
  Ia101 = 'IA101',
  Ia102 = 'IA102',
  Ia103 = 'IA103',
  Ia104 = 'IA104'
}

export interface IsAssetInSavedListError {
   __typename?: 'IsAssetInSavedListError';
  message: Scalars['String'];
  type: IsAssetInSavedListErrorType;
}

export enum IsAssetInSavedListErrorType {
  AuthenticationError = 'AUTHENTICATION_ERROR',
  GenericError = 'GENERIC_ERROR',
  InvalidRequestError = 'INVALID_REQUEST_ERROR'
}

export interface IsAssetInSavedListInput {
  assetID: Scalars['String'];
  brand: Brand;
}

export interface IsAssetInSavedListResponse {
   __typename?: 'IsAssetInSavedListResponse';
  isSaved?: Maybe<Scalars['Boolean']>;
  error?: Maybe<IsAssetInSavedListError>;
}

export interface IssuerName {
   __typename?: 'IssuerName';
  fullName: Scalars['String'];
  abbrevName: Scalars['String'];
  shortName: Scalars['String'];
}

export enum LegalFlag {
  Approved = 'APPROVED',
  Pending = 'PENDING'
}

export interface LimitVariants {
   __typename?: 'LimitVariants';
  newSession?: Maybe<PaywallExceptionsNewSession>;
  referrers?: Maybe<PaywallExceptionsReferrers>;
}

export interface LinkAppleStorePurchaseInput {
  environment: AppleStoreEnvironment;
  originalTransactionIDs: Array<Scalars['String']>;
  receipt: Scalars['String'];
}

export interface LinkAppleStorePurchaseResponse {
   __typename?: 'LinkAppleStorePurchaseResponse';
  error?: Maybe<AppleStoreLinkingError>;
  status?: Maybe<AppleStoreLinkingStatus>;
}

export interface LinkGooglePlayPurchaseInput {
  packageName: Scalars['String'];
  productID: Scalars['String'];
  token: Scalars['String'];
}

export interface LinkGooglePlayPurchaseResponse {
   __typename?: 'LinkGooglePlayPurchaseResponse';
  error?: Maybe<GooglePlayLinkingError>;
  status?: Maybe<GooglePlayLinkingStatus>;
}

export interface Listing {
   __typename?: 'Listing';
  asset?: Maybe<Asset>;
  assignee?: Maybe<Assignee>;
  brief?: Maybe<Scalars['String']>;
  collaborators: Array<Author>;
  deadline: Scalars['String'];
  id: Scalars['ID'];
  identifier?: Maybe<Scalars['String']>;
  shortID: Scalars['String'];
  lockedBy: Scalars['String'];
  printReady: Scalars['Boolean'];
}

export interface ListingConnection {
   __typename?: 'ListingConnection';
  edges: Array<AssetEdge>;
  listing: Array<Listing>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int'];
}

export interface ListingSearchEditorialError  extends Error {
   __typename?: 'ListingSearchEditorialError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface ListingSearchEditorialResponse {
   __typename?: 'ListingSearchEditorialResponse';
  connection?: Maybe<ListingConnection>;
  error?: Maybe<ListingSearchEditorialError>;
}

export interface ListingSearchResponse {
   __typename?: 'ListingSearchResponse';
  connection?: Maybe<ListingConnection>;
}

export enum ListingsFilterType {
  None = 'NONE',
  Draft = 'DRAFT',
  Ready = 'READY',
  Review = 'REVIEW'
}

export enum ListingsOrderType {
  Deadline = 'DEADLINE',
  LastModified = 'LAST_MODIFIED',
  Published = 'PUBLISHED',
  Publication = 'PUBLICATION'
}

export interface Match {
   __typename?: 'Match';
  id: Scalars['ID'];
  homeTeam: Team;
  awayTeam: Team;
  date: Scalars['String'];
  location: Scalars['String'];
  displayLocation: Scalars['String'];
  status: Scalars['String'];
  number: Scalars['Int'];
  roundID: Scalars['Int'];
  roundName: Scalars['String'];
}

export interface MemberCheckExistsResponse {
   __typename?: 'MemberCheckExistsResponse';
  memberExists: Scalars['Boolean'];
}

export interface MemberCorporateBulkInviteError  extends Error {
   __typename?: 'MemberCorporateBulkInviteError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberCorporateBulkInviteInput {
  accountId: Scalars['ID'];
  fileKey: Scalars['String'];
}

export interface MemberCorporateBulkInviteResponse {
   __typename?: 'MemberCorporateBulkInviteResponse';
  error?: Maybe<MemberCorporateBulkInviteError>;
  headerId: Scalars['ID'];
}

export interface MemberCorporateCreateError  extends Error {
   __typename?: 'MemberCorporateCreateError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberCorporateCreateInput {
  brand: Brand;
  existsInProvider?: Maybe<Scalars['Boolean']>;
  password: Scalars['String'];
  token: Scalars['String'];
}

export interface MemberCorporateCreateResponse {
   __typename?: 'MemberCorporateCreateResponse';
  error?: Maybe<MemberCorporateCreateError>;
  id: Scalars['ID'];
}

export interface MemberCorporateDetailError  extends Error {
   __typename?: 'MemberCorporateDetailError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberCorporateDetailResponse {
   __typename?: 'MemberCorporateDetailResponse';
  detail?: Maybe<MemberDetail>;
  error?: Maybe<MemberCorporateDetailError>;
}

export enum MemberCorporateImportStatus {
  Failed = 'FAILED',
  Imported = 'IMPORTED',
  Importing = 'IMPORTING',
  Invalid = 'INVALID',
  NotValid = 'NOT_VALID',
  Pending = 'PENDING',
  Valid = 'VALID'
}

export interface MemberCorporateImportStatusError  extends Error {
   __typename?: 'MemberCorporateImportStatusError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberCorporateImportStatusResponse {
   __typename?: 'MemberCorporateImportStatusResponse';
  error?: Maybe<MemberCorporateImportStatusError>;
  status: MemberCorporateImportStatus;
  validationErrors: Array<ImportStatusValidationError>;
}

export interface MemberCorporateInviteError  extends Error {
   __typename?: 'MemberCorporateInviteError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberCorporateInviteInput {
  accountID: Scalars['ID'];
  costCentre: Scalars['String'];
  email: Scalars['String'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  packageID: Scalars['String'];
  role: AccountCorporateRoleType;
}

export interface MemberCorporateInviteResponse {
   __typename?: 'MemberCorporateInviteResponse';
  error?: Maybe<MemberCorporateInviteError>;
  id: Scalars['ID'];
}

export interface MemberCorporateMember {
   __typename?: 'MemberCorporateMember';
  email: Scalars['String'];
  firstName: Scalars['String'];
  id: Scalars['ID'];
  lastName: Scalars['String'];
  membershipId?: Maybe<Scalars['String']>;
  packageId?: Maybe<Scalars['String']>;
  role: AccountCorporateRoleType;
  status: MemberCorporateStatus;
  updatedTime: Scalars['Time'];
}

export interface MemberCorporateRevokeInput {
  accountId: Scalars['ID'];
  id: Scalars['ID'];
}

export interface MemberCorporateRole {
   __typename?: 'MemberCorporateRole';
  accountId: Scalars['ID'];
  role: AccountCorporateRoleType;
}

export interface MemberCorporateSearchError  extends Error {
   __typename?: 'MemberCorporateSearchError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberCorporateSearchFilterInput {
  role?: Maybe<Array<AccountCorporateRoleType>>;
}

export enum MemberCorporateSearchOrder {
  NameAsc = 'NAME_ASC',
  NameDesc = 'NAME_DESC',
  StatusAsc = 'STATUS_ASC',
  StatusDesc = 'STATUS_DESC',
  UpdatedAsc = 'UPDATED_ASC',
  UpdatedDesc = 'UPDATED_DESC'
}

export interface MemberCorporateSearchResponse {
   __typename?: 'MemberCorporateSearchResponse';
  error?: Maybe<MemberCorporateSearchError>;
  members: Array<MemberCorporateMember>;
  offset: Scalars['Int'];
  totalCount: Scalars['Int'];
}

export interface MemberCorporateSearchTermInput {
  name?: Maybe<Scalars['String']>;
}

export enum MemberCorporateStatus {
  Created = 'CREATED',
  Deleted = 'DELETED',
  Error = 'ERROR',
  Invalid = 'INVALID',
  Invited = 'INVITED',
  NotExist = 'NOT_EXIST',
  Pending = 'PENDING',
  Registered = 'REGISTERED',
  Revoked = 'REVOKED',
  Verified = 'VERIFIED'
}

export interface MemberCorporateStatusError  extends Error {
   __typename?: 'MemberCorporateStatusError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberCorporateStatusResponse {
   __typename?: 'MemberCorporateStatusResponse';
  accountName: Scalars['String'];
  error?: Maybe<MemberCorporateStatusError>;
  existsInProvider?: Maybe<Scalars['Boolean']>;
  role?: Maybe<AccountCorporateRoleType>;
  status?: Maybe<MemberCorporateStatus>;
}

export interface MemberCorporateUpdateError  extends Error {
   __typename?: 'MemberCorporateUpdateError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberCorporateUpdateInput {
  accountId: Scalars['ID'];
  costCentre: Scalars['String'];
  firstName: Scalars['String'];
  id: Scalars['ID'];
  lastName: Scalars['String'];
  packageIds: Array<Scalars['String']>;
  revokeAccess: Scalars['Boolean'];
  role: AccountCorporateRoleType;
}

export interface MemberCorporateUpdateResponse {
   __typename?: 'MemberCorporateUpdateResponse';
  error?: Maybe<MemberCorporateUpdateError>;
  id: Scalars['ID'];
}

export interface MemberDetail {
   __typename?: 'MemberDetail';
  accountMembers: Array<AccountMember>;
  createdTime: Scalars['Time'];
  email: Scalars['String'];
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  status: MemberCorporateStatus;
  updatedTime: Scalars['Time'];
}

export interface MemberDetails {
   __typename?: 'MemberDetails';
  profile: MemberProfile;
}

export interface MemberDetailsError  extends Error {
   __typename?: 'MemberDetailsError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberDetailsResponse {
   __typename?: 'MemberDetailsResponse';
  error?: Maybe<MemberDetailsError>;
  member?: Maybe<MemberDetails>;
}

export interface MemberExternalSubscriptionProvider {
   __typename?: 'MemberExternalSubscriptionProvider';
  plan: Scalars['String'];
  provider: SubscriptionProvider;
  purchaseSource: SubscriptionPurchaseSource;
}

export interface MemberExternalSubscriptionProvidersError  extends Error {
   __typename?: 'MemberExternalSubscriptionProvidersError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberExternalSubscriptionProvidersResponse {
   __typename?: 'MemberExternalSubscriptionProvidersResponse';
  error?: Maybe<MemberExternalSubscriptionProvidersError>;
  providers: Array<MemberExternalSubscriptionProvider>;
}

export interface MemberProfile {
   __typename?: 'MemberProfile';
  email: Scalars['String'];
  id: Scalars['ID'];
  membershipId: Scalars['String'];
  displayName?: Maybe<Scalars['String']>;
  shortID: Scalars['String'];
  type: MemberType;
  roles: Array<MemberCorporateRole>;
}

export enum MemberPurchaseLinkageStatus {
  EmailConflict = 'EMAIL_CONFLICT',
  Linked = 'LINKED',
  NotLinked = 'NOT_LINKED'
}

export interface MemberRecommendedTagsError  extends Error {
   __typename?: 'MemberRecommendedTagsError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberRecommendedTagsResponse {
   __typename?: 'MemberRecommendedTagsResponse';
  error?: Maybe<MemberRecommendedTagsError>;
  tags?: Maybe<Array<RecommendedTag>>;
}

export interface MemberRequestProgressResponse {
   __typename?: 'MemberRequestProgressResponse';
  error: Scalars['String'];
  memberRequestProgressID: Scalars['String'];
  status: Scalars['String'];
  tokenURL?: Maybe<Scalars['String']>;
}

export interface MemberSubscriptionDetails {
   __typename?: 'MemberSubscriptionDetails';
  entitlements: Array<Scalars['String']>;
  plans: Array<Scalars['String']>;
}

export interface MemberSubscriptionDetailsError  extends Error {
   __typename?: 'MemberSubscriptionDetailsError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberSubscriptionDetailsResponse {
   __typename?: 'MemberSubscriptionDetailsResponse';
  error?: Maybe<MemberSubscriptionDetailsError>;
  subscription?: Maybe<MemberSubscriptionDetails>;
}

export interface MemberTagRecommendationAddError  extends Error {
   __typename?: 'MemberTagRecommendationAddError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberTagRecommendationAddInput {
  brand: Brand;
}

export interface MemberTagRecommendationAddResponse {
   __typename?: 'MemberTagRecommendationAddResponse';
  error?: Maybe<MemberTagRecommendationAddError>;
  success: Scalars['Boolean'];
}

export interface MemberTagRecommendationNewsfeedInput {
  mode: MemberTagRecommendationNewsfeedMode;
}

export enum MemberTagRecommendationNewsfeedMode {
  Automated = 'AUTOMATED',
  Manual = 'MANUAL',
  Suggested = 'SUGGESTED'
}

export interface MemberTagRecommendationPreferences {
   __typename?: 'MemberTagRecommendationPreferences';
  newsfeed: MemberTagRecommendationPreferencesNewsfeed;
}

export interface MemberTagRecommendationPreferencesError  extends Error {
   __typename?: 'MemberTagRecommendationPreferencesError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberTagRecommendationPreferencesNewsfeed {
   __typename?: 'MemberTagRecommendationPreferencesNewsfeed';
  mode: MemberTagRecommendationNewsfeedMode;
}

export interface MemberTagRecommendationPreferencesResponse {
   __typename?: 'MemberTagRecommendationPreferencesResponse';
  error?: Maybe<MemberTagRecommendationPreferencesError>;
  preferences?: Maybe<MemberTagRecommendationPreferences>;
}

export interface MemberTagRecommendationPreferencesUpdateError  extends Error {
   __typename?: 'MemberTagRecommendationPreferencesUpdateError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MemberTagRecommendationPreferencesUpdateInput {
  newsfeed?: Maybe<MemberTagRecommendationNewsfeedInput>;
}

export interface MemberTagRecommendationPreferencesUpdateResponse {
   __typename?: 'MemberTagRecommendationPreferencesUpdateResponse';
  error?: Maybe<MemberTagRecommendationPreferencesUpdateError>;
  success: Scalars['Boolean'];
}

export enum MemberType {
  Member = 'MEMBER'
}

export interface MeteringError  extends Error {
   __typename?: 'MeteringError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MeterRule {
   __typename?: 'MeterRule';
  count?: Maybe<Scalars['Int']>;
  prompt?: Maybe<Prompt>;
  promptVariants?: Maybe<Array<Maybe<PromptVariants>>>;
  swgPrompt?: Maybe<SwgPrompt>;
}

export interface MetersVariants {
   __typename?: 'MetersVariants';
  referrers?: Maybe<PaywallExceptionsReferrers>;
  meters?: Maybe<Array<Maybe<MeterRule>>>;
}

export interface MobileAppClientPaywallCallToAction {
   __typename?: 'MobileAppClientPaywallCallToAction';
  text: Scalars['String'];
  url: Scalars['String'];
}

export interface MobileAppClientPaywallCallToActions {
   __typename?: 'MobileAppClientPaywallCallToActions';
  subscribe?: Maybe<MobileAppClientPaywallCallToAction>;
}

export interface MobileAppClientPaywallError  extends Error {
   __typename?: 'MobileAppClientPaywallError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MobileAppClientPaywallLimit {
   __typename?: 'MobileAppClientPaywallLimit';
  count: Scalars['Int'];
  prompt: MobileAppClientPaywallPrompt;
}

export interface MobileAppClientPaywallLimiting {
   __typename?: 'MobileAppClientPaywallLimiting';
  limits: Array<MobileAppClientPaywallLimit>;
}

export interface MobileAppClientPaywallMeter {
   __typename?: 'MobileAppClientPaywallMeter';
  count: Scalars['Int'];
  prompt: MobileAppClientPaywallPrompt;
}

export interface MobileAppClientPaywallMetering {
   __typename?: 'MobileAppClientPaywallMetering';
  meters: Array<MobileAppClientPaywallMeter>;
  periodDays: Scalars['Int'];
}

export interface MobileAppClientPaywallPrompt {
   __typename?: 'MobileAppClientPaywallPrompt';
  callToActions: MobileAppClientPaywallCallToActions;
  messages: MobileAppClientPaywallPromptMessages;
  title: Scalars['String'];
}

export interface MobileAppClientPaywallPromptMessages {
   __typename?: 'MobileAppClientPaywallPromptMessages';
  message: Scalars['String'];
  remaining: Scalars['String'];
}

export interface MobileAppClientPaywallRequestContext {
  platform: MobileAppPlatform;
  product: MobileAppProduct;
}

export interface MobileAppClientPaywallResponse {
   __typename?: 'MobileAppClientPaywallResponse';
  error?: Maybe<MobileAppClientPaywallError>;
  rules?: Maybe<MobileAppClientPaywallRules>;
}

export interface MobileAppClientPaywallRules {
   __typename?: 'MobileAppClientPaywallRules';
  limiting: MobileAppClientPaywallLimiting;
  metering: MobileAppClientPaywallMetering;
}

export interface MobileAppConfig {
   __typename?: 'MobileAppConfig';
  analytics: MobileConfigAnalytics;
  article: MobileConfigArticle;
  frontPage: MobileConfigFrontPage;
  help: MobileConfigHelp;
  legal: MobileConfigLegal;
  menu: MobileConfigMenu;
  survey: MobileConfigSurvey;
  versionUpgrade: MobileConfigVersionUpgrade;
}

export interface MobileAppConfigError  extends Error {
   __typename?: 'MobileAppConfigError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MobileAppConfigResponse {
   __typename?: 'MobileAppConfigResponse';
  config?: Maybe<MobileAppConfig>;
  error?: Maybe<MobileAppConfigError>;
}

export enum MobileAppPlatform {
  Android = 'ANDROID',
  Ios = 'IOS'
}

export enum MobileAppProduct {
  Smh = 'SMH',
  Theage = 'THEAGE'
}

export interface MobileConfigAdc {
   __typename?: 'MobileConfigAdc';
  url: Scalars['String'];
}

export interface MobileConfigAdUnitPositional {
   __typename?: 'MobileConfigAdUnitPositional';
  isDefault: Scalars['Boolean'];
  offset: Scalars['Int'];
  sizes: MobileConfigAdUnitSizes;
}

export interface MobileConfigAdUnits {
   __typename?: 'MobileConfigAdUnits';
  limit: Scalars['Int'];
  positional: Array<MobileConfigAdUnitPositional>;
}

export interface MobileConfigAdUnitSize {
   __typename?: 'MobileConfigAdUnitSize';
  height: Scalars['Int'];
  width: Scalars['Int'];
}

export interface MobileConfigAdUnitSizes {
   __typename?: 'MobileConfigAdUnitSizes';
  phone: Array<MobileConfigAdUnitSize>;
  tablet: Array<MobileConfigAdUnitSize>;
}

export interface MobileConfigAnalytics {
   __typename?: 'MobileConfigAnalytics';
  adc: MobileConfigAdc;
}

export interface MobileConfigApplication {
   __typename?: 'MobileConfigApplication';
  latestVersion: Scalars['String'];
  upgradeAction?: Maybe<MobileConfigUpgradeAction>;
}

export interface MobileConfigArticle {
   __typename?: 'MobileConfigArticle';
  adUnits: MobileConfigAdUnits;
  linkArticle: MobileConfigLinkArticle;
}

export interface MobileConfigChapter {
   __typename?: 'MobileConfigChapter';
  adUnits: MobileConfigAdUnits;
  sections: Array<MobileConfigChapterSection>;
}

export interface MobileConfigChapters {
   __typename?: 'MobileConfigChapters';
  myNews: MobileConfigChapter;
  topStories: MobileConfigChapter;
}

export interface MobileConfigChapterSection {
   __typename?: 'MobileConfigChapterSection';
  name: Scalars['String'];
  /** @deprecated Replaced by subSections */
  sections: Array<MobileConfigChapterSubSection>;
  subSections: Array<MobileConfigChapterSubSection>;
}

export interface MobileConfigChapterSubSection {
   __typename?: 'MobileConfigChapterSubSection';
  key: Scalars['String'];
}

export interface MobileConfigConditions {
   __typename?: 'MobileConfigConditions';
  url: Scalars['String'];
}

export interface MobileConfigDatabase {
   __typename?: 'MobileConfigDatabase';
  minimumVersion: Scalars['String'];
}

export enum MobileConfigEnv {
  Dev = 'DEV',
  Prod = 'PROD',
  Stage = 'STAGE',
  Test = 'TEST'
}

export interface MobileConfigFeedback {
   __typename?: 'MobileConfigFeedback';
  url: Scalars['String'];
}

export interface MobileConfigFeedInfo {
   __typename?: 'MobileConfigFeedInfo';
  base: Scalars['String'];
  path: Scalars['String'];
}

export interface MobileConfigFrontPage {
   __typename?: 'MobileConfigFrontPage';
  chapters: MobileConfigChapters;
  feed: Scalars['String'];
  /** @deprecated Replaced by the myNews in chapters */
  myNews: Array<MobileConfigChapterSection>;
  /** @deprecated Replaced by the topStories in chapters */
  topStories: Array<MobileConfigChapterSection>;
}

export interface MobileConfigHelp {
   __typename?: 'MobileConfigHelp';
  feedback: MobileConfigFeedback;
  support: MobileConfigSupport;
}

export interface MobileConfigLegal {
   __typename?: 'MobileConfigLegal';
  conditions: MobileConfigConditions;
  privacy: MobileConfigPrivacy;
}

export interface MobileConfigLinkArticle {
   __typename?: 'MobileConfigLinkArticle';
  regex: Scalars['String'];
  url: Scalars['String'];
}

export interface MobileConfigMenu {
   __typename?: 'MobileConfigMenu';
  feedInfo: MobileConfigFeedInfo;
  sections: Array<MobileConfigMenuItem>;
}

export interface MobileConfigMenuItem {
   __typename?: 'MobileConfigMenuItem';
  name: Scalars['String'];
  path: Scalars['String'];
  query: MobileConfigMenuItemContentQuery;
  subSections: Array<MobileConfigMenuItem>;
}

export interface MobileConfigMenuItemContentQuery {
  name: Scalars['String'];
}

export enum MobileConfigPlatform {
  Android = 'ANDROID',
  Ios = 'IOS'
}

export interface MobileConfigPrivacy {
   __typename?: 'MobileConfigPrivacy';
  url: Scalars['String'];
}

export enum MobileConfigProduct {
  Smh = 'SMH',
  Theage = 'THEAGE'
}

export interface MobileConfigSupport {
   __typename?: 'MobileConfigSupport';
  email: Scalars['String'];
}

export interface MobileConfigSurvey {
   __typename?: 'MobileConfigSurvey';
  enabled: Scalars['Boolean'];
  frequencyInDays: Scalars['Int'];
  key: Scalars['String'];
}

export interface MobileConfigUpgradeAction {
   __typename?: 'MobileConfigUpgradeAction';
  callToActions: UpgradeActionCallToActions;
  message: UpgradeActionMessage;
  title: Scalars['String'];
}

export interface MobileConfigVersionUpgrade {
   __typename?: 'MobileConfigVersionUpgrade';
  application: MobileConfigApplication;
  database: MobileConfigDatabase;
}

export interface MostUsedTagsError {
   __typename?: 'MostUsedTagsError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface MostUsedTagsResponse {
   __typename?: 'MostUsedTagsResponse';
  error?: Maybe<MostUsedTagsError>;
  tagUsages?: Maybe<Array<TagUsage>>;
}

export interface Mutation {
   __typename?: 'Mutation';
  accountCorporateCreate: AccountCorporateCreateResponse;
  accountCorporateUpdate: AccountCorporateUpdateResponse;
  ampLinkMember: AmpLinkMemberReply;
  assetLock: AssetLockReply;
  assetRead: AssetReadReply;
  assetUnlock: AssetUnlockReply;
  clippingAdd: ClippingAddReply;
  contentUnitSave: ContentUnitSaveReply;
  createEntitlementToken: CreateEntitlementTokenResponse;
  createMember: CreateMemberResponse;
  createMemberFromSocial: CreateMemberFromSocialResponse;
  createRunsheet: CreateRunsheetResponse;
  createRunsheetLock: CreateRunsheetLockReply;
  createVideoUploadURL: VideoUploadUrlResponse;
  ctaUpdateConfig: CtaUpdateConfigReply;
  deleteRunsheet: DeleteRunsheetResponse;
  ingestVideo: IngestVideoResponse;
  linkAppleStorePurchase: LinkAppleStorePurchaseResponse;
  memberCorporateBulkInvite: MemberCorporateBulkInviteResponse;
  memberCorporateCreate: MemberCorporateCreateResponse;
  memberCorporateUpdate: MemberCorporateUpdateResponse;
  memberCorporateInvite: MemberCorporateInviteResponse;
  memberCorporateRevoke: MemberCorporateUpdateResponse;
  memberTagRecommendationAdd: MemberTagRecommendationAddResponse;
  memberTagRecommendationPreferencesUpdate: MemberTagRecommendationPreferencesUpdateResponse;
  newsletterSubscribe: SubscribeReply;
  linkGooglePlayPurchase: LinkGooglePlayPurchaseResponse;
  publishEntitlement: PublishEntitlementResponse;
  puzzlesSaveProgress: PuzzleProgressResponse;
  savedListAddAsset: SavedListAddAssetResponse;
  savedListRemoveAsset: SavedListRemoveAssetResponse;
  sendMemberPasswordResetEmail: SendMemberPasswordResetEmailResponse;
  shortlistAddItem: ShortlistAddItemReply;
  shortlistMerge: ShortlistMergeReply;
  shortlistRemoveItem: ShortlistRemoveItemReply;
  subscriptionCreate: SubscriptionCreateReply;
  tagCreate: TagCreateReply;
  tagUpdate: TagUpdateReply;
  updateAccount?: Maybe<UpdateAccountResult>;
  updateMemberName: UpdateMemberNameResponse;
  updatePage?: Maybe<PageUpdate>;
  updateRunsheetLock: UpdateRunsheetLockReply;
  userTagsStatusUpdate: UserTagsStatusUpdateResponse;
  verifyAppleStoreReceipt: VerifyAppleStoreReceiptResponse;
  verifyGooglePlayReceipt: VerifyGooglePlayReceiptResponse;
}


export interface MutationAccountCorporateCreateArgs {
  input: AccountCorporateCreateInput;
}


export interface MutationAccountCorporateUpdateArgs {
  input: AccountCorporateUpdateInput;
}


export interface MutationAmpLinkMemberArgs {
  readerID: Scalars['String'];
  memberID: Scalars['String'];
  expiresAt: Scalars['String'];
  email: Scalars['String'];
}


export interface MutationAssetLockArgs {
  input: AssetLockInput;
}


export interface MutationAssetReadArgs {
  read: AssetReadInput;
}


export interface MutationAssetUnlockArgs {
  input: AssetUnlockInput;
}


export interface MutationClippingAddArgs {
  clipping: ClippingAddInput;
}


export interface MutationContentUnitSaveArgs {
  contentUnit: ContentUnitSaveInput;
}


export interface MutationCreateEntitlementTokenArgs {
  input: CreateEntitlementTokenInput;
}


export interface MutationCreateMemberArgs {
  input: CreateMemberInput;
}


export interface MutationCreateMemberFromSocialArgs {
  input: CreateMemberFromSocialInput;
}


export interface MutationCreateRunsheetArgs {
  input: CreateRunsheetInput;
}


export interface MutationCreateRunsheetLockArgs {
  runsheetID: Scalars['ID'];
}


export interface MutationCreateVideoUploadUrlArgs {
  video: VideoUploadUrlInput;
}


export interface MutationCtaUpdateConfigArgs {
  podcastInput?: Maybe<CtaUpdatePodcastConfigInput>;
}


export interface MutationDeleteRunsheetArgs {
  input: DeleteRunsheetInput;
}


export interface MutationIngestVideoArgs {
  input: IngestVideoInput;
}


export interface MutationLinkAppleStorePurchaseArgs {
  input: LinkAppleStorePurchaseInput;
}


export interface MutationMemberCorporateBulkInviteArgs {
  input: MemberCorporateBulkInviteInput;
}


export interface MutationMemberCorporateCreateArgs {
  input: MemberCorporateCreateInput;
}


export interface MutationMemberCorporateUpdateArgs {
  input: MemberCorporateUpdateInput;
}


export interface MutationMemberCorporateInviteArgs {
  input: MemberCorporateInviteInput;
}


export interface MutationMemberCorporateRevokeArgs {
  input: MemberCorporateRevokeInput;
}


export interface MutationMemberTagRecommendationAddArgs {
  input: MemberTagRecommendationAddInput;
}


export interface MutationMemberTagRecommendationPreferencesUpdateArgs {
  input: MemberTagRecommendationPreferencesUpdateInput;
}


export interface MutationNewsletterSubscribeArgs {
  subscription: NewsletterSubscribeInput;
}


export interface MutationLinkGooglePlayPurchaseArgs {
  input: LinkGooglePlayPurchaseInput;
}


export interface MutationPublishEntitlementArgs {
  input: PublishEntitlementInput;
}


export interface MutationPuzzlesSaveProgressArgs {
  input: PuzzleProgressInput;
}


export interface MutationSavedListAddAssetArgs {
  addAsset: SavedListAddAssetInput;
}


export interface MutationSavedListRemoveAssetArgs {
  removeAsset: SavedListRemoveAssetInput;
}


export interface MutationSendMemberPasswordResetEmailArgs {
  input: SendMemberPasswordResetEmailInput;
}


export interface MutationShortlistAddItemArgs {
  addItem: ShortlistAddItemInput;
}


export interface MutationShortlistMergeArgs {
  merge: ShortlistMergeInput;
}


export interface MutationShortlistRemoveItemArgs {
  removeItem: ShortlistRemoveItemInput;
}


export interface MutationSubscriptionCreateArgs {
  subscriptionInfo: SubscriptionCreateInput;
}


export interface MutationTagCreateArgs {
  tagInfo: TagCreateInput;
}


export interface MutationTagUpdateArgs {
  tagInfo: TagUpdateInput;
}


export interface MutationUpdateAccountArgs {
  account: UpdateAccountInput;
}


export interface MutationUpdateMemberNameArgs {
  input: UpdateMemberNameInput;
}


export interface MutationUpdatePageArgs {
  id: Scalars['ID'];
  config: PageConfigInput;
}


export interface MutationUpdateRunsheetLockArgs {
  runsheetID: Scalars['ID'];
}


export interface MutationUserTagsStatusUpdateArgs {
  input: UserTagsStatusUpdateInput;
}


export interface MutationVerifyAppleStoreReceiptArgs {
  input: VerifyAppleStoreReceiptInput;
}


export interface MutationVerifyGooglePlayReceiptArgs {
  input: VerifyGooglePlayReceiptInput;
}

export interface Navigation {
   __typename?: 'Navigation';
  navigations: Array<NavigationItem>;
}

export interface NavigationItem {
   __typename?: 'NavigationItem';
  id: Scalars['ID'];
  name: Scalars['String'];
  description?: Maybe<Scalars['String']>;
  url: Scalars['String'];
  children: Array<NavigationItem>;
}

export interface NavigationsError {
   __typename?: 'NavigationsError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface NavigationsResponse {
   __typename?: 'NavigationsResponse';
  error?: Maybe<NavigationsError>;
  navigation?: Maybe<Navigation>;
}

export interface NewsletterSubscribeInput {
  channelKey: Scalars['String'];
  email: Scalars['String'];
  newsletterID: Scalars['String'];
}

export enum Newsroom {
  Afr = 'AFR',
  Afrlockup = 'AFRLOCKUP',
  Metro = 'METRO',
  Metrolockup = 'METROLOCKUP'
}

export interface NewsroomBrandsError  extends Error {
   __typename?: 'NewsroomBrandsError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface NewsroomBrandsResponse {
   __typename?: 'NewsroomBrandsResponse';
  brands: Array<Brand>;
  details: Array<BrandDetails>;
  error?: Maybe<NewsroomBrandsError>;
}

export interface NewsroomCategoriesError  extends Error {
   __typename?: 'NewsroomCategoriesError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface NewsroomCategoriesResponse {
   __typename?: 'NewsroomCategoriesResponse';
  categories: Array<Category>;
  error?: Maybe<NewsroomCategoriesError>;
}

export interface NewsroomsError  extends Error {
   __typename?: 'NewsroomsError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface NewsroomsResponse {
   __typename?: 'NewsroomsResponse';
  error?: Maybe<NewsroomsError>;
  newsrooms: Array<Newsroom>;
}

export enum NewsroomType {
  Afr = 'AFR',
  Afrlockup = 'AFRLOCKUP',
  Metro = 'METRO',
  Metrolockup = 'METROLOCKUP'
}

export interface Notification {
   __typename?: 'Notification';
  message: Scalars['String'];
  event: Scalars['String'];
}

export enum NotificationCategory {
  Payment = 'PAYMENT',
  Promotion = 'PROMOTION'
}

export enum NotificationChannel {
  App = 'APP',
  Web = 'WEB'
}

export interface NotificationsError  extends Error {
   __typename?: 'NotificationsError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface NotificationsPendingResponse {
   __typename?: 'NotificationsPendingResponse';
  error?: Maybe<NotificationsError>;
  notifications: Array<Notification>;
}

export interface Page {
   __typename?: 'Page';
  config?: Maybe<PageConfig>;
  description?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  pageType: PageType;
  redirect?: Maybe<Scalars['String']>;
}

export interface PageConfig {
   __typename?: 'PageConfig';
  /** @deprecated Please, use the *ads* object. This one is added for backwards compatibility reasons. */
  adsDisabled?: Maybe<Scalars['Boolean']>;
  /** @deprecated There is no longer a need to restrict page configs to a particular page type. */
  pageType: Scalars['String'];
  ads?: Maybe<PageConfigAds>;
  nielsen?: Maybe<PageConfigNielsen>;
  seo?: Maybe<PageConfigSeo>;
  social?: Maybe<PageConfigSocial>;
}

export interface PageConfigAds {
   __typename?: 'PageConfigAds';
  suppress: Scalars['Boolean'];
}

export interface PageConfigInput {
  adsDisabled?: Maybe<Scalars['Boolean']>;
  pageType: PageType;
}

export interface PageConfigNielsen {
   __typename?: 'PageConfigNielsen';
  appID?: Maybe<Scalars['String']>;
  asn?: Maybe<Scalars['String']>;
  sega?: Maybe<Scalars['String']>;
}

export interface PageConfigSeo {
   __typename?: 'PageConfigSEO';
  canonical?: Maybe<Canonical>;
  description?: Maybe<Scalars['String']>;
  keywords?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
}

export interface PageConfigSocial {
   __typename?: 'PageConfigSocial';
  image?: Maybe<PageConfigSocialImage>;
}

export interface PageConfigSocialImage {
   __typename?: 'PageConfigSocialImage';
  height: Scalars['Int'];
  url: Scalars['String'];
  width: Scalars['Int'];
}

export interface PageContentError  extends Error {
   __typename?: 'PageContentError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface PageContentResponse {
   __typename?: 'PageContentResponse';
  contentUnits: Array<ContentUnit>;
  error?: Maybe<PageContentError>;
}

export interface PageInfo {
   __typename?: 'PageInfo';
  startCursor?: Maybe<Scalars['ID']>;
  endCursor?: Maybe<Scalars['ID']>;
  hasNextPage: Scalars['Boolean'];
  hasPreviousPage: Scalars['Boolean'];
  startOffset?: Maybe<Scalars['Int']>;
  endOffset?: Maybe<Scalars['Int']>;
}

export enum PageType {
  Article = 'article',
  Homepage = 'homepage',
  Section = 'section'
}

export interface PageUpdate {
   __typename?: 'PageUpdate';
  id: Scalars['ID'];
}

export enum PaymentType {
  UnknownPaymentType = 'UNKNOWN_PAYMENT_TYPE',
  CreditCard = 'CREDIT_CARD',
  Paypal = 'PAYPAL'
}

export interface PaypalPaymentInput {
  expressToken: Scalars['String'];
}

export interface PaywallExceptions {
   __typename?: 'PaywallExceptions';
  referrers?: Maybe<PaywallExceptionsReferrers>;
  referrers_variants?: Maybe<Array<Maybe<PaywallExceptionsReferrersVariants>>>;
  sponsored?: Maybe<Scalars['Boolean']>;
}

export interface PaywallExceptionsNewSession {
   __typename?: 'PaywallExceptionsNewSession';
  limit?: Maybe<Scalars['Int']>;
}

export interface PaywallExceptionsReferrers {
   __typename?: 'PaywallExceptionsReferrers';
  domains?: Maybe<Array<Maybe<Scalars['String']>>>;
  limit?: Maybe<Scalars['Int']>;
}

export interface PaywallExceptionsReferrersVariants {
   __typename?: 'PaywallExceptionsReferrersVariants';
  referrers?: Maybe<PaywallExceptionsReferrers>;
}

export interface PaywallInclusions {
   __typename?: 'PaywallInclusions';
  tags?: Maybe<Array<Scalars['String']>>;
}

export interface PaywallRule {
   __typename?: 'PaywallRule';
  enabled?: Maybe<Scalars['Boolean']>;
  exceptions?: Maybe<PaywallExceptions>;
  inclusions?: Maybe<PaywallInclusions>;
  limit?: Maybe<Scalars['Int']>;
  prompt?: Maybe<Prompt>;
  promptVariants?: Maybe<Array<Maybe<PromptVariants>>>;
  limit_variants?: Maybe<Array<Maybe<LimitVariants>>>;
  swgPrompt?: Maybe<SwgPrompt>;
}

export interface PaywallRuleData {
   __typename?: 'PaywallRuleData';
  meter: PaywallRuleMeterResponse;
  prompt?: Maybe<Prompt>;
  promptType: Scalars['String'];
  swgPrompt?: Maybe<SwgPrompt>;
}

export interface PaywallRuleError  extends Error {
   __typename?: 'PaywallRuleError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface PaywallRuleMeterResponse {
   __typename?: 'PaywallRuleMeterResponse';
  global: Scalars['Boolean'];
  referrer: Scalars['Boolean'];
}

export interface PaywallRuleRequestContext {
  alreadyMetered: Scalars['Boolean'];
  currentMeterCount: Scalars['Int'];
  referrer?: Maybe<Scalars['String']>;
}

export interface PaywallRuleRequestStory {
  brand: Brand;
  categories?: Maybe<Array<Scalars['String']>>;
  sponsored: Scalars['Boolean'];
  tags?: Maybe<Array<Scalars['String']>>;
  type: StoryType;
}

export interface PaywallRuleResponse {
   __typename?: 'PaywallRuleResponse';
  error?: Maybe<PaywallRuleError>;
  rule?: Maybe<PaywallRuleData>;
}

export interface Prompt {
   __typename?: 'Prompt';
  callToAction?: Maybe<Scalars['String']>;
  countRemaining?: Maybe<Scalars['String']>;
  message?: Maybe<Scalars['String']>;
  style?: Maybe<Scalars['String']>;
  subscriptionURL?: Maybe<Scalars['String']>;
  /** @deprecated Please use  *title* defined in the type *SwgPrompt*. *swgMessage* is available only for backwards compatibility. */
  swgMessage?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
}

export interface PromptVariants {
   __typename?: 'PromptVariants';
  /** @deprecated Please use the *categories* array. *category* is available only for backwards compatibility. */
  category?: Maybe<Scalars['String']>;
  prompt?: Maybe<Prompt>;
  categories?: Maybe<Array<Maybe<Scalars['String']>>>;
  swgPrompt?: Maybe<SwgPrompt>;
  tags?: Maybe<Array<Scalars['String']>>;
}

export interface PropertyByMetroError  extends Error {
   __typename?: 'PropertyByMetroError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface PropertyByMetroResponse {
   __typename?: 'PropertyByMetroResponse';
  assets: Array<Asset>;
  error?: Maybe<PropertyByMetroError>;
}

export interface PublishEntitlementInput {
  email: Scalars['String'];
  packageName: Scalars['String'];
  productId: Scalars['String'];
  token: Scalars['String'];
}

export interface PublishEntitlementResponse {
   __typename?: 'PublishEntitlementResponse';
  status: PublishEntitlementStatus;
}

export enum PublishEntitlementStatus {
  InProgress = 'inProgress',
  Success = 'success'
}

export interface PuzzleList {
   __typename?: 'PuzzleList';
  crosswords: Array<CrossWord>;
}

export interface PuzzleProgress {
   __typename?: 'PuzzleProgress';
  gameplay: Scalars['String'];
  puzzleId: Scalars['ID'];
  status: PuzzleProgressStatus;
  version: Scalars['Int'];
}

export interface PuzzleProgressConflict {
   __typename?: 'PuzzleProgressConflict';
  gameplay: Scalars['String'];
  updatedTime: Scalars['Time'];
  version: Scalars['Int'];
}

export interface PuzzleProgressError  extends Error {
   __typename?: 'PuzzleProgressError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface PuzzleProgressInput {
  gameplay: Scalars['String'];
  puzzleId: Scalars['ID'];
  puzzleType: PuzzleType;
  status: PuzzleProgressStatus;
  version?: Maybe<Scalars['Int']>;
}

export interface PuzzleProgressResponse {
   __typename?: 'PuzzleProgressResponse';
  conflict?: Maybe<PuzzleProgressConflict>;
  error?: Maybe<PuzzleProgressError>;
  version?: Maybe<Scalars['Int']>;
}

export enum PuzzleProgressStatus {
  Completed = 'COMPLETED',
  InProgress = 'IN_PROGRESS'
}

export interface PuzzlesByDateResponse {
   __typename?: 'PuzzlesByDateResponse';
  results?: Maybe<PuzzleList>;
  error?: Maybe<PuzzlesError>;
}

export interface PuzzlesError  extends Error {
   __typename?: 'PuzzlesError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface PuzzlesLoadProgressError  extends Error {
   __typename?: 'PuzzlesLoadProgressError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface PuzzlesLoadProgressResponse {
   __typename?: 'PuzzlesLoadProgressResponse';
  error?: Maybe<PuzzlesLoadProgressError>;
  results: Array<PuzzleProgress>;
}

export enum PuzzleType {
  Crossword = 'CROSSWORD',
  Sudoku = 'SUDOKU'
}

export interface Query {
   __typename?: 'Query';
  account?: Maybe<Account>;
  accountCorporate: AccountCorporateResponse;
  accountCorporateSearch: AccountCorporateSearchResponse;
  asset?: Maybe<Asset>;
  assetByExternalID?: Maybe<Asset>;
  assetCheckLock: AssetLock;
  assetsConnectionByCriteria: AssetsConnectionResponse;
  assetsConnectionByNavigationPath?: Maybe<AssetsConnection>;
  assetsConnectionByTag?: Maybe<AssetsConnection>;
  audienceSegments: AudienceSegmentsResponse;
  assetsConnectionByTags: AssetsConnectionResponse;
  author?: Maybe<Author>;
  /** @deprecated Please do not use this endpoint as future work will be done to fetch author data from api-author-profile */
  authorsByIds: AuthorsByIdsResponse;
  authorSearch?: Maybe<Array<Maybe<Author>>>;
  categories: Array<AssetCategory>;
  clippings: Array<Clipping>;
  clippingsLastUpdated: ClippingsLastUpdatedResponse;
  clippingsList: ClippingListResponse;
  commodityPrices: CommodityPricesResponse;
  companySearch: CompanySearchReply;
  contentUnit?: Maybe<ContentUnit>;
  crosswordsRecent: CrossWordsRecentResponse;
  ctaBySlot: CtaBySlotResponse;
  ctasConnection?: Maybe<CtasConnection>;
  currencyCompare: Array<CurrencyCompare>;
  dividendHistory: Array<DividendHistory>;
  dividendSummary: DividendSummary;
  entitlementsByPlans: EntitlementsByPlansResponse;
  fileUploadURL: FileUploadUrlResponse;
  financialCommoditiesBySymbols: FinancialCommoditiesBySymbolsResponse;
  financialForexRateByISOCode: FinancialForexRateResponse;
  indexPrices: Array<IndexPrice>;
  isAssetInSavedList: IsAssetInSavedListResponse;
  /** @deprecated Replaced by assetsConnectionByCriteria */
  latestAssets: Array<Asset>;
  listings: Array<Listing>;
  listingSearch: ListingSearchResponse;
  listingSearchEditorial: ListingSearchEditorialResponse;
  tempListings: Array<Listing>;
  memberCorporateDetail: MemberCorporateDetailResponse;
  memberCorporateImportStatus: MemberCorporateImportStatusResponse;
  memberCorporateSearch: MemberCorporateSearchResponse;
  memberCorporateStatus: MemberCorporateStatusResponse;
  memberDetails: MemberDetailsResponse;
  memberExternalSubscriptionProviders: MemberExternalSubscriptionProvidersResponse;
  memberSubscriptionDetails: MemberSubscriptionDetailsResponse;
  memberRecommendedTags: MemberRecommendedTagsResponse;
  memberRequestProgress: MemberRequestProgressResponse;
  memberCheckExists: MemberCheckExistsResponse;
  memberTagRecommendationPreferences: MemberTagRecommendationPreferencesResponse;
  mobileAppConfig: MobileAppConfigResponse;
  mobileAppClientPaywall: MobileAppClientPaywallResponse;
  mostPopularStories: Array<Asset>;
  mostUsedTags: MostUsedTagsResponse;
  navigations: NavigationsResponse;
  newsroomBrands: NewsroomBrandsResponse;
  newsroomCategories: NewsroomCategoriesResponse;
  newsrooms: NewsroomsResponse;
  notificationsPending: NotificationsPendingResponse;
  page?: Maybe<Page>;
  pageByNavigationPath?: Maybe<Page>;
  pageContentByPageType: PageContentResponse;
  paywallRule: PaywallRuleResponse;
  paywallRules?: Maybe<Rules>;
  propertyByMetro: PropertyByMetroResponse;
  publicSearch?: Maybe<AssetsConnection>;
  puzzlesByDate: PuzzlesByDateResponse;
  puzzlesProgressById: PuzzlesLoadProgressResponse;
  quoteFull: QuoteFull;
  quotesFull: QuotesFullResponse;
  quoteHistory: Array<QuoteHistory>;
  quoteIntradayTrades: QuoteIntradayTradesResponse;
  /** @deprecated Replaced by userReadingHistory */
  readingHistory: ReadingHistoryResponse;
  recommendAssets: Array<Asset>;
  runsheet: Runsheet;
  runsheets: Array<Runsheet>;
  runsheetSearch: RunsheetSearchResponse;
  savedList: SavedListResponse;
  /** @deprecated Replaced by ListingSearch query */
  search?: Maybe<AssetsConnection>;
  scoreboard: FootballScoreboard;
  shortlist: ShortlistResponse;
  sportsCompetitions: Array<Maybe<SportsCompetition>>;
  streetAddressSearch: StreetAddressSearchResponse;
  streetAddress: StreetAddressResponse;
  subscription: SubscriptionReply;
  tag?: Maybe<Tag>;
  todaysPaper: TodaysPaperResponse;
  /** @deprecated Please, use the *findTags* query for searching tags. This one is retained for backwards compatibility reasons. */
  searchTags: TagsReply;
  findTags: FindTagsReply;
  subscriptionPackage: SubscriptionPackageResponse;
  subscriptionPackagesByChannel: SubscriptionPackagesByChannelResponse;
  topBottomQuotes: Array<TopBottomQuote>;
  trending: Array<Asset>;
  userReadingHistory: UserReadingHistoryResponse;
  weatherForecast: Forecast;
  tagDetail: TagDetail;
  tagContexts: TagContextsReply;
  tagThemes: TagThemesReply;
  userTagsByStatus: UserTagsByStatusConnectionResponse;
  userTagsByTagIds: UserTagsByTagIdsResponse;
  verifyGoogleEntitlement: VerifyGoogleEntitlementResponse;
}


export interface QueryAccountArgs {
  id?: Maybe<Scalars['ID']>;
}


export interface QueryAccountCorporateArgs {
  id: Scalars['ID'];
}


export interface QueryAccountCorporateSearchArgs {
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  orderBy: AccountCorporateSearchOrder;
  searchTerm?: Maybe<AccountCorporateSearchTermInput>;
}


export interface QueryAssetArgs {
  id: Scalars['String'];
}


export interface QueryAssetByExternalIdArgs {
  externalID: Scalars['String'];
  systemName: Scalars['String'];
}


export interface QueryAssetCheckLockArgs {
  id: Scalars['String'];
}


export interface QueryAssetsConnectionByCriteriaArgs {
  after?: Maybe<Scalars['ID']>;
  brand: Brand;
  categories?: Maybe<Array<Scalars['Int']>>;
  first: Scalars['Int'];
  render: Render;
  types: Array<AssetType>;
}


export interface QueryAssetsConnectionByNavigationPathArgs {
  brand: Scalars['String'];
  path: Scalars['String'];
  count: Scalars['Int'];
  offset?: Maybe<Scalars['Int']>;
  types: Array<Scalars['String']>;
  sinceID?: Maybe<Scalars['ID']>;
  render?: Maybe<Render>;
}


export interface QueryAssetsConnectionByTagArgs {
  brand: Scalars['String'];
  count: Scalars['Int'];
  offset?: Maybe<Scalars['Int']>;
  render?: Maybe<Render>;
  sinceID?: Maybe<Scalars['ID']>;
  tagID: Scalars['String'];
  types?: Maybe<Array<AssetType>>;
}


export interface QueryAudienceSegmentsArgs {
  userId: Scalars['String'];
}


export interface QueryAssetsConnectionByTagsArgs {
  brand: Scalars['String'];
  tagIDs: Array<Scalars['String']>;
  types?: Maybe<Array<AssetType>>;
  periodInDays: Scalars['Int'];
  render?: Maybe<Render>;
  first: Scalars['Int'];
  after?: Maybe<Scalars['ID']>;
}


export interface QueryAuthorArgs {
  id: Scalars['ID'];
}


export interface QueryAuthorsByIdsArgs {
  ids: Array<Scalars['ID']>;
}


export interface QueryAuthorSearchArgs {
  name: Scalars['String'];
}


export interface QueryCommodityPricesArgs {
  codes: Array<Scalars['String']>;
}


export interface QueryCompanySearchArgs {
  options: CompanySearchInput;
}


export interface QueryContentUnitArgs {
  id: Scalars['Int'];
  includeUnpublished?: Maybe<Scalars['Boolean']>;
  brand?: Maybe<Scalars['String']>;
}


export interface QueryCrosswordsRecentArgs {
  first: Scalars['Int'];
}


export interface QueryCtaBySlotArgs {
  slotName: Scalars['String'];
}


export interface QueryCtasConnectionArgs {
  count: Scalars['Int'];
  offset?: Maybe<Scalars['Int']>;
}


export interface QueryCurrencyCompareArgs {
  from: CurrencyCode;
  to: Array<CurrencyCode>;
}


export interface QueryDividendHistoryArgs {
  code: Scalars['String'];
  maxResults: Scalars['Int'];
}


export interface QueryDividendSummaryArgs {
  code: Scalars['String'];
}


export interface QueryEntitlementsByPlansArgs {
  plans: Array<Scalars['String']>;
}


export interface QueryFileUploadUrlArgs {
  accountId: Scalars['ID'];
  fileName: Scalars['String'];
}


export interface QueryFinancialCommoditiesBySymbolsArgs {
  symbols: Array<Scalars['String']>;
}


export interface QueryFinancialForexRateByIsoCodeArgs {
  from: Scalars['String'];
  to: Scalars['String'];
}


export interface QueryIndexPricesArgs {
  indexCodes?: Maybe<Array<Scalars['String']>>;
  globalIndexCodes?: Maybe<Array<Scalars['String']>>;
}


export interface QueryIsAssetInSavedListArgs {
  input: IsAssetInSavedListInput;
}


export interface QueryLatestAssetsArgs {
  brand: Scalars['String'];
  categories?: Maybe<Array<Scalars['Int']>>;
  count: Scalars['Int'];
  render: Scalars['String'];
  types: Array<AssetType>;
}


export interface QueryListingsArgs {
  filter: ListingsFilterType;
  orderBy: ListingsOrderType;
  ascending?: Maybe<Scalars['Boolean']>;
}


export interface QueryListingSearchArgs {
  assignee?: Maybe<Scalars['String']>;
  brands?: Maybe<Array<Brand>>;
  categories?: Maybe<Array<Scalars['String']>>;
  dateFrom?: Maybe<Scalars['Time']>;
  dateTo?: Maybe<Scalars['Time']>;
  dateTypes?: Maybe<Array<ListingsOrderType>>;
  editingState?: Maybe<ListingsFilterType>;
  editingStates?: Maybe<Array<ListingsFilterType>>;
  identifier?: Maybe<Scalars['String']>;
  includeUnpublished?: Maybe<Scalars['Boolean']>;
  newsrooms?: Maybe<Array<Newsroom>>;
  offset?: Maybe<Scalars['Int']>;
  sources?: Maybe<Array<Scalars['String']>>;
  storyTypes?: Maybe<Array<AssetType>>;
  tags?: Maybe<Array<Scalars['String']>>;
  query: Scalars['String'];
}


export interface QueryListingSearchEditorialArgs {
  assignee?: Maybe<Scalars['String']>;
  brands?: Maybe<Array<Brand>>;
  categories?: Maybe<Array<Scalars['String']>>;
  dateFrom?: Maybe<Scalars['Time']>;
  dateTo?: Maybe<Scalars['Time']>;
  dateTypes?: Maybe<Array<ListingsOrderType>>;
  editingState?: Maybe<ListingsFilterType>;
  editingStates?: Maybe<Array<ListingsFilterType>>;
  identifier?: Maybe<Scalars['String']>;
  includeUnpublished?: Maybe<Scalars['Boolean']>;
  legalStatus?: Maybe<Array<LegalFlag>>;
  newsrooms?: Maybe<Array<Newsroom>>;
  offset?: Maybe<Scalars['Int']>;
  sources?: Maybe<Array<Scalars['String']>>;
  storyTypes?: Maybe<Array<AssetType>>;
  tags?: Maybe<Array<Scalars['String']>>;
  query: Scalars['String'];
}


export interface QueryTempListingsArgs {
  filter: ListingsFilterType;
  orderBy: ListingsOrderType;
  ascending?: Maybe<Scalars['Boolean']>;
}


export interface QueryMemberCorporateDetailArgs {
  accountId: Scalars['ID'];
  memberId: Scalars['ID'];
}


export interface QueryMemberCorporateImportStatusArgs {
  headerId: Scalars['ID'];
}


export interface QueryMemberCorporateSearchArgs {
  accountId: Scalars['ID'];
  filter?: Maybe<MemberCorporateSearchFilterInput>;
  limit?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  order: MemberCorporateSearchOrder;
  term?: Maybe<MemberCorporateSearchTermInput>;
}


export interface QueryMemberCorporateStatusArgs {
  memberId?: Maybe<Scalars['ID']>;
  token?: Maybe<Scalars['String']>;
}


export interface QueryMemberRecommendedTagsArgs {
  brand: Brand;
  count: Scalars['Int'];
}


export interface QueryMemberRequestProgressArgs {
  requestID: Scalars['String'];
}


export interface QueryMemberCheckExistsArgs {
  email: Scalars['String'];
}


export interface QueryMobileAppConfigArgs {
  appVersion: Scalars['String'];
  env: MobileConfigEnv;
  platform: MobileConfigPlatform;
  platformVersion: Scalars['String'];
  product: MobileConfigProduct;
}


export interface QueryMobileAppClientPaywallArgs {
  context: MobileAppClientPaywallRequestContext;
}


export interface QueryMostPopularStoriesArgs {
  assetType?: Maybe<Scalars['String']>;
  brand: Scalars['String'];
  category?: Maybe<Scalars['String']>;
  count?: Maybe<Scalars['Int']>;
  subCategory?: Maybe<Scalars['String']>;
  tags?: Maybe<Array<Scalars['String']>>;
}


export interface QueryMostUsedTagsArgs {
  brand: Brand;
  count: Scalars['Int'];
  periodInDays: Scalars['Int'];
  tagContext?: Maybe<TagContextType>;
}


export interface QueryNavigationsArgs {
  brand: Brand;
  render: Render;
}


export interface QueryNewsroomBrandsArgs {
  newsrooms: Array<Newsroom>;
}


export interface QueryNewsroomCategoriesArgs {
  newsrooms: Array<Newsroom>;
}


export interface QueryNotificationsPendingArgs {
  brand: Brand;
  category: NotificationCategory;
  channel: NotificationChannel;
}


export interface QueryPageArgs {
  id: Scalars['ID'];
}


export interface QueryPageByNavigationPathArgs {
  brand: Scalars['String'];
  render: Scalars['String'];
  path: Scalars['String'];
}


export interface QueryPageContentByPageTypeArgs {
  brand: Brand;
  pageType: PageType;
  render: Render;
}


export interface QueryPaywallRuleArgs {
  context: PaywallRuleRequestContext;
  story: PaywallRuleRequestStory;
}


export interface QueryPaywallRulesArgs {
  brand: Scalars['String'];
}


export interface QueryPropertyByMetroArgs {
  brand: Brand;
}


export interface QueryPublicSearchArgs {
  brand: Scalars['String'];
  dateFrom?: Maybe<Scalars['Time']>;
  dateTo?: Maybe<Scalars['Time']>;
  query: Scalars['String'];
  offset?: Maybe<Scalars['Int']>;
  pageSize: Scalars['Int'];
  sortBy?: Maybe<AssetSortBy>;
  filterBy?: Maybe<AssetFilterByInput>;
}


export interface QueryPuzzlesByDateArgs {
  date: Scalars['String'];
}


export interface QueryPuzzlesProgressByIdArgs {
  ids: Array<Scalars['ID']>;
  puzzleType: PuzzleType;
}


export interface QueryQuoteFullArgs {
  code: Scalars['String'];
}


export interface QueryQuotesFullArgs {
  securityCodes: Array<Scalars['String']>;
}


export interface QueryQuoteHistoryArgs {
  code: Scalars['String'];
  maxResults: Scalars['Int'];
}


export interface QueryQuoteIntradayTradesArgs {
  code: Scalars['String'];
  timeIntervalInMinutes: Scalars['Int'];
}


export interface QueryRecommendAssetsArgs {
  type: Scalars['String'];
  brand: Scalars['String'];
  assetID: Scalars['String'];
  count: Scalars['Int'];
}


export interface QueryRunsheetArgs {
  id: Scalars['ID'];
}


export interface QueryRunsheetsArgs {
  orderBy?: Maybe<RunsheetsOrderType>;
  ascending?: Maybe<Scalars['Boolean']>;
}


export interface QueryRunsheetSearchArgs {
  ascending?: Maybe<Scalars['Boolean']>;
  brands?: Maybe<Array<Brand>>;
  categories?: Maybe<Array<Scalars['Int']>>;
  category?: Maybe<Scalars['Int']>;
  dateFrom?: Maybe<Scalars['Time']>;
  dateTo?: Maybe<Scalars['Time']>;
  name?: Maybe<Scalars['String']>;
  newsroom?: Maybe<Newsroom>;
  newsrooms?: Maybe<Array<NewsroomType>>;
  orderBy?: Maybe<RunsheetsOrderType>;
  productionDate?: Maybe<Scalars['String']>;
}


export interface QuerySavedListArgs {
  brand: Brand;
  count?: Maybe<Scalars['Int']>;
  sinceID?: Maybe<Scalars['String']>;
}


export interface QuerySearchArgs {
  brand?: Maybe<Scalars['String']>;
  query: Scalars['String'];
  includeUnpublished?: Maybe<Scalars['Boolean']>;
  offset?: Maybe<Scalars['Int']>;
  brands?: Maybe<Array<Scalars['String']>>;
  dateFrom?: Maybe<Scalars['String']>;
  dateTo?: Maybe<Scalars['String']>;
  editingState?: Maybe<Scalars['String']>;
}


export interface QueryScoreboardArgs {
  id: Scalars['ID'];
}


export interface QueryShortlistArgs {
  deviceId?: Maybe<Scalars['String']>;
}


export interface QueryStreetAddressSearchArgs {
  client: Scalars['String'];
  partialAddress: Scalars['String'];
}


export interface QueryStreetAddressArgs {
  client: Scalars['String'];
  id: Scalars['ID'];
}


export interface QuerySubscriptionArgs {
  requestID: Scalars['String'];
}


export interface QueryTagArgs {
  brand: Scalars['String'];
  id: Scalars['ID'];
}


export interface QueryTodaysPaperArgs {
  brand: Scalars['String'];
}


export interface QuerySearchTagsArgs {
  search: Scalars['String'];
  field: TagSearchField;
  currentID?: Maybe<Scalars['String']>;
  pageSize?: Maybe<Scalars['Int']>;
  filterType?: Maybe<Scalars['Int']>;
  filterValue?: Maybe<Scalars['Int']>;
}


export interface QueryFindTagsArgs {
  query: Scalars['String'];
  field: FindTagsField;
  currentID?: Maybe<Scalars['ID']>;
  pageSize?: Maybe<Scalars['Int']>;
  filterType?: Maybe<FindTagsFilterType>;
  filterValue?: Maybe<Scalars['Int']>;
  exclude?: Maybe<FindTagsExcludeInput>;
  offset?: Maybe<Scalars['Int']>;
}


export interface QuerySubscriptionPackageArgs {
  id: Scalars['String'];
  promotionCode?: Maybe<Scalars['String']>;
}


export interface QuerySubscriptionPackagesByChannelArgs {
  brand: Brand;
  channel: SubscriptionChannel;
  from?: Maybe<Scalars['String']>;
}


export interface QueryTopBottomQuotesArgs {
  indexCode?: Maybe<Scalars['String']>;
}


export interface QueryTrendingArgs {
  brand: Scalars['String'];
  count?: Maybe<Scalars['Int']>;
}


export interface QueryUserReadingHistoryArgs {
  after?: Maybe<Scalars['ID']>;
  first: Scalars['Int'];
  newsroom?: Maybe<Newsroom>;
}


export interface QueryWeatherForecastArgs {
  postcode: Scalars['String'];
  location: Scalars['String'];
}


export interface QueryTagDetailArgs {
  id: Scalars['ID'];
}


export interface QueryUserTagsByStatusArgs {
  after?: Maybe<Scalars['String']>;
  brand: Brand;
  first: Scalars['Int'];
  status: UserTagStatus;
}


export interface QueryUserTagsByTagIdsArgs {
  brand: Brand;
  tagIDs: Array<Scalars['ID']>;
}


export interface QueryVerifyGoogleEntitlementArgs {
  packageName: Scalars['String'];
  productID: Scalars['String'];
  token: Scalars['String'];
}

export interface QuoteDividend {
   __typename?: 'QuoteDividend';
  value: Scalars['Float'];
  yield: Scalars['Float'];
}

export interface QuoteFull {
   __typename?: 'QuoteFull';
  averageVolume30Day: Scalars['Float'];
  dividend: QuoteDividend;
  lastUpdated: Scalars['String'];
  price: QuotePrice;
  security: QuoteSecurity;
  value: Scalars['Float'];
  week52: QuoteWeek52;
}

export interface QuoteHistory {
   __typename?: 'QuoteHistory';
  close: Scalars['Float'];
  cumulativeDilutionFactor: Scalars['Float'];
  date: Scalars['String'];
  high: Scalars['Float'];
  low: Scalars['Float'];
  open: Scalars['Float'];
  volume: Scalars['Float'];
}

export interface QuoteIntradayTrade {
   __typename?: 'QuoteIntradayTrade';
  code: Scalars['String'];
  date: Scalars['String'];
  salePrice: Scalars['Float'];
  value: Scalars['Float'];
  volume: Scalars['Int'];
}

export interface QuoteIntradayTradesError {
   __typename?: 'QuoteIntradayTradesError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface QuoteIntradayTradesResponse {
   __typename?: 'QuoteIntradayTradesResponse';
  error?: Maybe<QuoteIntradayTradesError>;
  trades: Array<QuoteIntradayTrade>;
}

export interface QuoteIssuer {
   __typename?: 'QuoteIssuer';
  name: IssuerName;
  principalActivity: Scalars['String'];
}

export interface QuotePrice {
   __typename?: 'QuotePrice';
  high: Scalars['Float'];
  last: Scalars['Float'];
  low: Scalars['Float'];
  movement: Scalars['Float'];
  movementPercentage: Scalars['Float'];
  open: Scalars['Float'];
  previousClose: Scalars['Float'];
  volume: Scalars['Float'];
}

export interface QuoteSecurity {
   __typename?: 'QuoteSecurity';
  asxCode: Scalars['String'];
  issuer: QuoteIssuer;
}

export interface QuotesFullError {
   __typename?: 'QuotesFullError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface QuotesFullResponse {
   __typename?: 'QuotesFullResponse';
  error?: Maybe<QuotesFullError>;
  quotes: Array<QuoteFull>;
}

export interface QuoteWeek52 {
   __typename?: 'QuoteWeek52';
  earningsYield: Scalars['Float'];
  high: Scalars['Float'];
  low: Scalars['Float'];
  marketCapitalization: Scalars['Float'];
  priceEarningsRatio: Scalars['Float'];
}

export interface ReadingHistory {
   __typename?: 'ReadingHistory';
  id: Scalars['ID'];
  assetsConnection?: Maybe<AssetsConnection>;
}


export interface ReadingHistoryAssetsConnectionArgs {
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
}

export interface ReadingHistoryError {
   __typename?: 'ReadingHistoryError';
  message: Scalars['String'];
  type: ReadingHistoryErrorType;
}

export enum ReadingHistoryErrorType {
  AuthenticationError = 'AUTHENTICATION_ERROR',
  GenericError = 'GENERIC_ERROR'
}

export interface ReadingHistoryResponse {
   __typename?: 'ReadingHistoryResponse';
  error?: Maybe<ReadingHistoryError>;
  readingHistory?: Maybe<ReadingHistory>;
}

export interface RecommendedTag {
   __typename?: 'RecommendedTag';
  displayName: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
}

export enum Render {
  MobileApp = 'MOBILE_APP',
  Web = 'WEB'
}

export interface RequestFieldError {
   __typename?: 'RequestFieldError';
  field: Scalars['String'];
  message: Scalars['String'];
}

export interface RestContentQuery  extends MobileConfigMenuItemContentQuery {
   __typename?: 'RestContentQuery';
  name: Scalars['String'];
}

export interface Rules {
   __typename?: 'Rules';
  meters?: Maybe<Array<Maybe<MeterRule>>>;
  paywall?: Maybe<PaywallRule>;
  meters_variants?: Maybe<Array<Maybe<MetersVariants>>>;
}

export interface Runsheet {
   __typename?: 'Runsheet';
  Data?: Maybe<RunsheetData>;
  ID: Scalars['ID'];
  UserID: Scalars['String'];
  LockedBy: Scalars['String'];
}

export interface RunsheetData {
   __typename?: 'RunsheetData';
  brands: Array<Scalars['String']>;
  category: Scalars['Int'];
  divisions: Array<RunsheetDivision>;
  name: Scalars['String'];
  newsroom: NewsroomType;
  /** @deprecated Only Newsroom field will be used from now on */
  newsrooms: Array<Newsroom>;
  productionDate?: Maybe<Scalars['String']>;
  watchers?: Maybe<Array<Scalars['String']>>;
}

export interface RunsheetDivision {
   __typename?: 'RunsheetDivision';
  description?: Maybe<Scalars['String']>;
  listings: Array<Listing>;
  title: Scalars['String'];
}

export interface RunsheetDivisionInput {
  description?: Maybe<Scalars['String']>;
  listings: Array<Scalars['ID']>;
  title: Scalars['String'];
}

export interface RunsheetSearchDivision {
   __typename?: 'RunsheetSearchDivision';
  description?: Maybe<Scalars['String']>;
  listingIDs: Array<Scalars['ID']>;
  title: Scalars['String'];
}

export interface RunsheetSearchError  extends Error {
   __typename?: 'RunsheetSearchError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface RunsheetSearchResponse {
   __typename?: 'RunsheetSearchResponse';
  error?: Maybe<RunsheetSearchError>;
  results: Array<RunsheetSearchResult>;
}

export interface RunsheetSearchResult {
   __typename?: 'RunsheetSearchResult';
  brands: Array<Scalars['String']>;
  category: Scalars['Int'];
  divisions: Array<RunsheetSearchDivision>;
  id: Scalars['ID'];
  lockedBy: Scalars['String'];
  name: Scalars['String'];
  newsroom: NewsroomType;
  /** @deprecated Only Newsroom field will be used from now on */
  newsrooms: Array<Newsroom>;
  productionDate?: Maybe<Scalars['String']>;
  userID: Scalars['String'];
  watchers?: Maybe<Array<Scalars['String']>>;
}

export enum RunsheetsOrderType {
  LastModified = 'LAST_MODIFIED',
  ProductionDate = 'PRODUCTION_DATE'
}

export interface SavedListAddAssetError {
   __typename?: 'SavedListAddAssetError';
  message: Scalars['String'];
  type: SavedListAddAssetErrorType;
}

export enum SavedListAddAssetErrorType {
  AuthenticationError = 'AUTHENTICATION_ERROR',
  GenericError = 'GENERIC_ERROR',
  InvalidRequestError = 'INVALID_REQUEST_ERROR'
}

export interface SavedListAddAssetInput {
  assetID: Scalars['String'];
  brand: Brand;
}

export interface SavedListAddAssetResponse {
   __typename?: 'SavedListAddAssetResponse';
  error?: Maybe<SavedListAddAssetError>;
}

export interface SavedListAssetsResponse {
   __typename?: 'SavedListAssetsResponse';
  asset: Asset;
  date: Scalars['String'];
}

export interface SavedListError {
   __typename?: 'SavedListError';
  message: Scalars['String'];
  type: SavedListErrorType;
}

export enum SavedListErrorType {
  AuthenticationError = 'AUTHENTICATION_ERROR',
  GenericError = 'GENERIC_ERROR',
  InvalidRequestError = 'INVALID_REQUEST_ERROR'
}

export interface SavedListRemoveAssetError {
   __typename?: 'SavedListRemoveAssetError';
  message: Scalars['String'];
  type: SavedListRemoveAssetErrorType;
  assetIDs: Array<Scalars['String']>;
}

export enum SavedListRemoveAssetErrorType {
  AuthenticationError = 'AUTHENTICATION_ERROR',
  GenericError = 'GENERIC_ERROR',
  InvalidRequestError = 'INVALID_REQUEST_ERROR'
}

export interface SavedListRemoveAssetInput {
  assetIDs: Array<Scalars['String']>;
  brand: Brand;
}

export interface SavedListRemoveAssetResponse {
   __typename?: 'SavedListRemoveAssetResponse';
  error?: Maybe<SavedListRemoveAssetError>;
}

export interface SavedListResponse {
   __typename?: 'SavedListResponse';
  /** @deprecated Replaced by savedAssets */
  assets: Array<Asset>;
  error?: Maybe<SavedListError>;
  savedAssets: Array<SavedListAssetsResponse>;
}

export interface ScoreboardData {
   __typename?: 'ScoreboardData';
  gameID?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
  type?: Maybe<Scalars['String']>;
}

export enum SegmentType {
  Churn = 'CHURN',
  Engagement = 'ENGAGEMENT',
  Invalid = 'INVALID',
  Propensity = 'PROPENSITY'
}

export interface SendMemberPasswordResetEmailInput {
  email: Scalars['String'];
  brand: Brand;
}

export interface SendMemberPasswordResetEmailResponse {
   __typename?: 'SendMemberPasswordResetEmailResponse';
  error?: Maybe<AccountError>;
  requestID: Scalars['String'];
}

export interface Shortlist {
   __typename?: 'Shortlist';
  id?: Maybe<Scalars['ID']>;
  items?: Maybe<Array<Maybe<ShortlistItem>>>;
  assetsConnection?: Maybe<AssetsConnection>;
}


export interface ShortlistAssetsConnectionArgs {
  first?: Maybe<Scalars['Int']>;
  after?: Maybe<Scalars['String']>;
}

export interface ShortlistAddItemError {
   __typename?: 'ShortlistAddItemError';
  message: Scalars['String'];
  type: ShortlistAddItemErrorType;
}

export enum ShortlistAddItemErrorType {
  AuthenticationError = 'AUTHENTICATION_ERROR',
  GenericError = 'GENERIC_ERROR',
  InvalidRequestError = 'INVALID_REQUEST_ERROR'
}

export interface ShortlistAddItemInput {
  deviceId?: Maybe<Scalars['String']>;
  itemId: Scalars['String'];
  clientUtcOffsetMinutes: Scalars['Int'];
}

export interface ShortlistAddItemReference {
   __typename?: 'ShortlistAddItemReference';
  shortlistId: Scalars['ID'];
  item?: Maybe<ShortlistItem>;
  edge?: Maybe<AssetEdge>;
}

export interface ShortlistAddItemReply {
   __typename?: 'ShortlistAddItemReply';
  error?: Maybe<ShortlistAddItemError>;
  addedItem?: Maybe<ShortlistAddItemReference>;
}

export interface ShortlistError {
   __typename?: 'ShortlistError';
  message: Scalars['String'];
  type: ShortlistErrorType;
}

export enum ShortlistErrorType {
  AuthenticationError = 'AUTHENTICATION_ERROR',
  GenericError = 'GENERIC_ERROR',
  InvalidRequestError = 'INVALID_REQUEST_ERROR'
}

export interface ShortlistItem {
   __typename?: 'ShortlistItem';
  id: Scalars['ID'];
  assetId: Scalars['ID'];
  shortlistedDate?: Maybe<Scalars['String']>;
}

export interface ShortlistMergeError {
   __typename?: 'ShortlistMergeError';
  message: Scalars['String'];
  type: ShortlistMergeErrorType;
}

export enum ShortlistMergeErrorType {
  AuthenticationError = 'AUTHENTICATION_ERROR',
  GenericError = 'GENERIC_ERROR'
}

export interface ShortlistMergeInput {
  deviceId: Scalars['String'];
}

export interface ShortlistMergeReply {
   __typename?: 'ShortlistMergeReply';
  error?: Maybe<ShortlistMergeError>;
}

export interface ShortlistRemoveItemError {
   __typename?: 'ShortlistRemoveItemError';
  message: Scalars['String'];
  type: ShortlistRemoveItemErrorType;
}

export enum ShortlistRemoveItemErrorType {
  AuthenticationError = 'AUTHENTICATION_ERROR',
  GenericError = 'GENERIC_ERROR',
  InvalidRequestError = 'INVALID_REQUEST_ERROR'
}

export interface ShortlistRemoveItemInput {
  deviceId?: Maybe<Scalars['String']>;
  itemId: Scalars['String'];
}

export interface ShortlistRemoveItemReference {
   __typename?: 'ShortlistRemoveItemReference';
  shortlistId: Scalars['ID'];
  itemId: Scalars['String'];
}

export interface ShortlistRemoveItemReply {
   __typename?: 'ShortlistRemoveItemReply';
  error?: Maybe<ShortlistRemoveItemError>;
  removedItem?: Maybe<ShortlistRemoveItemReference>;
}

export interface ShortlistResponse {
   __typename?: 'ShortlistResponse';
  error?: Maybe<ShortlistError>;
  shortlist?: Maybe<Shortlist>;
}

export enum SocialLoginProvider {
  Apple = 'APPLE',
  Google = 'GOOGLE'
}

export interface SportsCompetition {
   __typename?: 'SportsCompetition';
  active: Scalars['Boolean'];
  competitionID: Scalars['Int'];
  displayName: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
  type: Scalars['String'];
}

export enum StoryType {
  Article = 'ARTICLE',
  Bespoke = 'BESPOKE',
  FeatureArticle = 'FEATURE_ARTICLE',
  Gallery = 'GALLERY',
  LiveArticle = 'LIVE_ARTICLE',
  Video = 'VIDEO'
}

export interface StreetAddress {
   __typename?: 'StreetAddress';
  postcode?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  streetName?: Maybe<Scalars['String']>;
  streetNumber?: Maybe<Scalars['String']>;
  streetType?: Maybe<Scalars['String']>;
  suburb?: Maybe<Scalars['String']>;
  unitNumber?: Maybe<Scalars['String']>;
}

export interface StreetAddressError  extends Error {
   __typename?: 'StreetAddressError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface StreetAddressResponse {
   __typename?: 'StreetAddressResponse';
  address?: Maybe<StreetAddress>;
  error?: Maybe<StreetAddressError>;
}

export interface StreetAddressSearchError  extends Error {
   __typename?: 'StreetAddressSearchError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface StreetAddressSearchResponse {
   __typename?: 'StreetAddressSearchResponse';
  error?: Maybe<StreetAddressSearchError>;
  partialAddress: Scalars['String'];
  suggestions: Array<StreetAddressSuggestion>;
}

export interface StreetAddressSuggestion {
   __typename?: 'StreetAddressSuggestion';
  address: Scalars['String'];
  id: Scalars['ID'];
}

export enum StreetType {
  UnknownStreetType = 'UNKNOWN_STREET_TYPE',
  Street = 'STREET',
  Road = 'ROAD',
  Avenue = 'AVENUE',
  Drive = 'DRIVE',
  Place = 'PLACE',
  Parade = 'PARADE',
  Bay = 'BAY',
  Link = 'LINK',
  Brae = 'BRAE',
  Dell = 'DELL',
  Nook = 'NOOK',
  Ride = 'RIDE',
  Vue = 'VUE',
  Hub = 'HUB',
  Trail = 'TRAIL',
  Centre = 'CENTRE',
  Village = 'VILLAGE',
  Bend = 'BEND',
  Concourse = 'CONCOURSE',
  Glade = 'GLADE',
  Point = 'POINT',
  Track = 'TRACK',
  Chase = 'CHASE',
  Retreat = 'RETREAT',
  Promenade = 'PROMENADE',
  Wynd = 'WYND',
  Ridge = 'RIDGE',
  Divide = 'DIVIDE',
  Alley = 'ALLEY',
  Mews = 'MEWS',
  Cove = 'COVE',
  Crest = 'CREST',
  Dale = 'DALE',
  Esplanade = 'ESPLANADE',
  Garden = 'GARDEN',
  Glen = 'GLEN',
  Grove = 'GROVE',
  Grange = 'GRANGE',
  Heath = 'HEATH',
  Boulevarde = 'BOULEVARDE',
  Parkway = 'PARKWAY',
  Quad = 'QUAD',
  Quay = 'QUAY',
  Roadway = 'ROADWAY',
  Terrace = 'TERRACE',
  Run = 'RUN',
  Gate = 'GATE',
  Haven = 'HAVEN',
  Pathway = 'PATHWAY',
  Arcade = 'ARCADE',
  Circuit = 'CIRCUIT',
  Circle = 'CIRCLE',
  Close = 'CLOSE',
  Crescent = 'CRESCENT',
  Course = 'COURSE',
  Court = 'COURT',
  Gardens = 'GARDENS',
  Green = 'GREEN',
  Gateway = 'GATEWAY',
  Hill = 'HILL',
  Heights = 'HEIGHTS',
  Highway = 'HIGHWAY',
  Isle = 'ISLE',
  Lane = 'LANE',
  Loop = 'LOOP',
  Mall = 'MALL',
  Park = 'PARK',
  Pass = 'PASS',
  Rise = 'RISE',
  Row = 'ROW',
  Square = 'SQUARE',
  View = 'VIEW',
  Walk = 'WALK',
  Way = 'WAY',
  Beach = 'BEACH',
  Common = 'COMMON',
  Ford = 'FORD',
  Lodge = 'LODGE',
  Round = 'ROUND',
  Junction = 'JUNCTION',
  Vista = 'VISTA',
  Key = 'KEY',
  Port = 'PORT',
  Inlet = 'INLET',
  Plaza = 'PLAZA',
  Return = 'RETURN',
  Reserve = 'RESERVE',
  Oval = 'OVAL',
  Corner = 'CORNER',
  Range = 'RANGE'
}

export interface SubscribeReply {
   __typename?: 'SubscribeReply';
  success: Scalars['Boolean'];
}

export enum SubscriptionChannel {
  Android = 'ANDROID',
  Ios = 'IOS'
}

export interface SubscriptionCreateInput {
  agreementMarketing: Scalars['Boolean'];
  agreementTerms: Scalars['Boolean'];
  billingAddress?: Maybe<BillingAddressInput>;
  deliveryAddress?: Maybe<DeliveryAddressInput>;
  internationalUserAgreement: Scalars['Boolean'];
  bundle: Scalars['String'];
  site: Scalars['String'];
  email: Scalars['String'];
  paymentType: PaymentType;
  paypal?: Maybe<PaypalPaymentInput>;
  creditCard?: Maybe<CreditCardInput>;
  phoneNumber?: Maybe<Scalars['String']>;
  promoteChannel?: Maybe<Scalars['String']>;
  promotionCode?: Maybe<Scalars['String']>;
  gstExemptNz?: Maybe<Scalars['Boolean']>;
}

export interface SubscriptionCreateReply {
   __typename?: 'SubscriptionCreateReply';
  requestId: Scalars['String'];
}

export interface SubscriptionPackage  extends SubscriptionPackageCommon {
   __typename?: 'SubscriptionPackage';
  billingText: Scalars['String'];
  id: Scalars['ID'];
  discountedPeriods?: Maybe<Scalars['Int']>;
  discountedPrice?: Maybe<Scalars['Int']>;
  displayName: Scalars['String'];
  displayNameShort: Scalars['String'];
  features: Array<Scalars['String']>;
  isPrint: Scalars['Boolean'];
  isYearly: Scalars['Boolean'];
  packageID: Scalars['String'];
  pricingText: Scalars['String'];
  promotionApplied?: Maybe<Scalars['Boolean']>;
  renewalTerm: Scalars['Int'];
  smallImage: Scalars['String'];
  regularPrice: Scalars['Int'];
  regularPriceFrequency: Scalars['String'];
}

export interface SubscriptionPackageCommon {
  billingText: Scalars['String'];
  id: Scalars['ID'];
  discountedPeriods?: Maybe<Scalars['Int']>;
  discountedPrice?: Maybe<Scalars['Int']>;
  displayName: Scalars['String'];
  displayNameShort: Scalars['String'];
  features: Array<Scalars['String']>;
  packageID: Scalars['String'];
  pricingText: Scalars['String'];
  renewalTerm: Scalars['Int'];
  smallImage: Scalars['String'];
}

export interface SubscriptionPackageDiscountPrice {
   __typename?: 'SubscriptionPackageDiscountPrice';
  messages: SubscriptionPackageDiscountPriceMessages;
}

export interface SubscriptionPackageDiscountPriceMessages {
   __typename?: 'SubscriptionPackageDiscountPriceMessages';
  price: SubscriptionPackagePriceMessage;
  struckPrice: SubscriptionPackagePriceMessage;
}

export interface SubscriptionPackageError  extends Error {
   __typename?: 'SubscriptionPackageError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface SubscriptionPackageFeatures {
   __typename?: 'SubscriptionPackageFeatures';
  nonSubscriber: SubscriptionPackageFeaturesNonSubscriber;
  subscribeNow: SubscriptionPackageFeaturesSubscribeNow;
}

export interface SubscriptionPackageFeaturesNonSubscriber {
   __typename?: 'SubscriptionPackageFeaturesNonSubscriber';
  text: Array<Scalars['String']>;
}

export interface SubscriptionPackageFeaturesSubscribeNow {
   __typename?: 'SubscriptionPackageFeaturesSubscribeNow';
  text: Array<Scalars['String']>;
  title: Scalars['String'];
}

export interface SubscriptionPackageHeaderText {
   __typename?: 'SubscriptionPackageHeaderText';
  text: Array<Scalars['String']>;
}

export interface SubscriptionPackageHeadline {
   __typename?: 'SubscriptionPackageHeadline';
  promotion?: Maybe<SubscriptionPackagePageHeadline>;
  regular: SubscriptionPackagePageHeadline;
}

export interface SubscriptionPackageHeadlineText {
   __typename?: 'SubscriptionPackageHeadlineText';
  text: Array<Scalars['String']>;
}

export interface SubscriptionPackageName {
   __typename?: 'SubscriptionPackageName';
  long: Scalars['String'];
  short: Scalars['String'];
}

export interface SubscriptionPackagePageHeader {
   __typename?: 'SubscriptionPackagePageHeader';
  paywall: SubscriptionPackageHeaderText;
}

export interface SubscriptionPackagePageHeadline {
   __typename?: 'SubscriptionPackagePageHeadline';
  paywall: SubscriptionPackageHeadlineText;
  subscribe: SubscriptionPackageHeadlineText;
}

export interface SubscriptionPackagePrice {
   __typename?: 'SubscriptionPackagePrice';
  discount?: Maybe<SubscriptionPackageDiscountPrice>;
  id: Scalars['ID'];
  key: Scalars['String'];
  promotion?: Maybe<SubscriptionPackagePricePromotion>;
  regular: SubscriptionPackageRegularPrice;
}

export interface SubscriptionPackagePriceMessage {
   __typename?: 'SubscriptionPackagePriceMessage';
  text: Scalars['String'];
}

export interface SubscriptionPackagePricePromotion {
   __typename?: 'SubscriptionPackagePricePromotion';
  code: Scalars['String'];
  text: Array<Scalars['String']>;
}

export interface SubscriptionPackagePricing {
   __typename?: 'SubscriptionPackagePricing';
  monthly: SubscriptionPackagePrice;
}

export interface SubscriptionPackageRegularPrice {
   __typename?: 'SubscriptionPackageRegularPrice';
  messages: SubscriptionPackageRegularPriceMessages;
}

export interface SubscriptionPackageRegularPriceMessages {
   __typename?: 'SubscriptionPackageRegularPriceMessages';
  price: SubscriptionPackagePriceMessage;
}

export interface SubscriptionPackageResponse {
   __typename?: 'SubscriptionPackageResponse';
  Package: SubscriptionPackage;
}

export interface SubscriptionPackagesByChannelResponse {
   __typename?: 'SubscriptionPackagesByChannelResponse';
  error?: Maybe<SubscriptionPackageError>;
  packages: Array<ChannelSubscriptionPackage>;
}

export interface SubscriptionPackageTermsAndConditions {
   __typename?: 'SubscriptionPackageTermsAndConditions';
  text: Array<Scalars['String']>;
}

export interface SubscriptionPlanEntitlement {
   __typename?: 'SubscriptionPlanEntitlement';
  entitlements: Array<Scalars['String']>;
  plan: Scalars['String'];
}

export enum SubscriptionProvider {
  AppStore = 'APP_STORE',
  GooglePlay = 'GOOGLE_PLAY',
  SubscribeWithGoogle = 'SUBSCRIBE_WITH_GOOGLE'
}

export enum SubscriptionPurchaseSource {
  AppStore = 'APP_STORE',
  GoogleNews = 'GOOGLE_NEWS',
  GooglePlay = 'GOOGLE_PLAY',
  SubscribeWithGoogle = 'SUBSCRIBE_WITH_GOOGLE'
}

export interface SubscriptionReply {
   __typename?: 'SubscriptionReply';
  errors?: Maybe<Array<SubscriptionValidationError>>;
  id: Scalars['String'];
  status: Scalars['String'];
  subscriptionNumber?: Maybe<Scalars['String']>;
}

export interface SubscriptionValidationError {
   __typename?: 'SubscriptionValidationError';
  field: Scalars['String'];
  messages: Array<Scalars['String']>;
}

export interface SwgPrompt {
   __typename?: 'SWGPrompt';
  message?: Maybe<Scalars['String']>;
  package?: Maybe<Scalars['String']>;
  price?: Maybe<Scalars['String']>;
  subscriptionURL?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
}

export interface Tag {
   __typename?: 'Tag';
  ads?: Maybe<TagAds>;
  archived: Scalars['Boolean'];
  context: Scalars['String'];
  description: Scalars['String'];
  displayName: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
  seo?: Maybe<TagSeo>;
  shortID: Scalars['String'];
  slug?: Maybe<Scalars['String']>;
  themes?: Maybe<Array<Scalars['String']>>;
  urls?: Maybe<TagUrLs>;
  visible: Scalars['Boolean'];
  assetsConnection?: Maybe<AssetsConnection>;
}


export interface TagAssetsConnectionArgs {
  brand: Scalars['String'];
  count?: Maybe<Scalars['Int']>;
  offset?: Maybe<Scalars['Int']>;
  render?: Maybe<Render>;
  since?: Maybe<Scalars['ID']>;
  types?: Maybe<Array<AssetType>>;
}

export interface TagAds {
   __typename?: 'TagAds';
  overrides: Array<Scalars['String']>;
}

export interface TagContentQuery  extends MobileConfigMenuItemContentQuery {
   __typename?: 'TagContentQuery';
  assetsCount: Scalars['Int'];
  name: Scalars['String'];
  tagID: Scalars['String'];
}

export interface TagContext {
   __typename?: 'TagContext';
  description: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
}

export interface TagContextsReply {
   __typename?: 'TagContextsReply';
  TagContexts: Array<Maybe<TagContext>>;
}

export enum TagContextType {
  Advertising = 'ADVERTISING',
  Company = 'COMPANY',
  Content = 'CONTENT',
  Geolocation = 'GEOLOCATION',
  Navigation = 'NAVIGATION',
  Person = 'PERSON',
  Team = 'TEAM',
  Topic = 'TOPIC',
  Podcast = 'PODCAST'
}

export interface TagCreateInput {
  canonicalBrand?: Maybe<Scalars['String']>;
  contextID: Scalars['ID'];
  description?: Maybe<Scalars['String']>;
  displayName: Scalars['String'];
  externalEntities?: Maybe<TagExternalEntitiesInput>;
  name: Scalars['String'];
  SEODescription?: Maybe<Scalars['String']>;
  SEOTitle?: Maybe<Scalars['String']>;
  slug?: Maybe<Scalars['String']>;
  themeIDs?: Maybe<Array<Scalars['ID']>>;
  visible: Scalars['Boolean'];
}

export interface TagCreateReply {
   __typename?: 'TagCreateReply';
  tagID: Scalars['ID'];
}

export interface TagDetail {
   __typename?: 'TagDetail';
  archived: Scalars['Boolean'];
  canonicalBrand: Scalars['String'];
  context: TagContext;
  description: Scalars['String'];
  displayName: Scalars['String'];
  externalEntities?: Maybe<TagExternalEntities>;
  id: Scalars['ID'];
  name: Scalars['String'];
  seo?: Maybe<TagSeo>;
  shortID: Scalars['String'];
  slug?: Maybe<Scalars['String']>;
  themes?: Maybe<Array<TagTheme>>;
  urls?: Maybe<TagUrLs>;
  visible: Scalars['Boolean'];
}

export interface TagExternalEntities {
   __typename?: 'TagExternalEntities';
  wikiData?: Maybe<TagWikiData>;
}

export interface TagExternalEntitiesInput {
  wikiData?: Maybe<TagWikiDataInput>;
}

export interface TagPublishedUrl {
   __typename?: 'TagPublishedURL';
  path: Scalars['String'];
}

export interface TagPublishedUrLs {
   __typename?: 'TagPublishedURLs';
  afr?: Maybe<TagPublishedUrl>;
  brisbanetimes?: Maybe<TagPublishedUrl>;
  canberratimes?: Maybe<TagPublishedUrl>;
  smh?: Maybe<TagPublishedUrl>;
  theage?: Maybe<TagPublishedUrl>;
  watoday?: Maybe<TagPublishedUrl>;
}

export enum TagSearchField {
  DisplayName = 'displayName',
  Name = 'name',
  PartialText = 'partialText'
}

export interface TagSeo {
   __typename?: 'TagSEO';
  description?: Maybe<Scalars['String']>;
  keywords?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
}

export interface TagsReply {
   __typename?: 'TagsReply';
  Tags: Array<Maybe<Tag>>;
}

export interface TagTheme {
   __typename?: 'TagTheme';
  description: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
}

export interface TagThemesReply {
   __typename?: 'TagThemesReply';
  TagThemes: Array<Maybe<TagTheme>>;
}

export interface TagUpdateInput {
  archived: Scalars['Boolean'];
  canonicalBrand?: Maybe<Scalars['String']>;
  contextID: Scalars['ID'];
  description?: Maybe<Scalars['String']>;
  displayName: Scalars['String'];
  externalEntities?: Maybe<TagExternalEntitiesInput>;
  tagID: Scalars['ID'];
  name: Scalars['String'];
  SEODescription?: Maybe<Scalars['String']>;
  SEOTitle?: Maybe<Scalars['String']>;
  slug?: Maybe<Scalars['String']>;
  themeIDs?: Maybe<Array<Scalars['ID']>>;
  visible: Scalars['Boolean'];
}

export interface TagUpdateReply {
   __typename?: 'TagUpdateReply';
  tagID: Scalars['ID'];
}

export interface TagUrl {
   __typename?: 'TagURL';
  brand: Scalars['String'];
  path: Scalars['String'];
}

export interface TagUrLs {
   __typename?: 'TagURLs';
  canonical?: Maybe<TagUrl>;
  published?: Maybe<TagPublishedUrLs>;
}

export interface TagUsage {
   __typename?: 'TagUsage';
  tag: TagDetail;
  count: Scalars['Int'];
}

export interface TagWikiData {
   __typename?: 'TagWikiData';
  id?: Maybe<Scalars['String']>;
  url?: Maybe<Scalars['String']>;
}

export interface TagWikiDataInput {
  id: Scalars['String'];
  url?: Maybe<Scalars['String']>;
}

export interface Team {
   __typename?: 'Team';
  id: Scalars['ID'];
  name: Scalars['String'];
  displayName?: Maybe<Scalars['String']>;
  score: Scalars['String'];
  teamID: Scalars['Int'];
}


export interface TodaysPaperResponse {
   __typename?: 'TodaysPaperResponse';
  link?: Maybe<Scalars['String']>;
}

export interface TopBottomQuote {
   __typename?: 'TopBottomQuote';
  ASXCode: Scalars['String'];
  issuerName: IssuerName;
  lastTradePrice: Scalars['Float'];
  priceMovement: Scalars['Float'];
  priceMovementPercentage: Scalars['Float'];
  tradeValue: Scalars['Float'];
  tradeVolume: Scalars['Int'];
  lastUpdated: Scalars['String'];
}

export interface UpdateAccountInput {
  id?: Maybe<Scalars['ID']>;
  autoplay: Scalars['Boolean'];
  onboarding?: Maybe<UpdateAccountInputOnboarding>;
  postCode?: Maybe<Scalars['String']>;
  state?: Maybe<Scalars['String']>;
  suburb?: Maybe<Scalars['String']>;
}

export interface UpdateAccountInputOnboarding {
  newsfeed: Scalars['Boolean'];
  tags: Scalars['Boolean'];
}

export interface UpdateAccountResult {
   __typename?: 'UpdateAccountResult';
  autoplay: Scalars['Boolean'];
  error?: Maybe<AccountError>;
  id: Scalars['ID'];
  new: Scalars['Boolean'];
  location?: Maybe<AccountLocation>;
  onboarding?: Maybe<AccountOnboarding>;
}

export interface UpdateMemberNameInput {
  firstName: Scalars['String'];
  lastName: Scalars['String'];
  siteKey: Scalars['String'];
}

export interface UpdateMemberNameResponse {
   __typename?: 'UpdateMemberNameResponse';
  success: Scalars['Boolean'];
}

export interface UpdateRunsheetLockError  extends Error {
   __typename?: 'UpdateRunsheetLockError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface UpdateRunsheetLockReply {
   __typename?: 'UpdateRunsheetLockReply';
  error?: Maybe<UpdateRunsheetLockError>;
  runsheetID: Scalars['ID'];
  success: Scalars['Boolean'];
  userID: Scalars['String'];
}

export interface UpgradeActionCallToAction {
   __typename?: 'UpgradeActionCallToAction';
  link?: Maybe<Scalars['String']>;
  text: Scalars['String'];
}

export interface UpgradeActionCallToActions {
   __typename?: 'UpgradeActionCallToActions';
  cancel?: Maybe<UpgradeActionCallToAction>;
  upgrade: UpgradeActionCallToAction;
}

export interface UpgradeActionMessage {
   __typename?: 'UpgradeActionMessage';
  text: Array<Scalars['String']>;
}

export interface UserReadingHistoryAsset {
   __typename?: 'UserReadingHistoryAsset';
  asset?: Maybe<Asset>;
  date?: Maybe<Scalars['String']>;
}

export interface UserReadingHistoryEdge {
   __typename?: 'UserReadingHistoryEdge';
  cursor: Scalars['ID'];
  node: UserReadingHistoryAsset;
}

export interface UserReadingHistoryError  extends Error {
   __typename?: 'UserReadingHistoryError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface UserReadingHistoryResponse {
   __typename?: 'UserReadingHistoryResponse';
  edges?: Maybe<Array<UserReadingHistoryEdge>>;
  error?: Maybe<UserReadingHistoryError>;
  pageInfo: PageInfo;
}

export interface UserTag {
   __typename?: 'UserTag';
  displayName: Scalars['String'];
  id: Scalars['ID'];
  name: Scalars['String'];
  status: Scalars['String'];
  recommendedTime?: Maybe<Scalars['Time']>;
}

export interface UserTagEdge {
   __typename?: 'UserTagEdge';
  cursor: Scalars['ID'];
  node: UserTag;
}

export interface UserTagsByStatusConnectionError {
   __typename?: 'UserTagsByStatusConnectionError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface UserTagsByStatusConnectionResponse {
   __typename?: 'UserTagsByStatusConnectionResponse';
  edges?: Maybe<Array<UserTagEdge>>;
  error?: Maybe<UserTagsByStatusConnectionError>;
  pageInfo: PageInfo;
}

export interface UserTagsByTagIdsError {
   __typename?: 'UserTagsByTagIdsError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface UserTagsByTagIdsResponse {
   __typename?: 'UserTagsByTagIdsResponse';
  error?: Maybe<UserTagsByTagIdsError>;
  userTags?: Maybe<Array<UserTag>>;
}

export interface UserTagsStatusUpdateError {
   __typename?: 'UserTagsStatusUpdateError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface UserTagsStatusUpdateInput {
  brand: Brand;
  status: UserTagStatus;
  tagIDs: Array<Scalars['ID']>;
}

export interface UserTagsStatusUpdateResponse {
   __typename?: 'UserTagsStatusUpdateResponse';
  error?: Maybe<UserTagsStatusUpdateError>;
  success: Scalars['Boolean'];
}

export enum UserTagStatus {
  Followed = 'FOLLOWED',
  Suppressed = 'SUPPRESSED',
  Unfollowed = 'UNFOLLOWED'
}

export interface Variations {
   __typename?: 'Variations';
  about?: Maybe<Scalars['String']>;
  headline?: Maybe<Scalars['String']>;
  label?: Maybe<Scalars['String']>;
  featuredImages?: Maybe<AssetImages>;
}

export interface VerifyAppleStoreReceiptError  extends Error {
   __typename?: 'VerifyAppleStoreReceiptError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface VerifyAppleStoreReceiptInput {
  environment: AppleStoreEnvironment;
  receipt: Scalars['String'];
}

export interface VerifyAppleStoreReceiptResponse {
   __typename?: 'VerifyAppleStoreReceiptResponse';
  error?: Maybe<VerifyAppleStoreReceiptError>;
  receipt?: Maybe<AppleStoreReceipt>;
}

export interface VerifyGoogleEntitlementError  extends Error {
   __typename?: 'VerifyGoogleEntitlementError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface VerifyGoogleEntitlementResponse {
   __typename?: 'VerifyGoogleEntitlementResponse';
  status?: Maybe<MemberPurchaseLinkageStatus>;
  error?: Maybe<VerifyGoogleEntitlementError>;
}

export interface VerifyGooglePlayReceiptError  extends Error {
   __typename?: 'VerifyGooglePlayReceiptError';
  message: Scalars['String'];
  type: ErrorType;
}

export interface VerifyGooglePlayReceiptInput {
  packageName: Scalars['String'];
  productID: Scalars['String'];
  token: Scalars['String'];
}

export interface VerifyGooglePlayReceiptResponse {
   __typename?: 'VerifyGooglePlayReceiptResponse';
  error?: Maybe<VerifyGooglePlayReceiptError>;
  receipt?: Maybe<GooglePlayReceipt>;
}

export interface VideoUploadUrlInput {
  fileName: Scalars['String'];
}

export interface VideoUploadUrlResponse {
   __typename?: 'VideoUploadURLResponse';
  preSignedURL: Scalars['String'];
}

export enum VisibilityExcludeType {
  Invisible = 'INVISIBLE',
  Visible = 'VISIBLE'
}

export interface WidgetConfiguration {
   __typename?: 'WidgetConfiguration';
  type?: Maybe<Scalars['String']>;
  data?: Maybe<WidgetData>;
}

export type WidgetData = ScoreboardData | CricketScorecardData;

