// TODO: Add tests
/* istanbul ignore file */
import 'abortcontroller-polyfill/dist/polyfill-patch-fetch'
// import { ApolloClient } from 'apollo-client'
// import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory'
// import { HttpLink } from 'apollo-link-http'
// import { onError } from 'apollo-link-error'
// import { ApolloLink, NextLink, Observable, Operation } from 'apollo-link'
import { ApolloClient, createHttpLink, InMemoryCache, split } from '@apollo/client'
import { setContext } from '@apollo/client/link/context'
import { getMainDefinition } from '@apollo/client/utilities'
import { WebSocketLink } from '@apollo/client/link/ws'
import Cookies from 'js-cookie'
import { log } from '@ffxblue/library-js-logging/build/shared'
import { persistCache } from 'apollo3-cache-persist'
import environment from 'config/environment/environment'
import { Logger } from 'config/logger/logger'
import introspectionQueryResultData from './introspectionQueryResultData'

const {
  FRONT_API: { TIMEOUT: FRONT_API_TIMEOUT, URL: FRONT_API_URL },
} = environment
// let controller: AbortController
// let controllerAbortTimeout: ReturnType<typeof setTimeout>

// const request = (operation: Operation): void => {
//   controller = new AbortController()
//   controllerAbortTimeout = setTimeout(() => {
//     controller.abort()
//   }, FRONT_API_TIMEOUT)

//   operation.setContext({
//     fetchOptions: {
//       signal: controller.signal,
//     },
//   })
// }

// const requestLink = new ApolloLink(
//   (operation: Operation, forward: NextLink): Observable<{}> =>
//     new Observable((observer): (() => void) => {
//       // TODO: Find correct 'Subscription' type in Apollo libs
//       // eslint-disable-next-line @typescript-eslint/no-explicit-any
//       let handle: any

//       Promise.resolve(operation)
//         .then((oper) => request(oper))
//         .then(() => {
//           handle = forward(operation).subscribe({
//             next: observer.next.bind(observer),
//             error: observer.error.bind(observer),
//             complete: observer.complete.bind(observer),
//           })
//         })
//         .catch(observer.error.bind(observer))

//       return (): void => {
//         clearTimeout(controllerAbortTimeout)

//         if (handle) {
//           handle.unsubscribe()
//         }
//       }
//     })
// )

// const client = new ApolloClient({
//   cache: new InMemoryCache({
//     fragmentMatcher: new IntrospectionFragmentMatcher({ introspectionQueryResultData }),
//   }),
//   link: ApolloLink.from([
//     onError(({ graphQLErrors, networkError }) => {
//       if (graphQLErrors) {
//         graphQLErrors.forEach((error) =>
//           Logger.error('GraphQL request failed', log.EventType('front', 'error'), log.Error(error))
//         )
//       }

//       if (networkError) {
//         Logger.error('Network request failed', log.EventType('front', 'error'), log.Error(networkError))
//       }
//     }),
//     requestLink,
//     new HttpLink({
//       uri: FRONT_API_URL,
//       credentials: 'omit',
//     }),
//   ]),
// })

const getToken = () => Cookies.get('ffx:access_token')

const httpLink = createHttpLink({
  uri: 'https://api-front-internal-development-v1.ffxblue.com.au/graphql',
})

const wsLink = new WebSocketLink({
  options: {
    lazy: false,
    reconnect: true,
  },
  uri: `wss://api-front-internal-development-v1.ffxblue.com.au/graphql?access_token=${getToken()}`,
})

const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  const token = getToken()
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : '',
    },
  }
})

const splitLink = split(
  ({ query }) => {
    const definition = getMainDefinition(query)
    return definition.kind === 'OperationDefinition' && definition.operation === 'subscription'
  },
  wsLink,
  authLink.concat(httpLink)
)

const cache = new InMemoryCache({
  typePolicies: {
    AssetLockNotification: {
      keyFields: false,
    },
    EditorialAssetPreview: {
      keyFields: ['shortId'],
    },
    ImageRendition: {
      keyFields: ['mediaId', 'type'],
    },
  },
})

// await persistCache({
//   cache,
//   storage: new LocalStorageWrapper(window.localStorage),
// })

const client = new ApolloClient({
  link: splitLink,
  cache,
})

export default client
