
      export interface IntrospectionResultData {
        __schema: {
          types: {
            kind: string;
            name: string;
            possibleTypes: {
              name: string;
            }[];
          }[];
        };
      }
      const result: IntrospectionResultData = {
  "__schema": {
    "types": [
      {
        "kind": "INTERFACE",
        "name": "Error",
        "possibleTypes": [
          {
            "name": "AccountCorporateError"
          },
          {
            "name": "AccountCorporateSearchError"
          },
          {
            "name": "AssetsConnectionError"
          },
          {
            "name": "AudienceSegmentsError"
          },
          {
            "name": "ClippingLastUpdatedError"
          },
          {
            "name": "ClippingListError"
          },
          {
            "name": "CrossWordsRecentError"
          },
          {
            "name": "CTAError"
          },
          {
            "name": "EntitlementsByPlansError"
          },
          {
            "name": "FileUploadURLError"
          },
          {
            "name": "ListingSearchEditorialError"
          },
          {
            "name": "MemberCorporateDetailError"
          },
          {
            "name": "MemberCorporateImportStatusError"
          },
          {
            "name": "MemberCorporateSearchError"
          },
          {
            "name": "MemberCorporateStatusError"
          },
          {
            "name": "MemberDetailsError"
          },
          {
            "name": "MemberExternalSubscriptionProvidersError"
          },
          {
            "name": "MemberSubscriptionDetailsError"
          },
          {
            "name": "MemberRecommendedTagsError"
          },
          {
            "name": "MemberTagRecommendationPreferencesError"
          },
          {
            "name": "MobileAppConfigError"
          },
          {
            "name": "MobileAppClientPaywallError"
          },
          {
            "name": "NewsroomBrandsError"
          },
          {
            "name": "NewsroomCategoriesError"
          },
          {
            "name": "NewsroomsError"
          },
          {
            "name": "NotificationsError"
          },
          {
            "name": "PageContentError"
          },
          {
            "name": "PaywallRuleError"
          },
          {
            "name": "PropertyByMetroError"
          },
          {
            "name": "PuzzlesError"
          },
          {
            "name": "PuzzlesLoadProgressError"
          },
          {
            "name": "RunsheetSearchError"
          },
          {
            "name": "StreetAddressSearchError"
          },
          {
            "name": "StreetAddressError"
          },
          {
            "name": "SubscriptionPackageError"
          },
          {
            "name": "UserReadingHistoryError"
          },
          {
            "name": "VerifyGoogleEntitlementError"
          },
          {
            "name": "AccountCorporateCreateError"
          },
          {
            "name": "AccountCorporateUpdateError"
          },
          {
            "name": "MeteringError"
          },
          {
            "name": "EntitlementTokenError"
          },
          {
            "name": "CreateMemberFromSocialError"
          },
          {
            "name": "CreateRunsheetError"
          },
          {
            "name": "DeleteRunsheetError"
          },
          {
            "name": "IngestVideoError"
          },
          {
            "name": "AppleStoreLinkingError"
          },
          {
            "name": "MemberCorporateBulkInviteError"
          },
          {
            "name": "MemberCorporateCreateError"
          },
          {
            "name": "MemberCorporateUpdateError"
          },
          {
            "name": "MemberCorporateInviteError"
          },
          {
            "name": "MemberTagRecommendationAddError"
          },
          {
            "name": "MemberTagRecommendationPreferencesUpdateError"
          },
          {
            "name": "GooglePlayLinkingError"
          },
          {
            "name": "PuzzleProgressError"
          },
          {
            "name": "UpdateRunsheetLockError"
          },
          {
            "name": "VerifyAppleStoreReceiptError"
          },
          {
            "name": "VerifyGooglePlayReceiptError"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "ErrorType",
        "possibleTypes": [
          {
            "name": "ErrorTypeForbidden"
          },
          {
            "name": "ErrorTypeInvalidAuth"
          },
          {
            "name": "ErrorTypeInvalidRequest"
          },
          {
            "name": "ErrorTypeNotFound"
          },
          {
            "name": "ErrorTypeUnavailable"
          }
        ]
      },
      {
        "kind": "UNION",
        "name": "WidgetData",
        "possibleTypes": [
          {
            "name": "ScoreboardData"
          },
          {
            "name": "CricketScorecardData"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "FinancialInstrument",
        "possibleTypes": [
          {
            "name": "FinancialCommodityFuture"
          },
          {
            "name": "FinancialCommoditySpot"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "MobileConfigMenuItemContentQuery",
        "possibleTypes": [
          {
            "name": "CategoryContentQuery"
          },
          {
            "name": "ExternalContentQuery"
          },
          {
            "name": "RestContentQuery"
          },
          {
            "name": "TagContentQuery"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "SubscriptionPackageCommon",
        "possibleTypes": [
          {
            "name": "SubscriptionPackage"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "FinancialCommodity",
        "possibleTypes": [
          {
            "name": "FinancialCommodityFuture"
          },
          {
            "name": "FinancialCommoditySpot"
          }
        ]
      },
      {
        "kind": "INTERFACE",
        "name": "FinancialFuture",
        "possibleTypes": [
          {
            "name": "FinancialCommodityFuture"
          }
        ]
      }
    ]
  }
};
      export default result;
    