import React, { ComponentType } from 'react'
import { FeatureFlags } from 'config/featureflags/featureflags.default'

interface Props {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [Prop: string]: any
  featureFlags?: FeatureFlags
}

declare global {
  interface Window {
    INITIAL_STATE: {
      featureFlags?: FeatureFlags
    }
  }
}

const featureFlags = window.INITIAL_STATE && window.INITIAL_STATE.featureFlags

function withFeatureFlags(Component: ComponentType<Props>) {
  return function WrapperComponent(props: Props): JSX.Element {
    return <Component {...props} featureFlags={featureFlags} />
  }
}

export default withFeatureFlags
