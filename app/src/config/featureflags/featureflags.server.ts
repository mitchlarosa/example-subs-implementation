import os from 'os'
import { initialize, Unleash } from 'unleash-client'
import { log } from '@ffxblue/library-js-logging/build/shared'
import environment, { Environment } from 'config/environment/environment'
import { Logger } from 'config/logger/logger'
import { FeatureFlags } from 'config/featureflags/featureflags.default'

const {
  APP_NAME,
  FEATURE_FLAGS_API: { REFRESH_INTERVAL, URL },
}: Environment = environment
let isFeatureFlagsRegistered = false // eslint-disable-line import/no-mutable-exports

function initFeatureFlags(): Unleash {
  const unleash = initialize({
    url: `${URL}/api`,
    appName: APP_NAME,
    instanceId: os.hostname(),
    refreshInterval: REFRESH_INTERVAL,
  })

  unleash.on('registered', (payload) => {
    Logger.info(
      'Feature flag client registered with service',
      log.EventType('featureFlag', 'serverEvent'),
      log.String('registration_payload', payload)
    )

    isFeatureFlagsRegistered = true
  })

  unleash.on('ready', () => {
    Logger.info('Feature flag client ready', log.EventType('featureFlag', 'serverEvent'))
  })

  unleash.on('warn', (msg) => {
    Logger.warn('Feature flag warn', log.EventType('featureFlag', 'serverEvent'), log.String('warn_msg', msg))
  })

  unleash.on('error', (err) => {
    Logger.error('Feature flag error', log.EventType('featureFlag', 'serverEvent'), log.Error(err))
  })

  return unleash
}

function getFeatureFlags(
  flags: FeatureFlags,
  cb: (flag: string) => string | boolean,
  overrides: FeatureFlags
): FeatureFlags {
  return Object.keys(flags).reduce((obj: FeatureFlags, flag: string) => {
    obj[flag] = cb(flag)

    if (overrides && overrides[flag]) {
      obj[flag] = overrides[flag] === 'true'
    }

    return obj
  }, {})
}

export { getFeatureFlags, isFeatureFlagsRegistered }
export default initFeatureFlags
