let FEATURE_FLAGS = {} // eslint-disable-line import/no-mutable-exports

interface FeatureFlags {
  [key: string]: string | boolean
}

function setFeatureFlags(flags: Record<string, boolean>): void {
  FEATURE_FLAGS = { ...FEATURE_FLAGS, ...flags }
}

export { FEATURE_FLAGS, FeatureFlags, setFeatureFlags }
