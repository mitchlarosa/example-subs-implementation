import { createLogger, createLoggerMiddleware } from '@ffxblue/library-js-logging/build/server'
import { logLevelFromName } from '@ffxblue/library-js-logging/build/shared'
import environment from 'config/environment/environment'

const {
  APP_NAME,
  APP_VERSION,
  DEPLOY_ENV,
  HOSTNAME,
  LOGGING: { MINIMUM_LOG_LEVEL },
} = environment
const loggerParams = {
  env: DEPLOY_ENV,
  host: HOSTNAME,
  source: APP_NAME,
  version: APP_VERSION,
  minLogLevel: logLevelFromName(MINIMUM_LOG_LEVEL),
}
const Logger = createLogger(loggerParams)
const loggerMiddleware = createLoggerMiddleware(loggerParams)

export { Logger, loggerMiddleware }
