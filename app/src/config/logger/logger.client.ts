import { createLogger } from '@ffxblue/library-js-logging/build/client'
import { logLevelFromName } from '@ffxblue/library-js-logging/build/shared'
import environment from 'config/environment/environment'

const {
  APP_NAME,
  APP_VERSION,
  DEPLOY_ENV,
  LOGGING: { MINIMUM_LOG_LEVEL, URL },
} = environment
const Logger = createLogger({
  env: DEPLOY_ENV,
  host: window.location.host,
  source: APP_NAME,
  userAgent: window.navigator.userAgent,
  version: APP_VERSION,
  minLogLevel: logLevelFromName(MINIMUM_LOG_LEVEL),
  postUrl: URL,
})

export { Logger }
