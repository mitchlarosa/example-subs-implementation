// Get server environment variables
// DO NOT set environment variables in this module - use variables.common.ts and/or variables.server.ts
import { EnvironmentVariablesServer as Environment } from './types'
import environment from './variables.server'

export { Environment }
export default environment
