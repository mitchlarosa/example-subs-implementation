// Set client environment variables
// Inherits from variables.common.ts
// DO NOT set application secrets in this module
// DO NOT import in components - use "import environment from 'config/environment/environment'"
import envalid from 'envalid'
import { EnvironmentVariablesClient, ProcessEnv } from './types'
import environmentVariablesCommon, { reporter } from './variables.common'

const { cleanEnv, url } = envalid
const env = cleanEnv(
  process.env,
  {
    FRONT_API_URL_EXTERNAL: url({ desc: 'GraphQL endpoint for api-front' }),
    LOGGING_URL: url({ desc: 'API endpoint for logging' }),
  },
  {
    reporter,
  }
)

const { FRONT_API_URL_EXTERNAL, LOGGING_URL }: ProcessEnv = env
const environmentVariablesClient: EnvironmentVariablesClient = {
  ...environmentVariablesCommon,
  FRONT_API: {
    ...environmentVariablesCommon.FRONT_API,
    URL: FRONT_API_URL_EXTERNAL,
  },
  LOGGING: {
    ...environmentVariablesCommon.LOGGING,
    URL: LOGGING_URL,
  },
}

export { EnvironmentVariablesClient }
export default environmentVariablesClient
