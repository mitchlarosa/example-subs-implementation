// Set common environment variables for client and server
// DO NOT set application secrets in this module
// DO NOT import in components - use "import environment from 'config/environment/environment'"
import os from 'os'
import envalid from 'envalid'
import { createLogger } from '@ffxblue/library-js-logging/build/server'
import { log, logLevelFromName } from '@ffxblue/library-js-logging/build/shared'
import { name as APP_NAME, version as APP_VERSION } from '../../../../package.json'
import { EnvironmentVariablesCommon, ProcessEnv } from './types'

interface ReporterOptions {
  env: unknown
  errors: {
    [key: string]: Error
  }
}

const { DEPLOY_ENV, LOGGING_MINIMUM_LOG_LEVEL } = process.env
const HOSTNAME = os.hostname()

// Create a custom logger for environment
const Logger = createLogger({
  env: DEPLOY_ENV,
  host: HOSTNAME,
  minLogLevel: logLevelFromName(LOGGING_MINIMUM_LOG_LEVEL),
  source: APP_NAME,
  version: APP_VERSION,
})

const reporter = ({ errors }: ReporterOptions): void => {
  const errorKeys = Object.keys(errors)
  if (!errorKeys.length) {
    return
  }

  errorKeys.forEach((key): void => {
    const error = errors[key]
    Logger.error(
      `${error.name} exception for environment variable ${key}: ${error.message}`,
      log.EventType('env', 'error')
    )
  })

  process.exit(1)
}

const { cleanEnv, num, str } = envalid
const env = cleanEnv(
  process.env,
  {
    DEPLOY_ENV: str({
      choices: ['development', 'local', 'pr', 'production', 'staging', 'test'],
      desc: 'Deployment environment',
    }),
    FRONT_API_TIMEOUT: num({ default: 5000, desc: 'GraphQL request timeout for api-front' }),
    LOGGING_MINIMUM_LOG_LEVEL: str({
      choices: ['debug', 'error', 'info', 'warn'],
      desc: 'Minimum required logging level',
    }),
    NODE_ENV: str({
      choices: DEPLOY_ENV === 'local' ? ['development', 'test', 'production'] : ['production'],
      desc: 'Node environment',
    }),
  },
  {
    reporter,
  }
)

const { FRONT_API_TIMEOUT, NODE_ENV }: ProcessEnv = env
const environmentVariablesCommon: EnvironmentVariablesCommon = {
  APP_NAME,
  APP_VERSION,
  DEPLOY_ENV,
  FRONT_API: {
    TIMEOUT: parseInt(FRONT_API_TIMEOUT, 10),
  },
  HOSTNAME,
  LOGGING: {
    MINIMUM_LOG_LEVEL: LOGGING_MINIMUM_LOG_LEVEL,
  },
  NODE_ENV,
}

export { EnvironmentVariablesCommon, Logger, reporter }
export default environmentVariablesCommon
