interface ProcessEnv {
  DEPLOY_ENV?: string
  FEATURE_FLAGS_API_REFRESH_INTERVAL?: string
  FEATURE_FLAGS_API_URL?: string
  FRONT_API_TIMEOUT?: string
  FRONT_API_URL?: string
  FRONT_API_URL_EXTERNAL?: string
  HTTP_PORT?: string
  HTTP_PORT_INTERNAL?: string
  HTTP_SERVER_CLOSE_TIMEOUT?: string
  LOGGING_MINIMUM_LOG_LEVEL?: string
  LOGGING_URL?: string
  NODE_ENV?: string
  TRACING_HOST?: string
  TRACING_PORT?: string
  TRACING_SAMPLING_RATE?: string
}

interface EnvironmentVariablesCommon {
  APP_NAME: string
  APP_VERSION: string
  DEPLOY_ENV?: string
  FRONT_API: {
    TIMEOUT: number
  }
  HOSTNAME: string
  LOGGING: {
    MINIMUM_LOG_LEVEL?: string
    URL?: string
  }
  NODE_ENV?: string
}

type EnvironmentVariablesClient = EnvironmentVariablesCommon & {
  FRONT_API: {
    URL?: string
  }
}

type EnvironmentVariablesServer = EnvironmentVariablesCommon & {
  FEATURE_FLAGS_API: {
    REFRESH_INTERVAL: number
    URL?: string
  }
  FRONT_API: {
    URL?: string
  }
  HTTP_PORT: number
  HTTP_PORT_INTERNAL: number
  HTTP_SERVER_CLOSE_TIMEOUT: number
  TRACING: {
    HOST: string
    PORT: number
    SAMPLING_RATE: number
    SERVICE_NAME: string
  }
}

export { EnvironmentVariablesClient, EnvironmentVariablesCommon, EnvironmentVariablesServer, ProcessEnv }
