// Set server environment variables
// Inherits from variables.common.ts
// Set application secrets in this module (if required)
// DO NOT import in components - use "import environment from 'config/environment/environment'"
import { cleanEnv, num, port, str, url } from 'envalid'
import { EnvironmentVariablesServer, ProcessEnv } from './types'
import environmentVariablesCommon, { reporter } from './variables.common'

const env = cleanEnv(
  process.env,
  {
    FEATURE_FLAGS_API_REFRESH_INTERVAL: num({ default: 10000, desc: 'Refresh interval for service-feature-flags' }),
    FEATURE_FLAGS_API_URL: url({ desc: 'API endpoint for service-feature-flags' }),
    FRONT_API_URL: url({ desc: 'GraphQL endpoint for api-front' }),
    HTTP_PORT: port({ default: 80, desc: 'Express HTTP port' }),
    HTTP_PORT_INTERNAL: port({ default: 8088, desc: 'Express internal HTTP port' }),
    HTTP_SERVER_CLOSE_TIMEOUT: num({
      default: 15000,
      desc: 'Express HTTP server close timeout when a shutdown signal is received',
    }),
    TRACING_HOST: str({ desc: 'Tracing hostname' }),
    TRACING_PORT: port({ default: 6832, desc: 'Tracing HTTP port' }),
    TRACING_SAMPLING_RATE: num({ default: 0.5, desc: 'Tracing sampling rate' }),
  },
  {
    reporter,
  }
)

const {
  DEPLOY_ENV,
  FEATURE_FLAGS_API_REFRESH_INTERVAL,
  FEATURE_FLAGS_API_URL,
  FRONT_API_URL,
  HTTP_PORT,
  HTTP_PORT_INTERNAL,
  HTTP_SERVER_CLOSE_TIMEOUT,
  TRACING_HOST,
  TRACING_PORT,
  TRACING_SAMPLING_RATE,
}: ProcessEnv = env

const environmentVariablesServer: EnvironmentVariablesServer = {
  ...environmentVariablesCommon,
  FEATURE_FLAGS_API: {
    REFRESH_INTERVAL: parseInt(FEATURE_FLAGS_API_REFRESH_INTERVAL, 10),
    URL: FEATURE_FLAGS_API_URL,
  },
  FRONT_API: {
    ...environmentVariablesCommon.FRONT_API,
    URL: FRONT_API_URL,
  },
  HTTP_PORT: parseInt(HTTP_PORT, 10),
  HTTP_PORT_INTERNAL: parseInt(HTTP_PORT_INTERNAL, 10),
  HTTP_SERVER_CLOSE_TIMEOUT: parseInt(HTTP_SERVER_CLOSE_TIMEOUT, 10),
  TRACING: {
    HOST: TRACING_HOST,
    PORT: parseInt(TRACING_PORT, 10),
    SAMPLING_RATE: parseInt(TRACING_SAMPLING_RATE, 10),
    SERVICE_NAME: `skeleton-nodejs-application-${DEPLOY_ENV}`,
  },
}

export { EnvironmentVariablesServer }
export default environmentVariablesServer
