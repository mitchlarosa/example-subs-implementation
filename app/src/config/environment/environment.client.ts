// Get client environment variables
// DO NOT set environment variables in this module - use variables.common.ts and/or variables.client.ts
import { EnvironmentVariablesClient as Environment } from './types'

declare global {
  interface Window {
    ENVIRONMENT_VARIABLES: object
  }
}

const environment = window.ENVIRONMENT_VARIABLES

delete window.ENVIRONMENT_VARIABLES

export { Environment }
export default environment
