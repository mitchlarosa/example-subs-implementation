import { EnvError, EnvMissingError } from 'envalid'
import { reporter } from './variables.common'

describe('(Function) reporter', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let mockConsoleLog: jest.SpyInstance<void, [any?, ...any[]]>
  let mockProcessExit: jest.SpyInstance<void>

  beforeEach(() => {
    mockConsoleLog = jest.spyOn(console, 'log').mockImplementation(() => {})
    mockProcessExit = jest.spyOn(process, 'exit').mockReturnThis()
  })

  afterEach(() => {
    jest.restoreAllMocks()
  })

  it('should return if no errors exist', () => {
    expect(
      reporter({
        env: null,
        errors: {},
      })
    ).toBeUndefined()
  })

  it('should log and exit the application if errors exist', () => {
    const errMessage = new EnvError('Value "development" not in choices [production]')

    reporter({
      env: null,
      errors: { NODE_ENV: errMessage },
    })

    const { eventLevel, eventType, msg } = JSON.parse(mockConsoleLog.mock.calls[0][0])

    expect(mockConsoleLog).toHaveBeenCalled()
    expect(eventLevel).toBe('error')
    expect(eventType).toBe('env:error')
    expect(msg).toBe(`EnvError exception for environment variable NODE_ENV: ${errMessage.message}`)
    expect(mockProcessExit).toHaveBeenCalled()
    expect(mockProcessExit).toHaveBeenNthCalledWith(1, 1)
  })

  it('should log and exit the application if required config does not exist', () => {
    const errMessage = new EnvMissingError('API endpoint for service-feature-flags')

    reporter({
      env: null,
      errors: { FEATURE_FLAGS_API_URL: errMessage },
    })

    const { eventLevel, eventType, msg } = JSON.parse(mockConsoleLog.mock.calls[0][0])

    expect(mockConsoleLog).toHaveBeenCalled()
    expect(eventLevel).toBe('error')
    expect(eventType).toBe('env:error')
    expect(msg).toBe(`EnvMissingError exception for environment variable FEATURE_FLAGS_API_URL: ${errMessage.message}`)
    expect(mockProcessExit).toHaveBeenCalled()
    expect(mockProcessExit).toHaveBeenNthCalledWith(1, 1)
  })
})
