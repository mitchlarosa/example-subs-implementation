import React, { useEffect, FunctionComponent } from 'react'
import { useSubscription, useQuery } from '@apollo/client'
import gql from 'graphql-tag'
import client from 'config/graphql/graphql'
import editorialAssetSearch, { EditorialAssetPreviewFragment, variables } from 'config/queries/editorialAssetSearch'

const EDITORIAL_SUBSCRIPTION = gql`
  subscription EditorialNotification {
    editorialNotification {
      error {
        message
      }
      notification {
        operation
        record {
          ... on AssetLockNotification {
            lockedBy
            id
          }
        }
        type
      }
      reconnect
    }
  }
`

const SubscriptionWrapper: FunctionComponent = ({ children }) => {
  const { data } = useSubscription(EDITORIAL_SUBSCRIPTION, {
    onSubscriptionData: (data2) => {
      console.log(data2)
      if (data2.subscriptionData.data.editorialNotification.notification.type === 'ASSET_LOCK') {
        console.log(data2.subscriptionData.data.editorialNotification.notification.record[0].lockedBy)
        client.writeFragment({
          // CUSTOM ID FROM NOTIFICATION GOES HERE
          id: `EditorialAssetPreview:{"shortId":"${atob(
            data2.subscriptionData.data.editorialNotification.notification.record[0].id
          )
            .split(':')[1]
            .slice(1, -1)}"}`,
          fragment: EditorialAssetPreviewFragment,
          data: {
            listing: {
              lockedBy: data2.subscriptionData.data.editorialNotification.notification.record[0].lockedBy,
            },
          },
        })
      }
    },
    variables: {},
  })

  const { refetch } = useQuery(editorialAssetSearch, {
    variables: {
      input: {
        ...variables.input,
      },
    },
  })

  const test = () => {
    client.writeFragment({
      // CUSTOM ID FROM NOTIFICATION GOES HERE
      id: 'EditorialAssetPreview:{"shortId":"p5dtq6"}',
      fragment: EditorialAssetPreviewFragment,
      data: {
        headlines: {
          headline: `tremendous - ${Math.floor(Math.random() * Math.floor(9000))}`,
        },
      },
    })
  }

  return (
    <>
      <button onClick={test} type="button">
        TEST UPDATE CACHE
      </button>
      <button onClick={() => refetch()} type="button">
        REFETCH QUERY
      </button>
      {children}
    </>
  )
}

export default SubscriptionWrapper
