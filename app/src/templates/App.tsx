import React, { FunctionComponent } from 'react'
import { BrowserRouter } from 'react-router-dom'
import { ApolloProvider } from '@apollo/client'
import styled from 'styled-components'
import { withErrorBoundary } from 'components/ErrorBoundary/withErrorBoundary'
import NavigationMenu, { items } from 'components/NavigationMenu/NavigationMenu'
import client from 'config/graphql/graphql'
import Routes, { routes } from 'routes/index'
import SubscriptionWrapper from './SubscriptionWrapper'

const Root = styled.div`
  font-weight: bold;
`
const Heading1 = styled.h1`
  font-weight: bold;
`

const App: FunctionComponent = () => (
  <Root>
    <Heading1>App</Heading1>
    <ApolloProvider client={client}>
      <BrowserRouter>
        <NavigationMenu items={items} />
        <SubscriptionWrapper>
          <Routes routes={routes} />
        </SubscriptionWrapper>
      </BrowserRouter>
    </ApolloProvider>
  </Root>
)

export { App, Heading1, Root }
export default withErrorBoundary(App)
