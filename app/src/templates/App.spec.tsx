import React from 'react'
import { ShallowWrapper, shallow } from 'enzyme'
import NavigationMenu from 'components/NavigationMenu/NavigationMenu'
import Routes from 'routes/index'
import { App, Heading1, Root } from './App'

describe('(Component) App', () => {
  let wrapper: ShallowWrapper

  beforeAll(() => {
    wrapper = shallow(<App />)
  })

  it('to render the app', () => {
    expect(wrapper.find(Root)).toHaveLength(1)
  })

  it('to render the h1', () => {
    expect(wrapper.find(Heading1)).toMatchInlineSnapshot(`
<App__Heading1>
  App
</App__Heading1>
`)
  })

  it('to render the navigation menu', () => {
    expect(wrapper.find(NavigationMenu)).toMatchInlineSnapshot(`
<NavigationMenu
  items={Array []}
/>
`)
  })

  it('to render the routes', () => {
    expect(wrapper.find(Routes)).toMatchInlineSnapshot(`
<Routes
  routes={Array []}
/>
`)
  })
})
