import React, { FunctionComponent } from 'react'
import serialize from 'serialize-javascript'
import { Environment } from 'config/environment/environment.client'
import { FeatureFlags } from 'config/featureflags/featureflags.default'
import 'components/Home/config'
import 'components/PageNotFound/config'
import 'components/Test/config'

interface Props {
  assets: {
    client: {
      js: string
    }
    vendor: {
      js: string
    }
  }
  environment: Environment
  initialState: {
    featureFlags?: FeatureFlags
  }
}

const RENDER_NODE = 'app'

const Html: FunctionComponent<Props> = ({ assets, environment, initialState }) => (
  <html lang="en">
    <head>
      <title>skeleton-nodejs-application</title>
    </head>
    <body>
      <div id={RENDER_NODE} />
      <script
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{
          __html: `window.ENVIRONMENT_VARIABLES = ${serialize(environment)};`,
        }}
      />
      <script
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{
          __html: `window.INITIAL_STATE = ${serialize(initialState)};`,
        }}
      />
      <script src={assets.vendor.js} />
      <script src={assets.client.js} />
    </body>
  </html>
)

export { RENDER_NODE }
export default Html
