import React from 'react'
import { ShallowWrapper, shallow } from 'enzyme'
import Home from 'components/Home/Home'
import Test from 'components/Test/Test'
import Routes, { routes, setRoute } from './index'

describe('(Component) Routes', () => {
  let wrapper: ShallowWrapper

  beforeEach(() => {
    setRoute({
      component: Home,
      isExactPath: true,
      name: 'Home',
      path: '/',
    })
    setRoute({
      component: Test,
      name: 'Test',
      path: '/test',
    })

    wrapper = shallow(<Routes routes={routes} />)
  })

  it('to render the routes', () => {
    expect(wrapper).toMatchInlineSnapshot(`
<React.Suspense
  fallback={<Loading />}
>
  <Switch>
    <Route
      component={[Function]}
      exact={true}
      key="Home"
      path="/"
    />
    <Route
      component={[Function]}
      exact={false}
      key="Test"
      path="/test"
    />
  </Switch>
</React.Suspense>
`)
  })
})
