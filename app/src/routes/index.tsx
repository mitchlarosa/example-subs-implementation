import React, { ReactElement, Suspense } from 'react'
import { Route, RouteProps, Switch } from 'react-router-dom'
import Loading from 'components/Loading/Loading'

interface SetRouteProps {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  component: any
  isExactPath?: boolean
  name: string
  path?: string
}

interface RouteWithProps {
  readonly props: RouteProps
}

interface Props {
  routes: Array<RouteWithProps>
}

const routes: Array<RouteWithProps> = []

const setRoute = ({ component, isExactPath = false, name, path }: SetRouteProps): void => {
  routes.push(<Route key={name} component={component} exact={isExactPath} path={path} />)
  routes.sort((a: RouteWithProps, b: RouteWithProps): number => {
    if (!a.props.exact && b.props.exact) {
      return 1
    }

    if (a.props.path && !b.props.path) {
      return -1
    }

    return 0
  })
}

// eslint-disable-next-line no-shadow
const Routes = ({ routes }: Props): ReactElement => (
  <Suspense fallback={<Loading />}>
    <Switch>{routes}</Switch>
  </Suspense>
)

export { routes, setRoute }
export default Routes
