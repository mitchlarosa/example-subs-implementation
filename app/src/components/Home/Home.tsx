import React, { FunctionComponent, ReactElement } from 'react'
import gql from 'graphql-tag' // eslint-disable-line import/no-extraneous-dependencies
import { Query, QueryResult } from '@apollo/client'
import ErrorMessage from 'components/ErrorMessage/ErrorMessage'
import Loading from 'components/Loading/Loading'
import withFeatureFlags from 'config/featureflags/featureflags.client'
import { FeatureFlags } from 'config/featureflags/featureflags.default'
import { Query as QueryTypes, QueryMemberCheckExistsArgs } from 'config/graphql/types'
import { HOME_NAME, PERM_FEATURE_FLAG_HOME_MEMBER_CHECK } from './constants'

interface Props {
  featureFlags?: FeatureFlags
}

const MEMBER_CHECK_EXISTS_QUERY = gql`
  query MemberCheckExists($email: String!) {
    memberCheckExists(email: $email) {
      memberExists
    }
  }
`
const MEMBER_CHECK_EXISTS_VARIABLES = {
  email: 'contract_test@nine.com.au',
}

const Home: FunctionComponent<Props> = ({ featureFlags = {} }) => (
  <div>
    <h2>{HOME_NAME}</h2>
    {/* {featureFlags[PERM_FEATURE_FLAG_HOME_MEMBER_CHECK] && (
      <Query<QueryTypes['memberCheckExists'], QueryMemberCheckExistsArgs>
        errorPolicy="all"
        query={MEMBER_CHECK_EXISTS_QUERY}
        variables={MEMBER_CHECK_EXISTS_VARIABLES}
      >
        {({ data, error, loading }: QueryResult): ReactElement | null => {
          if (loading) {
            return <Loading />
          }

          if (error) {
            return <ErrorMessage error={error} />
          }

          if (data && data.memberCheckExists && typeof data.memberCheckExists.memberExists === 'boolean') {
            return <div>Member does {!data.memberCheckExists.memberExists && 'not'} exist</div>
          }

          return null
        }}
      </Query>
    )} */}
  </div>
)

export { MEMBER_CHECK_EXISTS_QUERY, MEMBER_CHECK_EXISTS_VARIABLES, Home }
export default withFeatureFlags(Home)
