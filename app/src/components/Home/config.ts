import { lazy } from 'react'
import { setFeatureFlags } from 'config/featureflags/featureflags.default'
import { setNavigationMenuItem } from 'components/NavigationMenu/NavigationMenu'
import { setRoute } from 'routes/index'
import { HOME_NAME, HOME_URL_PATH, PERM_FEATURE_FLAG_HOME_MEMBER_CHECK } from './constants'

setRoute({
  component: lazy(() => import('./Home')),
  isExactPath: true,
  name: HOME_NAME,
  path: HOME_URL_PATH,
})

setFeatureFlags({
  [PERM_FEATURE_FLAG_HOME_MEMBER_CHECK]: true,
})

setNavigationMenuItem({
  name: HOME_NAME,
  path: HOME_URL_PATH,
  position: 0,
})
