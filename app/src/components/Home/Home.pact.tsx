/* eslint-disable import/no-extraneous-dependencies */
import React from 'react'
import { ApolloProvider } from 'react-apollo'
import { cleanup, render, waitForElement } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom'
import { GraphQLInteraction } from '@pact-foundation/pact'
import client from 'config/graphql/graphql'
import { apiFrontProvider } from '../../../../pact.setup'
import '@testing-library/jest-dom/extend-expect'
import { boolean, matcherRequestHeaders, matcherResponseHeaders } from '../../../../tests/contract/lib/matchers'
import { Home } from './Home'
import { PERM_FEATURE_FLAG_HOME_MEMBER_CHECK } from './constants'

// GraphQL queries - Start
const queryMemberCheckExists = `
  query MemberCheckExists($email: String!) {
    memberCheckExists(email: $email) {
      memberExists
      __typename
    }
  }
`
// GraphQL queries - End

// GraphQL constants - Start
const variableMemberCheckExists = {
  email: 'contract_test@nine.com.au',
}
// GraphQL constants - End

// Pact Matchers - Start
const matcherMemberCheckExists = {
  memberExists: boolean(true),
  __typename: 'MemberCheckExistsResponse',
}
// Pact Matchers - End

// Pact Interactions - Start
const memberCheckExists = new GraphQLInteraction()
  .given('API Front is up and working')
  .uponReceiving('Member check exists...')
  .withQuery(queryMemberCheckExists)
  .withOperation('MemberCheckExists')
  .withRequest({
    path: '/',
    method: 'POST',
    headers: matcherRequestHeaders,
  })
  .withVariables(variableMemberCheckExists)
  .willRespondWith({
    status: 200,
    headers: matcherResponseHeaders,
    body: {
      data: {
        memberCheckExists: matcherMemberCheckExists,
      },
    },
  })
// Pact Interactions - End

// Tests  - Start
describe('Interactions in Skeleton Home component', () => {
  afterEach(async () => {
    cleanup()
    await apiFrontProvider.removeInteractions()
  })

  it('checking for existing member', async () => {
    // Registering interactions
    await apiFrontProvider.addInteraction(memberCheckExists)

    const props = {
      featureFlags: {
        [PERM_FEATURE_FLAG_HOME_MEMBER_CHECK]: true,
      },
    }
    // Rendering component
    const { getByText } = render(
      <ApolloProvider client={client}>
        <MemoryRouter>
          <Home {...props} />
        </MemoryRouter>
      </ApolloProvider>
    )
    // Waiting till graphql interaction is over
    expect(getByText(/loading/i)).toBeInTheDocument()
    await waitForElement(() => getByText(/Member does exist/i))
  })
})
// Tests  - End
