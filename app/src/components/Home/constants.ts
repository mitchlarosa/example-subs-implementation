const HOME_NAME = 'Home'
const HOME_URL_PATH = '/'
const PERM_FEATURE_FLAG_HOME_MEMBER_CHECK = 'perm.skeleton-nodejs-application.home.member-check'

export { HOME_NAME, HOME_URL_PATH, PERM_FEATURE_FLAG_HOME_MEMBER_CHECK }
