import React from 'react'
import { MockedProvider } from '@apollo/react-testing'
import { ReactWrapper, mount } from 'enzyme'
import { Query } from 'react-apollo'
import ErrorMessage from 'components/ErrorMessage/ErrorMessage'
import Loading from 'components/Loading/Loading'
import { PERM_FEATURE_FLAG_HOME_MEMBER_CHECK } from './constants'
import { MEMBER_CHECK_EXISTS_QUERY, MEMBER_CHECK_EXISTS_VARIABLES, Home } from './Home'

describe('(Component) Home', () => {
  const props = {
    featureFlags: {
      [PERM_FEATURE_FLAG_HOME_MEMBER_CHECK]: false,
    },
  }
  let wrapper: ReactWrapper

  it('should not render the member check component when feature flag is false', (done) => {
    const mocks = [
      {
        request: {
          query: MEMBER_CHECK_EXISTS_QUERY,
          variables: MEMBER_CHECK_EXISTS_VARIABLES,
        },
        result: {
          data: {
            memberCheckExists: {
              memberExists: false,
            },
          },
        },
      },
    ]

    wrapper = mount(
      <MockedProvider addTypename={false} mocks={mocks}>
        <Home {...props} />
      </MockedProvider>
    )

    // Allow a little time for re-render with data
    setTimeout(() => {
      wrapper.update()
      expect(wrapper.find(Home).children('div')).toMatchInlineSnapshot(`
<div>
  <h2>
    Home
  </h2>
</div>
`)
      done()
    }, 100)
  })

  it('should render the loading message while waiting for a response from the GraphQL server', () => {
    const mocks: [] = []
    const newProps = {
      ...props,
      featureFlags: {
        ...props.featureFlags,
        [PERM_FEATURE_FLAG_HOME_MEMBER_CHECK]: true,
      },
    }

    wrapper = mount(
      <MockedProvider addTypename={false} mocks={mocks}>
        <Home {...newProps} />
      </MockedProvider>
    )

    expect(wrapper.find(Loading)).toMatchInlineSnapshot(`
<Loading>
  <div>
    Loading...
  </div>
</Loading>
`)
  })

  it('should render the error message when an error is returned by the GraphQL server', (done) => {
    const mocks = [
      {
        request: {
          query: MEMBER_CHECK_EXISTS_QUERY,
          variables: MEMBER_CHECK_EXISTS_VARIABLES,
        },
        error: new Error('Test error'),
      },
    ]
    const newProps = {
      ...props,
      featureFlags: {
        ...props.featureFlags,
        [PERM_FEATURE_FLAG_HOME_MEMBER_CHECK]: true,
      },
    }

    wrapper = mount(
      <MockedProvider addTypename={false} mocks={mocks}>
        <Home {...newProps} />
      </MockedProvider>
    )

    // Allow a little time for re-render with error
    setTimeout(() => {
      wrapper.update()
      expect(wrapper.find(ErrorMessage)).toMatchInlineSnapshot(`
<ErrorMessage
  error={[Error: Network error: Test error]}
>
  <div>
    Network error: Test error
  </div>
</ErrorMessage>
`)
      done()
    }, 100)
  })

  it('should render the member check content when data is returned by the GraphQL server', (done) => {
    const mocks = [
      {
        request: {
          query: MEMBER_CHECK_EXISTS_QUERY,
          variables: MEMBER_CHECK_EXISTS_VARIABLES,
        },
        result: {
          data: {
            memberCheckExists: {
              memberExists: false,
            },
          },
        },
      },
    ]
    const newProps = {
      ...props,
      featureFlags: {
        ...props.featureFlags,
        [PERM_FEATURE_FLAG_HOME_MEMBER_CHECK]: true,
      },
    }

    wrapper = mount(
      <MockedProvider addTypename={false} mocks={mocks}>
        <Home {...newProps} />
      </MockedProvider>
    )

    // Allow a little time for re-render with data
    setTimeout(() => {
      wrapper.update()
      expect(wrapper.find(Query).children('div')).toMatchInlineSnapshot(`
<div>
  Member does 
  not
   exist
</div>
`)
      done()
    }, 100)
  })
})
