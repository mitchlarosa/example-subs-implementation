import React from 'react'
import { ShallowWrapper, shallow } from 'enzyme'
import Test from './Test'

describe('(Component) Test', () => {
  let wrapper: ShallowWrapper

  beforeEach(() => {
    wrapper = shallow(<Test />)
  })

  it('to render the test content', () => {
    expect(wrapper).toMatchInlineSnapshot(`
<div>
  <h2>
    Test
  </h2>
</div>
`)
  })
})
