import React, { FunctionComponent } from 'react'
import { useQuery } from '@apollo/client'
import editorialAssetSearch, { variables } from 'config/queries/editorialAssetSearch'
import { TEST_NAME } from './constants'

const Test: FunctionComponent = () => {
  const { data, error, loading } = useQuery(editorialAssetSearch, {
    variables,
  })

  return (
    <div>
      <h2>{TEST_NAME}</h2>
      {!loading &&
        data &&
        data.editorialAssetSearch.edges.map((edge) => (
          <p>
            {edge.node.asset.shortId} - {edge.node.asset.listing.lockedBy} - {edge.node.asset.headlines.headline}
            <br />
          </p>
        ))}
    </div>
  )
}

export default Test
