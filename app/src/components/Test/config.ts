import { lazy } from 'react'
import { setNavigationMenuItem } from 'components/NavigationMenu/NavigationMenu'
import { setRoute } from 'routes/index'
import { TEST_NAME, TEST_URL_PATH } from './constants'

setRoute({
  component: lazy(() => import('./Test')),
  name: TEST_NAME,
  path: TEST_URL_PATH,
})

setNavigationMenuItem({
  name: TEST_NAME,
  path: TEST_URL_PATH,
  position: 1,
})
