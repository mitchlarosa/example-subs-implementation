import React, { FunctionComponent } from 'react'

interface Props {
  error: {
    message: string
  }
}

const ErrorMessage: FunctionComponent<Props> = ({ error }) => <div>{error.message}</div>

export default ErrorMessage
