import React from 'react'
import { ShallowWrapper, shallow } from 'enzyme'
import ErrorMessage from './ErrorMessage'

describe('(Component) ErrorMessage', () => {
  let wrapper: ShallowWrapper

  beforeEach(() => {
    wrapper = shallow(<ErrorMessage error={new Error('Test error')} />)
  })

  it('should render the error message', () => {
    expect(wrapper).toMatchInlineSnapshot(`
<div>
  Test error
</div>
`)
  })
})
