import React from 'react'
import { ShallowWrapper, shallow } from 'enzyme'
import NavigationMenu, { items, setNavigationMenuItem } from './NavigationMenu'

describe('(Component) NavigationMenu', () => {
  let wrapper: ShallowWrapper

  beforeEach(() => {
    setNavigationMenuItem({
      name: 'Home',
      path: '/',
      position: 0,
    })
    setNavigationMenuItem({
      name: 'Test',
      path: '/test',
      position: 1,
    })

    wrapper = shallow(<NavigationMenu items={items} />)
  })

  it('to render the navigation menu', () => {
    expect(wrapper).toMatchInlineSnapshot(`
<ul>
  <li
    key="Home"
  >
    <Link
      to="/"
    >
      Home
    </Link>
  </li>
  <li
    key="Test"
  >
    <Link
      to="/test"
    >
      Test
    </Link>
  </li>
</ul>
`)
  })
})
