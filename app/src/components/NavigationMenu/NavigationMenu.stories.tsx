import React from 'react'
import { routerStoriesOf } from 'utils/storybook/decorators'
import NavigationMenu, { items, setNavigationMenuItem } from './NavigationMenu'

setNavigationMenuItem({
  name: 'Home',
  path: '/',
  position: 0,
})
setNavigationMenuItem({
  name: 'Test',
  path: '/test',
  position: 1,
})

routerStoriesOf('Navigation Menu', module).add('Default', () => <NavigationMenu items={items} />, {
  info: 'a simple navigation',
})
