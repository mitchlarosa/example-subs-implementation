import React, { FunctionComponent, ReactNode } from 'react'
import { Link } from 'react-router-dom'

interface ItemProps {
  name: string
  path: string
  position: number
}

interface Props {
  items: Array<ReactNode>
}

const items: Array<ReactNode> = []

const setNavigationMenuItem = ({ name, path, position }: ItemProps): void => {
  items.splice(
    position,
    0,
    <li key={name}>
      <Link to={path}>{name}</Link>
    </li>
  )
}

// eslint-disable-next-line no-shadow
const NavigationMenu: FunctionComponent<Props> = ({ items }) => <ul>{items}</ul>

export { items, setNavigationMenuItem }
export default NavigationMenu
