import React from 'react'
import { ShallowWrapper, shallow } from 'enzyme'
import Loading from './Loading'

describe('(Component) Loading', () => {
  let wrapper: ShallowWrapper

  beforeEach(() => {
    wrapper = shallow(<Loading />)
  })

  it('should render the loading content', () => {
    expect(wrapper).toMatchInlineSnapshot(`
<div>
  Loading...
</div>
`)
  })
})
