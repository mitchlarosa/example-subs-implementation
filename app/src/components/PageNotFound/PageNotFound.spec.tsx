import React from 'react'
import { ShallowWrapper, shallow } from 'enzyme'
import PageNotFound from './PageNotFound'

describe('(Component) PageNotFound', () => {
  let wrapper: ShallowWrapper

  beforeEach(() => {
    wrapper = shallow(<PageNotFound />)
  })

  it('should render the page not found content', () => {
    expect(wrapper).toMatchInlineSnapshot(`
<div>
  Page not found
</div>
`)
  })
})
