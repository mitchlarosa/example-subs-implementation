import React, { FunctionComponent } from 'react'
import { PAGE_NOT_FOUND_NAME } from './constants'

const PageNotFound: FunctionComponent = () => <div>{PAGE_NOT_FOUND_NAME}</div>

export default PageNotFound
