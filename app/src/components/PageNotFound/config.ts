import { lazy } from 'react'
import { setRoute } from 'routes/index'
import { PAGE_NOT_FOUND_NAME } from './constants'

setRoute({
  component: lazy(() => import('./PageNotFound')),
  name: PAGE_NOT_FOUND_NAME,
})
