import React, { Component, ReactNode } from 'react'
import { log } from '@ffxblue/library-js-logging/build/shared'
import { Logger } from 'config/logger/logger'

interface Props {
  children: ReactNode
}

interface State {
  hasError: boolean
}

class ErrorBoundary extends Component<Props, State> {
  constructor(props: Props) {
    super(props)

    this.state = {
      hasError: false,
    }
  }

  static getDerivedStateFromError(): State {
    return {
      hasError: true,
    }
  }

  // eslint-disable-next-line class-methods-use-this
  componentDidCatch(error: Error): void {
    Logger.error('Error caught by boundary', log.EventType('page', 'error'), log.Error(error))
  }

  render(): ReactNode {
    if (this.state.hasError) {
      return <div>Sorry, an error has occurred</div>
    }

    return this.props.children
  }
}

export default ErrorBoundary
