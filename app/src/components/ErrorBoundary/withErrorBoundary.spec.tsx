import React, { Component, FunctionComponent, ReactNode } from 'react'
import { ReactWrapper, mount } from 'enzyme'
import { withErrorBoundary } from './withErrorBoundary'

const SuccessComponent: FunctionComponent = () => <div>Success</div>
const SuccessComponentWithErrorBoundary = withErrorBoundary(SuccessComponent)
const errorMsg = 'Test error'

class ErrorComponent extends Component {
  componentDidMount(): void {
    throw new Error(errorMsg)
  }

  render(): ReactNode {
    return <div />
  }
}

const ErrorComponentWithErrorBoundary = withErrorBoundary(ErrorComponent)

describe('(Component) withErrorBoundary', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let mockConsoleError: jest.SpyInstance<void, [any?, ...any[]]>
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let mockConsoleLog: jest.SpyInstance<void, [any?, ...any[]]>
  let wrapper: ReactWrapper

  beforeEach(() => {
    mockConsoleError = jest.spyOn(console, 'error').mockImplementation(() => {})
    mockConsoleLog = jest.spyOn(console, 'log').mockImplementation(() => {})
  })

  afterEach(() => {
    jest.restoreAllMocks()
  })

  it('to log when an error occurs', () => {
    wrapper = mount(<ErrorComponentWithErrorBoundary />)

    const { eventLevel, msg } = JSON.parse(mockConsoleLog.mock.calls[0][0])

    expect(mockConsoleError).toHaveBeenCalled()
    expect(mockConsoleLog).toHaveBeenCalled()
    expect(eventLevel).toBe('error')
    expect(msg).toBe('Error caught by boundary')
  })

  it('to display the fallback message when an error occurs', () => {
    wrapper = mount(<ErrorComponentWithErrorBoundary />)

    expect(wrapper).toMatchInlineSnapshot(`
<WrappedComponent>
  <ErrorBoundary>
    <div>
      Sorry, an error has occurred
    </div>
  </ErrorBoundary>
</WrappedComponent>
`)
  })

  it('to display the child component if no error occurs', () => {
    wrapper = mount(<SuccessComponentWithErrorBoundary />)

    expect(wrapper).toMatchInlineSnapshot(`
<WrappedComponent>
  <ErrorBoundary>
    <SuccessComponent>
      <div>
        Success
      </div>
    </SuccessComponent>
  </ErrorBoundary>
</WrappedComponent>
`)
  })
})
