import React, { ComponentProps, ComponentType } from 'react'
import ErrorBoundary from './ErrorBoundary'

export const withErrorBoundary = (Component: ComponentType): Function => {
  const WrappedComponent = (props: ComponentProps<typeof Component>): JSX.Element => (
    <ErrorBoundary>
      <Component {...props} />
    </ErrorBoundary>
  )

  return WrappedComponent
}
