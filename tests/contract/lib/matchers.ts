// Extended custom library over the pact matchers
import { term } from '@pact-foundation/pact/dsl/matchers'

export * from '@pact-foundation/pact/dsl/matchers'

const CONTENT_TYPE_JSON = 'application/json'
const REGEX_NON_EMPTY_STRING = '.*\\S.*'
// TO-DO need to write a proper regex
const REGEX_EMAIL = '.*\\S.*'

export const matcherRequestHeaders = {
  'Content-Type': CONTENT_TYPE_JSON,
}

export const matcherResponseHeaders = {
  'Content-Type': CONTENT_TYPE_JSON,
}

export function nonEmptyString(def:string = 'default') {
  return term({ matcher: REGEX_NON_EMPTY_STRING, generate: def })
}

export function email(def:string = 'default@default.com') {
  return term({ matcher: REGEX_EMAIL, generate: def })
}
