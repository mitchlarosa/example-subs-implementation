const path = require('path')

const config = {
  displayName: 'PACT',
  moduleDirectories: ['node_modules', path.resolve(__dirname, 'app/src')],
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    '\\.(css|scss)$': 'identity-obj-proxy',
  },
  setupFiles: ['./pact.setup.ts'],
  setupFilesAfterEnv: ['./pact.hooks.ts'],
  testRegex: '(\\.|)pact\\.[jt]sx?$',
}

module.exports = config
