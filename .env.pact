DEPLOY_ENV=local
FEATURE_FLAGS_API_REFRESH_INTERVAL=10000
FEATURE_FLAGS_API_URL=http://service-feature-flags-local-v1.ffxblue.com.au
FRONT_API_TIMEOUT=5000
FRONT_API_URL=http://localhost:3333
FRONT_API_URL_EXTERNAL=http://localhost:3333
HTTP_PORT=3333
LOGGING_MINIMUM_LOG_LEVEL=info
LOGGING_URL=http://localhost:3000
NODE_ENV=test
# Use only for local contract testing
# Address of your local broker setup
#PACT_BROKER_HOST=http://localhost:8088
#PACT_PUBLISH_CONTRACTS=false
TRACING_HOST=localhost
TRACING_PORT=6832
TRACING_SAMPLING_RATE=1
